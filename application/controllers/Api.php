<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(['utils']);
		$this->load->model(['Servicio_model', 'Autos_model', 'Crud_model']);
	}

	
	public function segurolluvia_get()
	{

		$id_servicio = $this->get('servicio_id');
		$data_servicio = $this->Servicio_model->listadoservicios(['servicio_id' => $id_servicio]);
		$data['seguro_lluvia'] = false;
		if (isset($data_servicio) && count($data_servicio) > 0) {
			$servicio = isset($data_servicio) && count($data_servicio) > 0 ? getFirstFromArray($data_servicio) : [];
			$validar_auto = getFirstFromArray($this->Servicio_model->fechaEntregaUnidad($servicio->placas));
			
			if ($validar_auto->fecha_entrega_unidad != '') {
				$fecha_seguro_lluvia = date("Y-m-d H:i", strtotime($validar_auto->fecha_entrega_unidad . "+ 1 days"));
				if ($fecha_seguro_lluvia >= date("Y-m-d: H:i") && $servicio->id_servicio == 1) {
					$data['seguro_lluvia'] = true;
				}
			}
		}

		$this->response($data, 200);
	}
}
