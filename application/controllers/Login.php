<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(['utils']);
    }

    public function index()
    {
        $this->load->helper(['form', 'url', 'utils']);
        $this->load->model('Usuario_model');
        $this->form_validation->set_error_delimiters('<div class="error_container">', '</div>');
        $this->form_validation->set_rules('email', 'correo electrónico', 'required|valid_email');
        $this->form_validation->set_rules('password', 'contraseña', 'required');
        $this->form_validation->set_message('required', 'el campo {field} es requerido.');
        $this->form_validation->set_message('valid_email', 'el campo {field} no es un correo valido.');
        $data['msg'] = null;
        if ($this->form_validation->run() == FALSE) {
            $data['msg'] = 1;
            $this->blade->render('login', $data);
        } else {

            $checkemail = $this->Usuario_model->getwherearray('usuarios', [
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            ]);

            if (isset($checkemail) && count($checkemail) > 0) {
                $usuario = getFirstFromArray($checkemail);
                $this->session->set_userdata([
                    'usuario_id' => $usuario->id,
                    'nombre' => $usuario->nombre,
                    'email' => $usuario->email,
                    'telefono' => $usuario->telefono,
                    'id_sucursal' => $usuario->id_sucursal,
                    'apellido_paterno' => $usuario->apellido_paterno,
                    'apellido_materno' => $usuario->apellido_materno,
                    'cuenta_confirmada' => $usuario->cuenta_confirmada,
                ]);
                redirect('xehos/servicios');
            } else {
                $data['msg'] = 1;
                $this->session->set_flashdata('login_error', 'Parametros incorrectos');
                $this->blade->render('login', $data);
            }
        }
    }

    public function cerrarsession()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

    public function registrar()
    {
        $this->load->helper(['form', 'url', 'utils']);
        $this->load->model(['Usuario_model', 'Crud_model']);
        $cat_colores = $this->Crud_model->buscarTodos('cat_colores');
        $cat_modelos = $this->Crud_model->buscarTodos('cat_modelos');
        $cat_marcas = $this->Crud_model->buscarTodos('cat_marcas');
        $cat_anios = $this->Crud_model->buscarTodos('cat_anios');
        $estados = $estados = $this->Crud_model->getwhere('estados', 'activo', 1); //$this->Crud_model->buscarTodos('estados');
        $data['cat_colores'] = $cat_colores;
        $data['cat_modelos'] = $cat_modelos;
        $data['cat_anio'] = $cat_anios;
        $data['cat_marcas'] = $cat_marcas;
        $data['estados'] = $estados;
        $this->blade->render('registrar', $data);
    }

    public function validarcuenta()
    {
        $data['estatus_validado'] = 0;
        if (empty($this->input->get('email')) || empty($this->input->get('token'))) {
            $data['estatus_validado'] = 1;
            return $this->blade->render('app/confirmar_cuenta', $data);
        }
        $email = $this->input->get('email');
        $token = $this->input->get('token');
        $query = $this->db->where('email', $email)
            ->where('usuarioId', $token)
            ->limit(1)
            ->get('usuarios');

        if ($query->num_rows() == 1) {
            $user_data = $query->row();
            if ($user_data->usuarioID == $token && (is_null($user_data->cuenta_confirmada) || $user_data->cuenta_confirmada == 0)) {
                $data['estatus_validado'] = 2;
            } else if ($user_data->usuarioID == $token && $user_data->cuenta_confirmada == 1) {
                $data['estatus_validado'] = 3;
            }
        }

        $this->blade->render('app/confirmar_cuenta', $data);
    }
}
