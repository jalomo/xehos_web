<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AjaxRequestServicio extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->load->library('session');
		$this->load->helper(['form', 'url', 'utils']);
		$this->load->model(['Servicio_model', 'Autos_model', 'Crud_model']);
		$this->load->library(['curl']);
		// if (empty($this->session->userdata('usuario_id'))) {
		// 	redirect('login');
		// }
	}

	public function ajax_gethorariosdisponibles()
	{
		$data = [];
		if ($this->input->post('fecha') !== '' && $this->input->post('id_sucursal') !== '') {
			$sucursal = $this->input->post('id_sucursal');
			$data = $this->Servicio_model->getHorariosDisponibles($this->input->post('fecha'), $sucursal);
		}

		echo json_encode($data);
	}

	public function ajax_getmunicipioByEstado()
	{
		$data = [];
		if ($this->input->post('id_estado') !== '') {
			$id_estado = $this->input->post('id_estado');
			$request = $this->Crud_model->getwhere('cat_municipios', 'id_estado', $id_estado);
			$data = isset($request) && count($request) > 0 ? $request : [];
		}

		echo json_encode($data);
	}

	public function ajax_getvehiculo($id = '')
	{
		$data = [];
		$this->load->model(['Servicio_model', 'Autos_model']);
		if ($id !== '') {
			$data = $this->Autos_model->getAutoByParametros([
				'auto_id' => $id
			]);
		}

		echo json_encode($data);
	}

	public function ajax_guardarvehiculo()
	{
		// $this->load->model(['Servicio_model', 'Autos_model']);
		$registro = $this->Crud_model->store('autos', [
			'id_usuario' => $this->session->userdata('usuario_id'),
			'id_color' => $this->input->post('id_color'),
			'placas' => $this->input->post('placas'),
			'id_modelo' => $this->input->post('id_modelo'),
			'id_marca' => $this->input->post('id_marca'),
			'numero_serie' =>  $this->input->post('numero_serie'),
			'id_anio' =>  !empty($this->input->post('id_anio')) ? $this->input->post('id_anio') : null,
			'kilometraje' =>  !empty($this->input->post('kilometraje')) ? $this->input->post('kilometraje') : null,
			'created_at' => date('Y-m-d')
		]);

		if ($registro) {
			echo json_encode(['status' => 1, 'msg' => '']);
		} else {
			echo json_encode(['status' => 0, 'msg' => 'Ha ocurrido un error.!']);
		}
	}

	public function ajax_datosfacturacion()
	{
		$parametros['id_usuario'] = $this->input->post('id_usuario');
		$parametros['referencia'] = $this->input->post('referencia');
		$parametros['razon_social'] = $this->input->post('razon_social');
		$parametros['domicilio'] = $this->input->post('domicilio');
		$parametros['email_facturacion'] = $this->input->post('email_facturacion');
		$parametros['id_cfdi'] = $this->input->post('id_cfdi');
		$parametros['id_metodo_pago'] = $this->input->post('id_metodo_pago');
		$parametros['cp'] = $this->input->post('cp');
		$parametros['rfc'] = $this->input->post('rfc');
		$registro = $this->Crud_model->store('datos_facturacion', $parametros);

		if ($registro) {
			echo json_encode(['status' => 1, 'msg' => '']);
		} else {
			echo json_encode(['status' => 0, 'msg' => 'Ha ocurrido un error.!']);
		}
	}

	public function ajax_getdatosfacturacion($id = '')
	{
		$data = [];
		$this->load->model(['Crud_model']);
		if ($id !== '') {
			$request = $this->Crud_model->getwhere('datos_facturacion', 'id', $id);
			$data = isset($request) && count($request) > 0 ? $request[0] : [];
		}

		echo json_encode($data);
	}

	public function ajax_getsucursales($id_estado = '')
	{
		$data = [];
		$this->load->model(['Crud_model']);
		if ($id_estado !== '') {
			$request = $this->Crud_model->getwhere('sucursales', 'id_estado', $id_estado);
			$data = isset($request) && count($request) > 0 ? $request : [];
		}
		echo json_encode($data);
	}

	public function ajax_getmodelobymarca()
	{
		$data = [];
		$this->load->model(['Crud_model']);
		if ($this->input->post('id_marca') !== '') {
			$request = $this->Crud_model->getwhere('cat_modelos', 'id_marca', $this->input->post('id_marca'));
			$data = isset($request) && count($request) > 0 ? $request : [];
		}

		echo json_encode($data);
	}

	public function ajax_getfechasBySucursal()
	{
		$data = [];
		$this->load->model(['Servicio_model']);
		if (!empty($this->input->post('id_sucursal'))) {
			$fecha = $this->input->post('fecha') !== '' ? $this->input->post('fecha') : '';
			$request = $this->Servicio_model->getfechaslavadores($fecha, $this->input->post('id_sucursal'));
			$data = isset($request) && count($request) > 0 ? $request : [];
		}

		echo json_encode($data);
	}

	public function ajax_existePlaca()
	{
		$data = [];
		$this->load->model(['Crud_model']);
		if (!empty($this->input->post('placas'))) {
			$request = $this->Crud_model->getwhere('autos', 'placas', $this->input->post('placas'));
			$data = isset($request) && count($request) > 0 ? $request : [];
		}

		echo json_encode($data);
	}

	public function ajax_cupondescuento()
	{
		$data =  $this->curl->curlPost('https://hexadms.com/xehos/index.php/recomendadores/Api_Cupones/validar_cupon', [
			'cupon' => $this->input->post('cupon')
		], true);
		echo $data;
	}

	public function ajax_procesarventa()
	{
		$this->load->model(['Servicio_model', 'Crud_model']);
		if (!empty($this->input->post('id_cupon'))) {
			if (!empty($this->input->post('id_servicio'))) {

				if ($this->input->post('id_tipo_pago') == 1) {
					$this->Servicio_model->store('servicio_paypal', [
						'id_servicio' => $this->input->post('id_servicio'),
						'pagado' => 1,
						'created_at' => date('Y-m-d')
					]);
				}

				if ($this->input->post('id_tipo_pago') !== 2) {
					$this->Servicio_model->store('historial_pago', [
						'id_servicio' => $this->input->post('id_servicio'),
						'usuario' => $this->session->userdata('nombre'),
						'created_at' => date('Y-m-d')
					]);
				}

				$this->Servicio_model->store('cupon_servicio', [
					'id_cupon' => $this->input->post('id_cupon'),
					'id_servicio' => $this->input->post('id_servicio'),
					'activo' => 1,
					'fecha_alta' => date('Y-m-d')
				]);
				$stored_cupon = $this->db->insert_id();
				if (!empty($this->input->post('id_servicio')) && !empty($stored_cupon)) {
					$updated = $this->Crud_model->update('servicio_lavado', 'id', $this->input->post('id_servicio'), [
						'id_cupon_servicio' => $stored_cupon,
						'total' => $this->input->post('total'),
						'id_tipo_pago' => $this->input->post('id_tipo_pago'),
						'subtotal' => $this->input->post('subtotal'),
						'origen' => 10,
						'id_estatus_lavado' => 1,
						'descuento' => $this->input->post('descuento')
					]);

					if ($updated == 1) {
						// $data_cupon = $this->sellarcupon();
						// $sms_msg = $this->enviarsms('has solicitado un servicio de autolavado');
						$this->getParametersSendEmail($this->input->post('id_servicio'));
						echo json_encode(['status' => 1, 'msg' => 'El operador atendera el servicio lo antes posible']);
					} else {
						echo json_encode(['status' => 0, 'msg' => 'Ha ocurrido un error actualizando el servicio!']);
					}
				} else {
					echo json_encode(['status' => 0, 'msg' => 'Ha ocurrido un error actualizando el servicio!']);
				}
			}
		} else {
			if (!empty($this->input->post('id_servicio'))) {
				$updated = $this->Crud_model->update('servicio_lavado', 'id', $this->input->post('id_servicio'), [
					'total' => $this->input->post('total'),
					'origen' => 10,
					'id_estatus_lavado' => 1,
					'id_tipo_pago' => $this->input->post('id_tipo_pago'),
					'subtotal' => $this->input->post('subtotal'),
					'descuento' => $this->input->post('descuento')
				]);

				if ($this->input->post('id_tipo_pago') == 1) {
					$this->Servicio_model->store('servicio_paypal', [
						'id_servicio' => $this->input->post('id_servicio'),
						'pagado' => 1,
						'created_at' => date('Y-m-d')
					]);
				}

				if ($this->input->post('id_tipo_pago') !== 2) {
					$this->Servicio_model->store('historial_pago', [
						'id_servicio' => $this->input->post('id_servicio'),
						'usuario' => $this->session->userdata('nombre'),
						'created_at' => date('Y-m-d')
					]);
				}

				if ($updated == 1) {
					// $sms_msg = $this->enviarsms('has solicitado un servicio de autolavado');
					$this->getParametersSendEmail($this->input->post('id_servicio'));
					echo json_encode(['status' => 1, 'msg' => 'El operador atendera el servicio lo antes posible']);
				} else {
					echo json_encode(['status' => 0, 'msg' => 'Ha ocurrido un error actualizando el servicio!']);
				}
			}
		}
	}




	public function sellarcupon()
	{
		$data = $this->curl->curlPost('https://hexadms.com/xehos/index.php/recomendadores/Api_Cupones/usar_cupon', [
			'id_cupon' => $this->input->post('id_cupon'),
			'id_cliente' => $this->session->userdata('usuario_id')
		], true);

		echo $data;
	}

	public function enviarsms($mensaje = '')
	{
		$data = $this->curl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje_xehos', [
			'mensaje' => $mensaje,
			'celular' => $this->session->userdata('telefono'),
			'sucursal' => "Xehos"
		], true);

		echo $data;
	}

	public function crearfacturacion()
	{
		$data = $this->curl->curlPost('https://sohexs.com/facturacion/index.php/api_factura/crear_factura', [
			'receptor_nombre' => $this->input->post('receptor_nombre'),
			'receptor_id_cliente' => $this->input->post('receptor_id_cliente'),
			'fatura_lugarExpedicion' => $this->input->post('fatura_lugarExpedicion'),
			'receptor_email' => $this->input->post('receptor_email'),
			'factura_folio' => $this->input->post('factura_folio'),
			'factura_serie' => $this->input->post('factura_serie'),
			'receptor_direccion' => $this->input->post('receptor_direccion'),
			'factura_formaPago' => $this->input->post('factura_formaPago'),
			'factura_medotoPago' => $this->input->post('factura_medotoPago'),
			'receptor_RFC' => $this->input->post('receptor_RFC'),
			'receptor_uso_CFDI' => $this->input->post('receptor_uso_CFDI'),
			'concepto_NoIdentificacion' => $this->input->post('concepto_NoIdentificacion'),
			'concepto_nombre' => $this->input->post('concepto_nombre'),
			'concepto_precio' => $this->input->post('concepto_precio'),
			'concepto_importe' => $this->input->post('concepto_importe'),
			'id_producto_servicio_interno' => $this->input->post('id_producto_servicio_interno'),
			'concepto_cantidad' => $this->input->post('concepto_cantidad'),
			'importe_iva' => $this->input->post('importe_iva')
		], true);

		echo $data;
	}

	public function guardarcalificacion()
	{
		$this->load->model(['Crud_model']);
		$servicio_calificacion =  $this->Servicio_model->store('servicio_calificacion', [
			'id_servicio' => $this->input->post('id_servicio'),
			'puntaje' => $this->input->post('puntaje'),
			'operador_servicio' => $this->input->post('operador_servicio'),
			'satisfecho' => $this->input->post('satisfecho'),
			'recomendaria' => $this->input->post('recomendaria'),
			'comentarios' => !empty($this->input->post('comentarios')) ? $this->input->post('comentarios') : null,
			'created_at' => date('Y-m-d')
		]);
		if (!empty($this->input->post('id_servicio')) && !empty($this->db->insert_id())) {
			$this->Crud_model->update(
				'servicio_lavado',
				'id',
				$this->input->post('id_servicio'),
				['id_servicio_calificacion' => $this->db->insert_id()]
			);
		}

		if (!empty($servicio_calificacion) && $servicio_calificacion != 0) {
			echo json_encode(['status' => 1, 'msg' => 'Gracias por su comentarios.']);
		} else {
			echo json_encode(['status' => 0, 'msg' => 'Ha ocurrido un error!']);
		}
	}


	public function testfunctions()
	{
		$json = json_decode($this->enviarsms('mensaje'), true);
		// dd($json);
		echo $json;
		// echo $this->enviarsms('mensaje');
	}

	public function ajax_getpositionlavador()
	{
		$data = [];
		if ($this->input->post('id_servicio') !== '') {
			$id_servicio = $this->input->post('id_servicio');
			$request = $this->Crud_model->getwhere('servicio_lavado', 'id', $id_servicio);
			$data = isset($request) && count($request) > 0 ? $request : [];
		}

		echo json_encode($data);
	}

	public function ajax_updatepassword()
	{
		$data = [];
		if ($this->input->post('password') !== '') {
			// $id_servicio = $this->input->post('id_servicio');
			// $request = $this->Crud_model->getwhere('servicio_lavado', 'id', $id_servicio);
			// $data = isset($request) && count($request) > 0 ? $request : [];
			$updated = $this->Crud_model->update('usuarios', 'id', $this->session->userdata('usuario_id'), [
				'password' => $this->input->post('password')
			]);
			// dd($this->sendEmailPassword());
			if (!empty($updated) && $updated != 0) {
				echo json_encode(['status' => 1, 'msg' => 'Password actualizado']);
				$this->sendEmailPassword();
				die();
			} else {
				echo json_encode(['status' => 0, 'msg' => 'Ha ocurrido un error!']);
				die();
			}
		}

		echo json_encode($data);
	}

	public function sendEmailPassword()
	{
		$this->load->helper(['correo']);
		$query = $this->db->where('id',  $this->session->userdata('usuario_id'))
			->limit(1)
			->get('usuarios');
		if ($query->num_rows() == 1) {
			// $datos = $query->row();
			// dd($datos);
			$cuerpo = $this->blade->render('app/email/cambio_password', [
				'datos' => $query->row(),
				'asunto' => '¡Cambio de contraseña!'
			], true);
	
			$sender = $this->session->userdata('email');
			// dd([
			// 	$cuerpo,
			// 	$sender
			// ]);
			enviar_correo($sender, "Cambio de contraseña!", $cuerpo, array());
		}

	}

	public function ajax_updateservicio()
	{
		if ($this->input->post('id_servicio') !== '') {
			$id_servicio = $this->input->post('id_servicio');

			if (!empty($this->input->post('data'))) {
				$request = $this->input->post('data');
				if (isset($request['cancelado'])) {
					$this->Servicio_model->store('historial_cancelaciones', [
						'id_servicio' => $this->input->post('id_servicio'),
						'id_usuario' => $this->session->userdata('usuario_id'),
						'created_at' => date('Y-m-d')
					]);
				}

				if (isset($request['confirmado'])) {
					$this->Servicio_model->store('historial_confirmaciones', [
						'id_servicio' => $this->input->post('id_servicio'),
						'id_usuario' => $this->session->userdata('usuario_id'),
						'created_at' => date('Y-m-d')
					]);
				}
			}

			$updated = $this->Crud_model->update('servicio_lavado', 'id', $id_servicio, $this->input->post('data'));
			if (!empty($updated) && $updated != 0) {
				echo json_encode(['status' => 1, 'msg' => 'Gracias por usar nuestro servicio']);
			} else {
				echo json_encode(['status' => 0, 'msg' => 'Ha ocurrido un error!']);
			}
		} else {
			echo json_encode(['status' => 0, 'msg' => 'Faltan parametros']);
		}
	}

	public function ajax_validar_registrar()
	{
		$this->load->helper(['form', 'url', 'utils']);
		$this->load->library('form_validation');
		$this->load->model(['Usuario_model', 'Crud_model']);
		$this->form_validation->set_error_delimiters('<div class="error_container">', '</div>');
		$this->form_validation->set_rules('email', 'correo electrónico', 'required|valid_email|is_unique[usuarios.email]');
		$this->form_validation->set_rules('nombre', 'nombre', 'required');
		$this->form_validation->set_rules('apellido_paterno', 'apellido paterno', 'required');
		$this->form_validation->set_rules('apellido_materno', 'apellido materno', 'required');
		$this->form_validation->set_rules('password', 'contraseña', 'required');
		$this->form_validation->set_rules('passconf', 'confirmar contraseña', 'required|matches[password]');
		$this->form_validation->set_rules('id_sucursal', 'sucursal', 'required');
		$this->form_validation->set_rules('id_estado', 'estado', 'required');
		$this->form_validation->set_rules('id_color', 'color', 'required');
		$this->form_validation->set_rules('placas', 'placas', 'required|is_unique[autos.placas]');
		$this->form_validation->set_rules('telefono', 'telefono', 'required|is_unique[usuarios.telefono]');
		$this->form_validation->set_rules('id_modelo', 'modelo', 'required');
		$this->form_validation->set_rules('id_marca', 'marca', 'required');
		$this->form_validation->set_message('required', 'el campo {field} es requerido.');

		$this->form_validation->set_message('required', 'el campo {field} es requerido.');
		$this->form_validation->set_message('matches', 'el campo {field} no coincide con el password.');
		$this->form_validation->set_message('valid_email', 'el campo {field} no es un correo valido.');
		$this->form_validation->set_message('is_unique', 'el campo {field} ya fue registrado.');
		if ($this->form_validation->run() == FALSE) {
			$data['error'] = true;
			$data['validaciones'] = validation_errors_array();
			echo json_encode($data);
		} else {
			$token = md5(sha1('autolavado'));
			$registrar = $this->Usuario_model->store('usuarios', [
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
				'nombre' => strtoupper($this->input->post('nombre')),
				'apellido_paterno' => strtoupper($this->input->post('apellido_paterno')),
				'apellido_materno' => strtoupper($this->input->post('apellido_materno')),
				'created_at' => date('Y-m-d'),
				'usuarioID' =>  $token,
				'telefono' =>  $this->input->post('telefono'),
				'id_sucursal' => $this->input->post('id_sucursal'),
				'token' =>  00001
			]);

			if ($registrar && !empty($this->db->insert_id())) {
				$this->Crud_model->store('autos', [
					'id_usuario' => $this->db->insert_id(),
					'id_color' => $this->input->post('id_color'),
					'placas' => $this->input->post('placas'),
					'id_modelo' => $this->input->post('id_modelo'),
					'id_marca' => $this->input->post('id_marca'),
					'numero_serie' =>  $this->input->post('numero_serie'),
					'id_anio' =>  !empty($this->input->post('id_anio')) ? $this->input->post('id_anio') : null,
					'kilometraje' =>  !empty($this->input->post('kilometraje')) ? $this->input->post('kilometraje') : null,
					'created_at' => date('Y-m-d')
				]);

				// $this->session->set_flashdata('registro_exitoso', 'Usuario creado.');
				// $this->codigoregistro([
				// 	'telefono' => $this->input->post('telefono'),
				// 	'correo' => $this->input->post('email'),
				// 	'token' => $token
				// ]);

				$data['error'] = false;
				$data['msg'] = "Usuario Registrado";
				$data['token'] =  $token;
				echo json_encode($data);
			} else {
				$data['error'] = true;
				$data['msg'] = "Ha ocurrido un error.";
				echo json_encode($data);
			}
		}
	}

	public function ajax_correo_confirmacion()
	{
		$this->codigoregistro([
			'telefono' => $this->input->post('telefono'),
			'correo' => $this->input->post('correo'),
			'token' => $this->input->post('token')
		]);
	}

	public function ajax_renviar_codigo_registro()
	{
		$query = $this->db->where('email', $this->input->post('correo'))
			->limit(1)
			->get('usuarios');

		if ($query->num_rows() == 1) {
			$user_data = $query->row();
			$this->codigoregistro([
				'telefono' => $user_data->telefono,
				'correo' => $user_data->email,
				'token' => $user_data->usuarioID
			]);
		}
	}

	public function codigoregistro($parametros)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_codigo_lavado',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array(
				'telefono' => $parametros['telefono'],
				'correo' => $parametros['correo'],
				'token' => $parametros['token']
			)
		));

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;
	}

	public function confirmarcuenta()
	{
		// dd("entra");
		$updated = $this->Crud_model->update('usuarios', 'id', $this->session->userdata('usuario_id'), [
			'cuenta_confirmada' => 1
		]);

		if ($updated == 1) {
			echo json_encode(['status' => true, 'msg' => 'La cuenta se ha verificado correctamente']);
		} else {
			echo json_encode(['status' => false, 'msg' => 'No se ha podido verificar la cuenta']);
		}
	}

	public function testemail()
	{
		// $this->load->library('enviaCorreos');
		return $this->sendemail([
			'id_lavador' => 1,
			'id_servicio' => 1,
			'horario_lavador' => date('Y-m-d H:i'),
			'folio_mostrar' => "folio-1",
		], "sfadiego@gmail.com");
	}

	public function getParametersSendEmail($id_servicio)
	{
		$request_servicio = $this->Crud_model->getById('servicio_lavado', $id_servicio);
		$servicio = getFirstFromArray($request_servicio);
		$request_horario = $this->Crud_model->getById('horarios_lavadores', $servicio->id_horario);
		$lavador = getFirstFromArray($request_horario);
		$data = [
			'id_lavador' => $servicio->id_lavador,
			'id_servicio' => $servicio->id,
			'horario_lavador' => $lavador->fecha . ' ' . $lavador->hora,
			'folio_mostrar' => $servicio->folio_mostrar,
		];
		return $this->sendemail($data);
	}

	public function sendemail($parametros, $send_to = '')
	{
		$this->load->helper(['correo']);
		$data_lavador = $this->Crud_model->getwhere('lavadores', 'lavadorId', $parametros['id_lavador']);
		$data_sevicio = $this->Crud_model->getwhere('servicio_lavado', 'id', $parametros['id_servicio']);
		$servicio = getFirstFromArray($data_sevicio);
		$data_tipo_sevicio = $this->Crud_model->getwhere('servicios', 'servicioId', $servicio->id_servicio);
		$tipo_servicio = getFirstFromArray($data_tipo_sevicio);
		$lavador = getFirstFromArray($data_lavador);

		$datos_correo = [
			'nombre' => $this->session->userdata('nombre'),
			'apellido_paterno' => $this->session->userdata('apellido_paterno'),
			'apellido_materno' => $this->session->userdata('apellido_materno'),
			'id_servicio' => $parametros['id_servicio'],
			'fecha_hora' => date_eng2esp($parametros['horario_lavador']),
			'lavador' => $lavador->lavadorNombre,
			'servicio' => $tipo_servicio->servicioNombre,
			'folio_mostrar' => $parametros['folio_mostrar']
		];

		$cuerpo = $this->blade->render('app/email/confirmacion_cita', [
			'datos_cita' => $datos_correo,
			'id_servicio' => $datos_correo['id_servicio'],
			'asunto' => '¡Asignación de cita!'
		], true);

		$sender = $send_to != '' ? $send_to : $this->session->userdata('email');
		// dd([
		// 	$cuerpo,
		// 	$sender
		// ]);
		enviar_correo($sender, "Confirmación de cita!", $cuerpo, array());
	}
}
