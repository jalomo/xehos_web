<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class ApiChat extends CI_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->library('chaton');

  }

  public function getTotalNotificaciones()
  {
    try {

      $telefono = $this->input->get('telefono');
      $dataNotificacion = $this->chaton->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/contador_notificaciones/' . $telefono, false, false);
      $notificaciones = json_decode($dataNotificacion);
      return print_r(json_encode($notificaciones));
    } catch (Exception $e) {
      return $e;
    }
  }
  public function getUltimaNotificacion()
  {
    try {

      $telefono = $this->input->get('telefono');
      $usuarios_admin = $this->db->select('adminNombre as nombre, telefono, "Operador" as tipo')->where('telefono !=', null)->get('admin')->result_array();
      $usuarios_lavadores = $this->db->select('lavadorNombre as nombre, lavadorTelefono as telefono, "Lavador" as tipo')->where('lavadorTelefono !=', null)->get('lavadores')->result_array();
      $usuarios = array_merge($usuarios_admin, $usuarios_lavadores);

      $dataNotificacion = $this->chaton->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/ultima_notificacion/' . $telefono, false, false);
      $notificaciones = json_decode($dataNotificacion);

      foreach ($usuarios as $user) {
        if ($user['telefono'] == $notificaciones->celular2) {
          $data = [
            'usuario' => $user['nombre'],
            'tipo' => $user['tipo'],
            'mensaje' => $notificaciones->mensaje
          ];
        }
      }

      return print_r(json_encode($data));
    } catch (Exception $e) {
      return $e;
    }
  }
  public function getNotificaciones()
  {
    try {

      $telefono = $this->input->get('telefono');

      $usuarios_admin = $this->db->select('adminNombre as nombre, telefono, "Operador" as tipo')->where('telefono !=', null)->get('admin')->result_array();
      $usuarios_lavadores = $this->db->select('lavadorNombre as nombre, lavadorTelefono as telefono, "Lavador" as tipo')->where('lavadorTelefono !=', null)->get('lavadores')->result_array();
      $usuarios = array_merge($usuarios_admin, $usuarios_lavadores);

      $dataNotificacion = $this->chaton->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones/' . $telefono, false, false);
      $notificaciones = json_decode($dataNotificacion)->data;
      $listado = [];
      foreach ($notificaciones as $notify) {
        $notify->usuario = '';
        $notify->tipoUsuario = '';
        foreach ($usuarios as $user) {
          if ($notify->celular2 == $user['telefono']) {
            $notify->usuario = $user['nombre'];
            $notify->tipoUsuario = $user['tipo'];
          }
        }
        array_push($listado, $notify);
      }
      return print_r(json_encode($listado));
    } catch (Exception $e) {
      return $e;
    }
  }

  public function getchat()
  {
    try {
      $telefono1 = $this->input->get('telefono1');
      $telefono2 = $this->input->get('telefono2');

      $dataNotificacion = $this->chaton->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/notificaciones_busqueda', [
        'telefono' => $telefono1,
        'celular2' => $telefono2
      ], false);
      $notificaciones = json_decode($dataNotificacion)->data;
      $convArrayKardex = json_decode(json_encode($notificaciones), true);
      usort($convArrayKardex, function ($b, $a) {
        return strcmp($b["fecha_creacion"], $a["fecha_creacion"]);
      });

      return print_r(json_encode($convArrayKardex));
    } catch (Exception $e) {
      return $e;
    }
  }
  public function apiNotificacion()
  {
    try {
      $dataNotificacion = $this->chaton->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion', [
        'celular' => $this->input->get('telefono'),
        'mensaje' => $this->chaton->eliminar_tildes($this->input->get('mensaje')),
        'sucursal' => 'M2137',
        'celular2' => $this->input->get('celular2'),
        'mensaje_respuesta' => $this->input->get('mensaje_respuesta')
      ], false);

      return print_r(json_encode($dataNotificacion));
    } catch (Exception $e) {
      return $e;
    }
  }
  public function apiEliminarNotificacion()
  {
    try {
      $dataNotificacion = $this->chaton->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/eliminar_notificacion', [
        'id' => $this->input->get('id'),
      ], false);

      return print_r(json_encode($dataNotificacion));
    } catch (Exception $e) {
      return $e;
    }
  }
  public function apiMarcarLeidoByCelulares()
  {
    try {
      $dataNotificacion = $this->chaton->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/ver_notificacion_numero', [
        'celular' => $this->input->get('celular'),
        'celular2' => $this->input->get('celular2'),
      ], false);

      return print_r(json_encode($dataNotificacion));
    } catch (Exception $e) {
      return $e;
    }
  }
  public function apiMarcarLeidoById()
  {
    try {
      $dataNotificacion = $this->chaton->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/ver_notificacion_numero_id', [
        'id' => $this->input->get('id'),
      ], false);

      return print_r(json_encode($dataNotificacion));
    } catch (Exception $e) {
      return $e;
    }
  }

}
