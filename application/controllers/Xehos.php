<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Xehos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(['form', 'url', 'utils', 'date', 'correo']);
		$this->load->model(['Servicio_model', 'Autos_model', 'Crud_model']);
		date_default_timezone_set('America/Mexico_City');

		if (empty($this->session->userdata('usuario_id'))) {
			redirect('login');
		} else {
			if (empty($this->session->userdata('cuenta_confirmada'))) {
				$link = "<a class=''  id='btn_enviar_verificacion' onclick='reenviar_correo()' >Link</a>";
				$this->session->set_flashdata('login_error', 'La cuenta no se ha verificado, revisar el correo electrónico o reenviar: ' . $link);
				redirect('login');
			}
		}
	}

	public function index()
	{
		redirect('xehos/misvehiculos');
	}

	public function autopartes()
	{
		$this->blade->render('app/autopartes');
	}

	public function tecompramostuauto()
	{
		$this->blade->render('app/tecompramostuauto');
	}

	public function sucursal()
	{
		$sucursales = $this->Crud_model->getwhere('sucursales', 'activo', 1);
		$data['sucursales'] = $sucursales;
		$this->blade->render('app/sucursales', $data);
	}

	public function altavehiculo()
	{

		$cat_colores = $this->Crud_model->buscarTodos('cat_colores');
		$cat_modelos = $this->Crud_model->buscarTodos('cat_modelos');
		$cat_marcas = $this->Crud_model->buscarTodos('cat_marcas');
		$cat_anios = $this->Crud_model->buscarTodos('cat_anios');
		$data['cat_colores'] = $cat_colores;
		$data['cat_modelos'] = $cat_modelos;
		$data['cat_anio'] = $cat_anios;
		$data['cat_marcas'] = $cat_marcas;


		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
		$this->form_validation->set_rules('id_color', 'color', 'required');
		$this->form_validation->set_rules('placas', 'placas', 'required|max_length[10]|is_unique[autos.placas]');
		$this->form_validation->set_rules('id_modelo', 'modelo', 'required');
		$this->form_validation->set_rules('id_marca', 'marca', 'required');
		$this->form_validation->set_rules('numero_serie', 'numero de serie', 'exact_length[17]');
		$this->form_validation->set_rules('kilometraje', 'Kilometraje', 'numeric');

		$this->form_validation->set_message('max_length', 'El campo {field} no es valido');
		$this->form_validation->set_message('exact_length', 'El campo {field} no es valido');
		$this->form_validation->set_message('is_unique', 'El campo {field} ya fue registrado.');
		$this->form_validation->set_message('required', 'El campo {field} es requerido.');

		$data['msg'] = null;
		if ($this->form_validation->run() == FALSE) {
			$data['msg'] = 1;
			$this->blade->render('app/alta_vehiculo', $data);
		} else {
			$registro = $this->Crud_model->store('autos', [
				'id_usuario' => $this->session->userdata('usuario_id'),
				'id_color' => $this->input->post('id_color'),
				'placas' => $this->input->post('placas'),
				'id_modelo' => $this->input->post('id_modelo'),
				'id_marca' => $this->input->post('id_marca'),
				'numero_serie' =>  $this->input->post('numero_serie'),
				'id_anio' =>  !empty($this->input->post('id_anio')) ? $this->input->post('id_anio') : null,
				'kilometraje' =>  !empty($this->input->post('kilometraje')) ? $this->input->post('kilometraje') : null,
				'created_at' => date('Y-m-d')
			]);

			if ($registro) {
				redirect('/xehos/misvehiculos');
			} else {
				$this->session->set_flashdata('registro_error', 'Ha ocurrido un error.!');
				$this->blade->render('app/alta_vehiculo', $data);
			}
		}
	}

	public function testsql()
	{
		// $misautos = $this->Servicio_model->listadoservicios(
		// 	['id_usuario' => $this->session->userdata('usuario_id')]
		// );
		// echo json_encode($misautos);
		// $servicio['folio_mostrar'] = getFolioMostrar(1, false);
		// $servicio['folio_control'] = getFolioControl();
		// dd($servicio);
	}

	public function misvehiculos()
	{
		$this->load->model('Autos_model');
		$misautos = $this->Autos_model->getAutosByUsurio($this->session->userdata('usuario_id'));
		$data['misautos'] = $misautos;
		$this->blade->render('app/misvehiculos', $data);
	}

	public function detalleServicio($id = '')
	{
		if ($id != '') {
			$id =  decrypt($id);
		}
		$data_servicio = $this->Servicio_model->getdetalleservicio(['servicio_id' => $id]);
		if (empty($data_servicio)) {
			redirect('xehos/listaservicios');
			die();
		}
		$data['info_servicio'] =  isset($data_servicio) ? getFirstFromArray($data_servicio) : [];
		$this->blade->render('app/servicio/detalle_servicio', $data);
	}

	public function servicios($id = '')
	{
		if ($id != '') {
			$id =  decrypt($id);
		}


		$this->load->helper(['form', 'url', 'utils']);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
		$this->form_validation->set_rules('id_usuario', 'usuario', 'required');
		$this->form_validation->set_rules('id_auto', 'auto', 'required');
		$this->form_validation->set_rules('calle', 'calle', 'required');
		$this->form_validation->set_rules('colonia', 'colonia', 'required');
		$this->form_validation->set_rules('id_sucursal', 'sucursal', 'required');
		$this->form_validation->set_rules('id_estado', 'estado', 'required');
		$this->form_validation->set_rules('id_municipio', 'municipio', 'required');
		$this->form_validation->set_rules('latitud', 'latitud', 'required');
		$this->form_validation->set_rules('longitud', 'longitud', 'required');
		$this->form_validation->set_rules('fecha', 'fecha', 'required');
		$this->form_validation->set_rules('hora', 'hora', 'required');
		$this->form_validation->set_rules('servicio', 'servicio', 'required');
		$this->form_validation->set_message('required', 'El campo {field} es requerido');
		$this->load->model(['Servicio_model', 'Autos_model']);

		$misautos = $this->Autos_model->getAutosByUsurio($this->session->userdata('usuario_id'));
		$cat_servicios = $this->Crud_model->buscarTodos('servicios');
		$estados = $this->Crud_model->getwhere('estados', 'activo', 1); //$this->Crud_model->buscarTodos('estados');
		$cat_colores = $this->Crud_model->buscarTodos('cat_colores');
		$cat_modelos = $this->Crud_model->buscarTodos('cat_modelos');
		$cat_marcas = $this->Crud_model->buscarTodos('cat_marcas');
		$cat_anios = $this->Crud_model->buscarTodos('cat_anios');
		$cat_cfdi = $this->Crud_model->buscarTodos('cat_cfdi');

		$data['cat_cfdi'] = $cat_cfdi;
		$data['id_servicio'] = $id;
		$data['cat_colores'] = $cat_colores;
		$data['cat_modelos'] = $cat_modelos;
		$data['cat_anio'] = $cat_anios;
		$data['cat_marcas'] = $cat_marcas;
		$data['misautos'] = $misautos;
		$data['cat_servicios'] = $cat_servicios;
		$data['estados'] = $estados;

		$data['info_servicio'] = null;
		if ($id != '') {
			$data_servicio = $this->Crud_model->getwhere('servicio_lavado', 'id', $id);
			$servicio = getFirstFromArray($data_servicio);
			$data['info_servicio'] = $servicio;
			$data_ubicacion = $this->Crud_model->getwhere('ubicacion_servicio', 'id', $servicio->id_ubicacion);
			$ubicacion = getFirstFromArray($data_ubicacion);
			$data['info_ubicacion'] = $ubicacion;

			$data_horario	 = $this->Crud_model->getwhere('horarios_lavadores', 'id', $servicio->id_horario);
			$data_hora_lavador = getFirstFromArray($data_horario);
			$data['data_hora_lavado'] = $data_hora_lavador;

			$data_tipo_servicio	 = $this->Crud_model->getwhere('servicios', 'servicioId', $servicio->id_servicio);
			$nombre_servicio = getFirstFromArray($data_tipo_servicio);
			$data['data_tipo_servicio'] = $nombre_servicio;
		}

		$datos_facturacion = $this->Crud_model->getwhere('datos_facturacion', 'id_usuario', $this->session->userdata('usuario_id'));
		$data['tiene_datosfacturacion'] = count($datos_facturacion) > 0 ? 1 : 0;
		$data['datos_facturacion'] = $datos_facturacion;

		$data['msg'] = null;
		if ($this->form_validation->run() == FALSE) {
			$data['msg'] = 1;
			return $this->blade->render('app/servicio/servicios', $data);
		} else {
			$data_lavador = $this->Servicio_model->getIdLavador();
			if (!$data_lavador) {
				$data['msg'] = 1;
				$this->session->set_flashdata('registro_error', 'No hay horario disponibles.');
				return $this->blade->render('app/servicio/servicios', $data);
			}

			$ubicacion = [];
			$dataservicio = [];
			$ubicacion['numero_int'] = !empty($this->input->post('numero_int')) ? $this->input->post('numero_int') : null;
			$ubicacion['numero_ext'] = !empty($this->input->post('numero_ext')) ? $this->input->post('numero_ext') : null;
			$ubicacion['colonia'] = !empty($this->input->post('colonia')) ? $this->input->post('colonia') : null;
			$ubicacion['id_estado'] = !empty($this->input->post('id_estado')) ? $this->input->post('id_estado') : null;
			$ubicacion['id_municipio'] = !empty($this->input->post('id_municipio')) ? $this->input->post('id_municipio') : null;
			$ubicacion['latitud'] = !empty($this->input->post('latitud')) ? $this->input->post('latitud') : null;
			$ubicacion['longitud'] = !empty($this->input->post('longitud')) ? $this->input->post('longitud') : null;
			$ubicacion['calle'] = !empty($this->input->post('calle')) ? $this->input->post('calle') : null;
			$ubicacion['id_sucursal'] = !empty($this->input->post('id_sucursal')) ? $this->input->post('id_sucursal') : null;
			$ubicacion['created_at'] = date('Y-m-d H:i');

			if ($id != '') {
				$horario_anterior = $servicio->id_horario;
				$nuevo_horario = $data_lavador->id;
				$this->Crud_model->update('ubicacion_servicio', 'id', $servicio->id_ubicacion, $ubicacion);
				//desocupar lavador
				$this->Crud_model->update('horarios_lavadores', 'id', $horario_anterior, ['ocupado' => 0]);
				//ocupar lavador
				$this->Crud_model->update('horarios_lavadores', 'id', $nuevo_horario, ['ocupado' => 1]);
			} else {

				$this->Crud_model->store('ubicacion_servicio', $ubicacion);
				$id_ubicacion = $this->db->insert_id();
				$dataservicio['id_ubicacion'] = $id_ubicacion;
				//ocupar lavador
				$this->Crud_model->update('horarios_lavadores', 'id', $data_lavador->id, ['ocupado' => 1]);
			}

			$dataservicio['folio_mostrar'] = getFolioMostrar($_POST['id_sucursal'], false);
			$dataservicio['folio_control'] = getFolioControl();
			$dataservicio['id_lavador'] = $data_lavador->id_lavador;
			$dataservicio['id_horario'] = $data_lavador->id;
			$dataservicio['fecha_programacion'] = $data_lavador->fecha . ' ' . $data_lavador->hora;
			$dataservicio['id_usuario'] = $this->input->post('id_usuario');
			$dataservicio['id_auto'] = $this->input->post('id_auto');
			$dataservicio['latitud_lavador'] = '';
			$dataservicio['longitud_lavador'] = '';
			$dataservicio['origen'] = 10;
			$dataservicio['id_sucursal'] = !empty($this->input->post('id_sucursal')) ? $this->input->post('id_sucursal') : null;

			$dataservicio['id_servicio'] = $this->input->post('servicio');
			$dataservicio['comentarios'] = $this->input->post('comentarios');
			$dataservicio['id_usuario_creo'] = $this->session->userdata('usuario_id');
			$dataservicio['created_at'] = date('Y-m-d H:i');

			if ($id != '') {
				$updated_servicio = $this->Crud_model->update('servicio_lavado', 'id', $id, $dataservicio);
				if ($updated_servicio == 1) {
					redirect('xehos/listaservicios');
				} else {
					$this->session->set_flashdata('registro_error', 'Ha ocurrido un error actualizando servicio');
					return $this->blade->render('app/servicio/servicios', $data);
				}
			} else {
				$stored_servicio = $this->Crud_model->store('servicio_lavado', $dataservicio);
				if ($stored_servicio && !empty($this->db->insert_id())) {
					$servicio_id = $this->db->insert_id();
					// $this->sendemail([
					// 	'id_servicio' => $servicio_id,
					// 	'horario_lavador' => $data_lavador->fecha . ' ' . $data_lavador->hora,
					// 	'id_lavador' => $data_lavador->id_lavador,
					// 	'folio_mostrar' => $dataservicio['folio_mostrar']
					// ]);

					// $mensaje = 'SEHOX AUTOLAVADO, se asignó un nuevo servicio: ' . date_eng2esp_1($data_lavador->fecha) . ' ' . $data_lavador->hora . ' Folio:' . $dataservicio['folio_mostrar'];
					// $tel_lavador = $this->getTelLavador($data_lavador->id_lavador);
					// sms_general($tel_lavador, $mensaje);

					redirect('xehos/pago/' . encrypt($servicio_id));
				}
			}
		}
		$this->blade->render('app/servicio/servicios', $data);
	}

	public function calificacion($id_servicio = '')
	{
		if ($id_servicio == '') {
			redirect('xehos/listaservicios');
			die();
		}

		$data['id_servicio'] = decrypt($id_servicio);
		// dd($data);
		$this->blade->render('app/servicio/calificacion', $data);
	}

	public function listaserviciosrealizados()
	{
		$servicios = $this->Servicio_model->listadoservicios([
			'id_usuario' => $this->session->userdata('usuario_id'),
			'realizados' => 1
		]);

		// dd($this->db->last_query());
		$data['servicios'] = isset($servicios) && count($servicios) > 0 ? $servicios : [];
		$this->blade->render('app/servicio/listaservicios_realizados', $data);
	}

	public function listaservicios()
	{
		$servicios = $this->Servicio_model->listadoservicios(
			[
				'id_usuario' => $this->session->userdata('usuario_id'),
				'activos' => 1
			]
		);

		$data['servicios'] = isset($servicios) && count($servicios) > 0 ? $servicios : [];
		$this->blade->render('app/servicio/listaservicios', $data);
	}

	public function pago($id_servicio = '')
	{
		if ($id_servicio == '') {
			redirect('xehos/listaservicios');
			die();
		}

		$id_servicio =  decrypt($id_servicio);
		$data_servicio = $this->Servicio_model->listadoservicios(['servicio_id' => $id_servicio]);
		$costos = $this->Servicio_model->getprecioservicio($id_servicio);

		$servicio = getFirstFromArray($data_servicio);
		$data_costo = getFirstFromArray($costos);

		$metodo_pago = $this->Crud_model->buscarTodos('cat_metodo_pago');
		$data['metodo_pago'] = $metodo_pago;
		$data['id_servicio'] = $id_servicio;
		// if(empty($servicio->id_paypal)){
		// 	redirect('xehos/listaservicios');
		// 	die();
		// }
		$data['info_servicio'] = $servicio;
		$data['costos'] = $data_costo;

		$validar_auto = getFirstFromArray($this->Servicio_model->fechaEntregaUnidad($servicio->placas));
		$data['seguro_lluvia'] = 0;
		if ($validar_auto->fecha_entrega_unidad != '') {
			$fecha_seguro_lluvia = date("Y-m-d H:i", strtotime($validar_auto->fecha_entrega_unidad . "+ 1 days"));
			if ($fecha_seguro_lluvia >= date("Y-m-d: H:i") && $servicio->id_servicio == 1) {
				$data['seguro_lluvia'] = 1;
			}
		}



		return $this->blade->render('app/paypal/paypal', $data);
	}


	public function miperfil()
	{
		$this->load->model('Crud_model');
		// $misautos = $this->Autos_model->getAutosByUsurio($this->session->userdata('usuario_id'));
		// $this->session->userdata('usuario_id')
		$perfildata = $this->Crud_model->getById('usuarios', $this->session->userdata('usuario_id'));
		// $ubicacion_usuario = $this->Crud_model->getwhere('ubicacion_usuario', 'id_usuario', $this->session->userdata('usuario_id'));
		// $ubicacion = getFirstFromArray($ubicacion_usuario);
		$estados = $this->Crud_model->buscarTodos('estados');
		$data['usuario'] = getFirstFromArray($perfildata);
		$data['estados'] = $estados;
		// $data['ubicacion_usuario'] = $ubicacion;
		// dd($ubicacion);
		// $cat_municipios = $this->Crud_model->getwhere('cat_municipios', 'id', $ubicacion->id_municipio);
		// $data['cat_municipios'] = $cat_municipios;
		$this->blade->render('app/perfil/miperfil', $data);
	}

	public function editarubicacionusuario()
	{
		$this->load->model('Crud_model');
		$ubicacion_usuario = $this->Crud_model->getwhere('ubicacion_usuario', 'id_usuario', $this->session->userdata('usuario_id'));
		$ubicacion_usuario = getFirstFromArray($ubicacion_usuario);
		$estados = $this->Crud_model->buscarTodos('estados');
		$data['estados'] = $estados;
		$data['editar'] = true;
		$data['ubicacion_usuario']  = $ubicacion_usuario;

		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
		$this->form_validation->set_rules('calle', 'calle', 'required');
		$this->form_validation->set_rules('colonia', 'colonia', 'required');
		$this->form_validation->set_rules('id_estado', 'estado', 'required');
		$this->form_validation->set_rules('id_municipio', 'municipio', 'required');
		$this->form_validation->set_rules('latitud', 'latitud', 'required');
		$this->form_validation->set_rules('longitud', 'longitud', 'required');

		$this->form_validation->set_message('valid_email', 'El campo {field} no es un correo valido.');
		$this->form_validation->set_message('required', 'El campo {field} es requerido.');
		if ($this->form_validation->run() == FALSE) {
			return $this->blade->render('app/perfil/editar_ubicacion', $data);
		} else {

			$ubicacion['numero_int'] = !empty($this->input->post('numero_int')) ? $this->input->post('numero_int') : null;
			$ubicacion['numero_ext'] = !empty($this->input->post('numero_ext')) ? $this->input->post('numero_ext') : null;
			$ubicacion['colonia'] = !empty($this->input->post('colonia')) ? $this->input->post('colonia') : null;
			$ubicacion['id_estado'] = !empty($this->input->post('id_estado')) ? $this->input->post('id_estado') : null;
			$ubicacion['id_municipio'] = !empty($this->input->post('id_municipio')) ? $this->input->post('id_municipio') : null;
			$ubicacion['latitud'] = !empty($this->input->post('latitud')) ? $this->input->post('latitud') : null;
			$ubicacion['longitud'] = !empty($this->input->post('longitud')) ? $this->input->post('longitud') : null;
			$ubicacion['calle'] = !empty($this->input->post('calle')) ? $this->input->post('calle') : null;
			$ubicacion['updated_at'] = date('Y-m-d');

			$updated = $this->Crud_model->update(
				'ubicacion_usuario',
				'id',
				$ubicacion_usuario->id,
				$ubicacion
			);

			if ($updated == 1) {
				redirect('/xehos/miperfil');
			} else {
				$this->session->set_flashdata('error_update', 'Parametros incorrectos');
				return $this->blade->render('app/perfil/editar_ubicacion', $data);
			}
		}

		$this->blade->render('app/perfil/editar_ubicacion', $data);
	}

	public function autos_nuevos()
	{

		$this->blade->render('app/autosnuevos');
	}

	public function autos_seminuevos()
	{

		$this->blade->render('app/autosseminuevos');
	}

	public function buzonnotificaciones()
	{
		$this->blade->render('app/notificaciones/buzon', [
			'telefono' => $this->session->userdata('telefono'),
			'nombre_usuario' => $this->session->userdata('nombre')
		]);
	}

	public function  contactos()
	{
		$this->load->library('chaton');

		$operadores = $this->db->select('adminNombre as nombre, telefono, adminEmail as email')->where('telefono !=', null)->get('admin')->result();
		$lavadores = $this->db->where('lavadorTelefono !=', null)->get('lavadores')->result();
		$telefono = $this->session->userdata('telefono');
		$dataNotificacion = $this->chaton->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones/' . $telefono, false, false);
		$notificaciones = json_decode($dataNotificacion)->data;

		$listadolavadores = [];
		foreach ($lavadores as $lavador) {
			$lavador->notificacion = false;
			foreach ($notificaciones as $notify) {
				if ($notify->status != 1) {
					if ($notify->celular2 == $lavador->lavadorTelefono) {
						$lavador->notificacion = true;
					}
				}
			}
			array_push($listadolavadores, $lavador);
		}

		$listadooperadores = [];
		foreach ($operadores as $operador) {
			$operador->notificacion = false;
			foreach ($notificaciones as $notify) {
				if ($notify->status != 1) {
					if ($notify->celular2 == $operador->telefono) {
						$operador->notificacion = true;
					}
				}
			}
			array_push($listadooperadores, $operador);
		}
		$data['lavadores'] = $listadolavadores;
		$data['operadores'] = $listadooperadores;
		$this->blade->render('app/notificaciones/contactos', $data);
	}

	public function  chat()
	{

		$this->load->library('chaton');

		$telefono1 = $this->session->userdata("telefono");
		$telefono2 = $this->input->get('telefono_destinatario');

		$data['telefono_remitente'] = $telefono1;
		$data['telefono_destinatario'] = $telefono2;
		$data['remitente'] = $this->chaton->getUserTelefono($telefono1);
		$data['destinatario'] = $this->chaton->getUserTelefono($telefono2);

		$data['telefono_panel'] = $telefono1;
		$data['telefono_destinatario'] = $this->input->get('telefono_destinatario');
		$this->blade->render('app/notificaciones/chat_webView', $data);
	}

	public function paypaltest()
	{
		$this->blade->render('app/paypal/paypal');
	}

	public function editarperfil()
	{
		$this->load->model('Crud_model');
		$perfildata = $this->Crud_model->getById('usuarios', $this->session->userdata('usuario_id'));
		$data['usuario'] = getFirstFromArray($perfildata);

		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
		$this->form_validation->set_rules('nombre', 'nombre', 'required');
		$this->form_validation->set_rules('apellido_paterno', 'apellido paterno', 'required');
		$this->form_validation->set_rules('apellido_materno', 'apellido materno', 'required');
		$this->form_validation->set_rules('telefono', 'telefono', 'required|exact_length[10]');
		$this->form_validation->set_rules('email', 'correo electrónico', 'required|valid_email');

		$this->form_validation->set_message('exact_length', '{field} debe tener {param} caracteres.');
		$this->form_validation->set_message('valid_email', 'El campo {field} no es un correo valido.');
		$this->form_validation->set_message('required', 'El campo {field} es requerido.');
		if ($this->form_validation->run() == FALSE) {
			return $this->blade->render('app/perfil/editarperfil', $data);
		} else {


			$updated = $this->Crud_model->update(
				'usuarios',
				'id',
				$this->session->userdata('usuario_id'),
				[
					'email' => $this->input->post('email'),
					'nombre' => strtoupper($this->input->post('nombre')),
					'apellido_paterno' => strtoupper($this->input->post('apellido_paterno')),
					'apellido_materno' => strtoupper($this->input->post('apellido_materno')),
					'telefono' =>  $this->input->post('telefono')
				]
			);

			if ($updated == 1) {
				redirect('/xehos/miperfil');
			} else {
				$this->session->set_flashdata('login_error', 'Parametros incorrectos');
				return $this->blade->render('app/perfil/editarperfil', $data);
			}
		}
	}

	public function v_horarios()
	{
		$data['fecha_apartir'] = '';
		$estados = $this->db->where('activo', 1)->get('estados')->result();
		$data['id_estado'] = form_dropdown('id_estado', array_combos($estados, 'id', 'estado', TRUE), '', 'class="form-control" id="id_estado" onchange="onchange_estado()"');
		$data['id_sucursal'] = form_dropdown('id_sucursal', '', '', 'class="form-control busqueda" id="id_sucursal"');
		$this->blade->render('app/admin/generar_horarios', $data);
	}
	public function getFechaPartir()
	{
		//Obtener la última fecha
		$q = $this->db->select('fecha')->order_by('fecha', 'desc')->where('id_sucursal', $_POST['id_sucursal'])->limit(1)->get('horarios_lavadores');
		if ($q->num_rows() == 1) {
			$nuevafecha = strtotime('+1 day', strtotime($q->row()->fecha));
			$fecha = date('Y-m-d', $nuevafecha);
		} else {
			$nuevafecha = strtotime('+1 day', strtotime(date('Y-m-d')));
			$fecha = date('Y-m-d', $nuevafecha);
		}
		echo json_encode(array('fecha' => $fecha));
	}

	public function generarhorariosAux()
	{
		ini_set('max_execution_time', 300);
		if ($this->input->post('fecha_apartir') >= $this->input->post('fecha_hasta')) {
			echo -1;
			die();
		}
		$lavadores = $this->db->where('id_sucursal', $_POST['id_sucursal'])->get('lavadores')->result();
		if (count($lavadores) == 0) {
			echo -2;
			die();
		}
		$random = random(10);
		$contador = 0;
		foreach ($lavadores as $key => $value) {
			$contador++;
			$fecha = $this->input->post('fecha_apartir');
			while ($fecha < $this->input->post('fecha_hasta')) {
				$time = '08:00';
				$contador = 0;
				while ($contador < 11) {
					$fecha_parcial = explode('-', $fecha);
					$aux = array(
						'hora' => $time,
						'id_lavador' => $value->lavadorId,
						'fecha_creacion' => date('Y-m-d'),
						'dia' => $fecha_parcial[2],
						'mes' => $fecha_parcial[1],
						'anio' => $fecha_parcial[0],
						'fecha' => $fecha,
						'ocupado' => 0,
						'activo' => 1,
						'random' => $random,
						'id_sucursal' => $_POST['id_sucursal']

					);
					$timestamp = strtotime($time) + 60 * 60;
					$time = date('H:i', $timestamp);
					$contador++;
					$this->db->insert('horarios_lavadores', $aux);
				}
				$nuevafecha = strtotime('+1 day', strtotime($fecha));
				$fecha = date('Y-m-d', $nuevafecha);
			}
		}

		echo $contador;
	}

	public function evidencia($id_servicio = '')
	{
		if ($id_servicio == '') {
			redirect('xehos/listaservicios');
			die();
		}
		$this->load->library('curl');
		$id_servicio =  decrypt($id_servicio);
		$request = $this->curl->curlPost('https://hexadms.com/xehos/index.php/inventario/api/recuperar_evidencia', [
			'id_servicio' => $id_servicio
		], true);

		$request_decoded = json_decode($request);
		$data['evidencia'] = isset($request_decoded) && count($request_decoded) > 0 ? $request_decoded : [];
		$this->blade->render('app/servicio/detalle_evidencia', $data);
	}

	public function ubicacionlavador($id = '')
	{
		if ($id == '') {
			redirect('xehos/listaservicios');
			die();
		}
		$id_servicio = decrypt($id);
		$data_servicio = $this->Crud_model->getwhere('servicio_lavado', 'id', $id_servicio);
		$servicio = getFirstFromArray($data_servicio);
		$data['id_servicio'] = $id_servicio;
		$data['info_servicio'] = $servicio;
		$this->blade->render('app/servicio/mapa_lavador', $data);
	}

	public function getTelLavador($lavadorId = '')
	{
		$q = $this->db->where('lavadorId', $lavadorId)->get('lavadores');
		if ($q->num_rows() == 1) {
			return $q->row()->lavadorTelefono;
		} else {
			return '';
		}
	}
}
