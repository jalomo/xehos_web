@layout('template_login/layoutlogin')
@section('styles')
@endsection

@section('contenido')
<div id="layoutAuthentication_content">
    <main>
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-lg-7">
                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                        <div class="card-header carheader-login">
                            <h3 class="text-center font-weight-light my-4">
                                <img class="img-login img-responsive" src="{{base_url('assets/imgs/xehoslogo.png')}}" alt="">
                            </h3>
                        </div>
                        <div class="card-body">
                            <form id="frmregistro" action="">
                            @include('registro/datos_usuario')
                            <hr>
                            <div class="form-row mb-4">
                                <div class="col-md-12 text-center">
                                    <h3>Datos del vehículo</h3>
                                </div>
                            </div>
                            <div class="form-row">
                                @include('registro/datos_vehiculo')
                            </div>
                            <div class="col-md-12">
                                <?php if (!empty($this->session->flashdata('registro_error'))) : ?>
                                    <div class="alert alert-danger" role="alert">
                                        @if (!empty($this->session->flashdata('registro_error')))
                                        {{ $this->session->flashdata('registro_error'); }}
                                        @endif
                                    </div>
                                <?php endif; ?>

                                @if (!empty($this->session->flashdata('registro_exitoso')))
                                <div class="alert alert-success" role="alert">
                                    {{ $this->session->flashdata('registro_exitoso'); }}
                                </div>
                                @endif
                            </div>
                            <div class="form-group mt-4 mb-0">
                                <button type="button" onclick="handleregistro()" class="btn btn-success btn-block">
                                    Registrarse
                                </button>
                            </div>
                            </form>
                        </div>
                        <div class="card-footer text-center">
                            <div class="small"><a href="{{ base_url('login') }}">Ir a login</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection

@section('scripts')
<script src="{{ site_url('assets/js/sweetalert/dist/sweetalert.min.js') }}" crossorigin="anonymous" ></script>
<script src="{{ site_url('assets/js/registro/registro.js') }}"></script>
@endsection