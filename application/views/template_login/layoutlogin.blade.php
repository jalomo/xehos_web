<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Xehos - autolavado</title>
        <script>
            var site_url = "{{ site_url() }}";
            var base_url = "{{ base_url() }}";
        </script>
        @include('template/scriptsheader')
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            @yield('contenido')
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted"> <i class="fas fa-car-side"></i> Xehos Autolavado</div>
                            <div>
                                <a target="_blank" href="https://xehos.com/">Página principal</a>
                                &middot;
                                <a target="_blank" href="https://xehos.com/#contact">Contacto</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        @include('template/scriptsfooter')
        @yield('scripts')
    </body>
</html>
