@layout('template/layout')
@section('styles')

@endsection
@section('breadcrumb')
pago
@endsection
@section('titulo')
Forma de pago
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12">
        <h2>Detalle de venta </h2>
    </div>
    <div class="col-md-12">
        <h2>Total: <small class="text-danger" id="calcular_total">$ {{isset($costos) ? $costos->precio : ''}}</small></h2>
        <h4>Subtotal: <small class="text-primary">$ {{isset($costos) ? $costos->precio : ''}}</small></h4>
        <h5>Descuento: <small class="text-success" id="txt_descuento">0 %</small></h5>
        <input type="hidden" id="total" name="total" value="{{ isset($costos) ? $costos->precio : '' }}">
        <input type="hidden" id="subtotal" name="subtotal" value="{{ isset($costos) ? $costos->precio : '' }}">
        <input type="hidden" id="descuento" name="descuento" value="0">
        <input type="hidden" id="id_cupon" name="id_cupon" value="">
        <input type="hidden" id="user_id" name="user_id" value="{{ !empty($this->session->userdata('usuario_id')) ? $this->session->userdata('usuario_id') : '' }}">
        
        <input type="hidden" id="id_servicio" name="id_servicio" value="{{ isset($id_servicio) ? $id_servicio : '' }}">
    </div>
    @if ($seguro_lluvia == 0)
    <div class="col-md-6">
        <div class="alert alert-dark mt-4" role="alert">
            Seguro de lluvia valido en servicio Express por 24hrs.
        </div>
    </div>
    @endif

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="cupon">Cupón de descuento.</label>
                    <input type="text" id="cupon" name="cupon" class="form-control" value="">
                </div>
                <div id="div_response_cupon"> </div>
            </div>
            <div class="col-md-4 mt-4 mb-4 text-right">
                <button onclick="validarcupon()" class="btn mt-2 btn-primary">Validar Cupón</button>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        @foreach ($metodo_pago as $metodo)
        <div class="form-check {{ $metodo->id == 3 && $seguro_lluvia == 0 ? 'disabled' :''}}">
            <input onclick="showformtipopago()" {{ $metodo->id == 3 && $seguro_lluvia == 0 ? 'disabled' :''}} class="form-check-input" type="radio" name="tipopago_radio" id="metodo_{{$metodo->id}}" value="{{$metodo->id}}">
            <label class="form-check-label" for="metodo_{{$metodo->id}}">
                {{$metodo->metodo_pago }}
            </label>
        </div>
        @endforeach
    </div>
    
</div>
<hr>
<div class="col-md-12 text-center">
    <div class="row">
        <div class="col-md-12 d-none" id="paypalcontainer">
            <h4 class="mb-4">Paypal / Debito o credito </h4>
            <div id="paypal-button-container" style="width: 100%"></div>
        </div>
        <div class="col-md-12 d-none" id="seguro_lluvia">
            <h4 class="mb-4">Seguro por lluvia </h4>

        </div>
        <div class="col-md-12 mb-4 d-none" id="terminal">
            <h4 class="mb-4">Terminal </h4>
            <div class="row">
                <div class="col-md-12 mt-4">
                    <button onclick="procesarpago()" class="btn mt-2 btn-primary"><i class="fas fa-money-bill-alt"></i> Procesar compra</button>
                </div>
            </div>
        </div>
        <div class="col-md-12 d-none" id="cupon100">
            <h4 class="mb-4">Cupón del 100 % </h4>
            <div class="row">
                <div class="col-md-12 mt-4">
                    <button onclick="procesarpago()" class="btn mt-2 btn-success"><i class="fas fa-ticket-alt"></i> Procesar compra</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://www.paypal.com/sdk/js?client-id=AW7wOfFjU5slELn4P9AsFmdjwFhB5GjlBzgQHlkNxx1n0A5hfDU2kdJ4OvIyQN1ppEZgpY5PVG_789of&currency=MXN"></script>
<script>
    
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ site_url('assets/js/servicio/paypal.js') }}"></script>
@endsection