@layout('template/layout')
@section('styles')

<style>
    * {
        box-sizing: border-box;
    }

    .chat_window {
        background-color: #fff;
        border: 2px solid #ccc;
    }

    .messages {
        position: relative;
        list-style: none;
        padding: 20px 10px 0 10px;
        margin: 0;
        height: 400px;
        overflow: scroll;
    }

    .messages .message {
        clear: both;
        overflow: hidden;
        margin-bottom: 20px;
        transition: all 0.5s linear;
        opacity: 0;
    }

    .messages .message.left .avatar {
        background-color: #2c2929;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        float: left;
        text-align: center;
        padding-top: 20px;
        color: white;
        overflow: hidden;
        width: 60px;
    }

    .messages .message.left .text_wrapper {
        background-color: #f1f1f1;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        margin-left: 20px;
    }

    .messages .message.left .text_wrapper::after,
    .messages .message.left .text_wrapper::before {
        right: 100%;
        border-right-color: #f1f1f1;
    }

    .messages .message.left .text {
        color: #2c2929;
    }

    .messages .message.right .avatar {
        background-color: #356aac;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        float: right;
        text-align: center;
        padding-top: 20px;
        color: white;
        overflow: hidden;
        width: 60px;
    }

    .messages .message.right .text_wrapper {
        background-color: #becaff;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        margin-right: 20px;
        float: right;
    }

    .messages .message.right .text_wrapper::after,
    .messages .message.right .text_wrapper::before {
        left: 100%;
        border-left-color: #becaff;
    }

    .messages .message.right .text {
        color: #45829b;
    }

    .messages .message.appeared {
        opacity: 1;
    }

    .messages .message .avatar {
        width: 60px;
        height: 60px;
        border-radius: 50%;
        display: inline-block;
    }

    .messages .message .text_wrapper {
        display: inline-block;
        padding: 20px;
        border-radius: 6px;
        width: calc(100% - 85px);
        min-width: 100px;
        position: relative;
    }

    .messages .message .text_wrapper::after,
    .messages .message .text_wrapper:before {
        top: 18px;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
    }

    .messages .message .text_wrapper::after {
        border-width: 13px;
        margin-top: 0px;
    }

    .messages .message .text_wrapper::before {
        border-width: 15px;
        margin-top: -2px;
    }

    .messages .message .text_wrapper .text {
        font-size: 18px;
        font-weight: 300;
    }

    .bottom_wrapper {
        position: relative;
        width: 100%;
        background-color: #fff;
        padding: 20px 20px;
        bottom: 0;
    }

    .bottom_wrapper .message_input_wrapper {
        display: inline-block;
        height: 50px;
        border-radius: 25px;
        border: 1px solid #bcbdc0;
        width: calc(100% - 160px);
        position: relative;
        padding: 0 20px;
    }

    .bottom_wrapper .message_input_wrapper .message_input {
        border: none;
        height: 100%;
        box-sizing: border-box;
        width: calc(100% - 40px);
        position: absolute;
        outline-width: 0;
        background-color: white;
    }

    .bottom_wrapper .send_message {
        width: 140px;
        height: 50px;
        display: inline-block;
        border-radius: 50px;
        background-color: #a3d063;
        border: 2px solid #a3d063;
        color: #fff;
        cursor: pointer;
        transition: all 0.2s linear;
        text-align: center;
        float: right;
    }

    .bottom_wrapper .sending {
        display: none;
        width: 140px;
        height: 50px;
        border-radius: 50px;
        background-color: #a3d063;
        border: 2px solid #a3d063;
        color: #fff;
        cursor: pointer;
        transition: all 0.2s linear;
        text-align: center;
        float: right;
    }

    .bottom_wrapper .send_message:hover {
        color: #a3d063;
        background-color: #fff;
    }

    .bottom_wrapper .send_message .text {
        font-size: 18px;
        font-weight: 300;
        display: inline-block;
        line-height: 48px;
    }

    .bottom_wrapper .sending .text {
        font-size: 18px;
        font-weight: 300;
        display: inline-block;
        line-height: 48px;
    }
</style>
@endsection

@section('contenido')

<div class="container-fluid panel-body">
    <h2 class="mt-2">CHAT</h2>
    <div class="row">
        <div class="col-md-12 text-right mt-3 mb-4">
            <a href="javascript:window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> &nbsp;Regresar al panel</a>
        </div>
        <div class="col-md-12">
            <div class="chat_window mb-5">
                <div id="cargando" class="spin text-center">
                    <i style="font-size:150px" class="fa fa-spinner fa-pulse fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                </div>
                <ul class="messages" id="chat">

                </ul>
                <div class="bottom_wrapper clearfix">
                    <div class="message_input_wrapper"> <input class="message_input" id="mensaje_chat" placeholder="Escribe el mensaje"></div>
                    <div class="send_message" onclick="enviarNotificacion()">
                        <div class="icon"></div>
                        <div class="text">Enviar</div>
                    </div>
                    <div class="sending">
                        <div class="icon"></div>
                        <div class="text">Enviando ...</div>
                    </div>
                </div>
                <input type="hidden" id="telefono_remitente" value="<?php echo $telefono_remitente; ?>" />
                <input type="hidden" id="telefono_destinatario" value="<?php echo $telefono_destinatario; ?>" />
                <input type="hidden" id="destinatario" value="<?php echo $destinatario; ?>" />
                <input type="hidden" id="remitente" value="<?php echo $remitente; ?>" />
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    var site_url = "{{site_url()}}";
    var notnotification = true;

    $(document).ready(function() {
        getNotificacionesChat();
        setInterval(getNotificacionesChat, 7000);
        marcarLeido();
    });

    function getNotificacionesChat() {
        var objDiv = document.getElementById("chat");
        objDiv.scrollTop = objDiv.scrollHeight;
        var url = site_url + "/ApiChat/getchat";
        let destinatario = $("#destinatario").val();
        let remitente = $("#remitente").val();
        data = {
            telefono1: $("#telefono_destinatario").val(),
            telefono2: $("#telefono_remitente").val(),
        };
        $.ajax({
            type: "GET",
            url: url,
            data: data,
            success: function(response) {
                $("#chat").html('');
                result = JSON.parse(response);
                if (result && result.length >= 1) {
                    $.each(result, function(key, value) {
                        tipo = $("#telefono_remitente").val() == value.celular ? 'left' : 'right';
                        usuario = $("#telefono_remitente").val() == value.celular ? destinatario : remitente;
                        $("#cargando").hide();
                        $("#chat").append('<li class="message ' + tipo + ' appeared">' +
                            '<div class="avatar">' + usuario + '</div>' +
                            '<div class="text_wrapper">' +
                            '<div class="text"> ' + value.mensaje + '</div>' +
                            '<div class="text-right">' + value.fecha_creacion + '</div>' +
                            '</div>' +
                            '</li>');
                    });
                    $('html, body').animate({
                        scrollTop: $(document).height()
                    }, 'slow');
                } else {
                    $("#cargando").hide();
                }
            }
        });
    }

    function marcarLeido() {
		let params = {
            celular: $("#telefono_remitente").val(),
            celular2: $("#telefono_destinatario").val()
		}
		$.ajax({
			type: "GET",
			url: site_url + "/ApiChat/apiMarcarLeidoByCelulares",
			data: params,
			success: function(result) {
				response = JSON.parse(result);
				if (response) {
                    console.log(response);
				}
			}
		});
	}

    function enviarNotificacion(_this) {
        if ($('#mensaje_chat').val().length > 1) {
            marcarLeido();
            var url = site_url + "/ApiChat/apiNotificacion";
            let data = {
                mensaje: $('#mensaje_chat').val(),
                telefono: $("#telefono_destinatario").val(),
                celular2: $("#telefono_remitente").val(),
                mensaje_respuesta: $(_this).data('respuesta')
            }
            $.ajax({
                type: "GET",
                url: url,
                data: data,
                beforeSend: function() {
                    $(".send_message").hide();
                },
                success: function(result) {
                    response = JSON.parse(result);
                    if (response) {
                        toastr.success("Mensaje eviando correctamente.");
                        $('#mensaje_chat').val('');
                        $(".send_message").show();
                        $(".sending").hide();
                    } else {
                        toastr.error('Ocurrio un error al enviar el mensaje');
                        $(".send_message").show();
                        $(".sending").hide();
                    }
                }
            });
        } else {
            toastr.error("Favor de indicar el mensaje");
        }
    }

    function obtenerFechaMostrar(fecha) {
        const dia = 2,
            mes = 1,
            anio = 0;
        split = fecha.split(' ');
        fecha = split[0].split('-');
        return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio] + ' ' + split[1];
    }
</script>
@endsection