@layout('template/layout')
@section('styles')
<style>
	.btn {
		background-color: #2487d8;
		border-radius: 0px 5px 5px 0px !important;
	}

	.form-control {
		background-color: white !important;
		border-radius: 0px !important;
	}

	.vertical-container {
		/* this class is used to give a max-width to the element it is applied to, and center it horizontally when it reaches that max-width */
		width: 98%;
		margin: 0 auto;
	}

	.vertical-container::after {
		/* clearfix */
		content: '';
		display: table;
		clear: both;
	}

	.v-timeline {
		position: relative;
		padding: 0;
		margin-top: 2em;
		margin-bottom: 2em;
	}

	.v-timeline::before {
		content: '';
		position: absolute;
		top: 0;
		left: 18px;
		height: 100%;
		width: 4px;
		background: #3d404c;
	}

	.vertical-timeline-content .btn {
		float: right;
	}

	.vertical-timeline-block {
		position: relative;
		margin: 2em 0;
	}

	.vertical-timeline-block:after {
		content: "";
		display: table;
		clear: both;
	}

	.vertical-timeline-block:first-child {
		margin-top: 0;
	}

	.vertical-timeline-block:last-child {
		margin-bottom: 0;
	}

	.vertical-timeline-icon {
		position: absolute;
		top: 0;
		left: 0;
		width: 40px;
		height: 40px;
		border-radius: 50%;
		font-size: 16px;
		border: 1px solid #3d404c;
		text-align: center;
		background: #2f323b;
		color: #ffffff;
	}

	.vertical-timeline-icon i {
		display: block;
		width: 24px;
		height: 24px;
		position: relative;
		left: 50%;
		top: 50%;
		margin-left: -12px;
		margin-top: -9px;
	}

	.vertical-timeline-content {
		position: relative;
		margin-left: 60px;
		background-color: rgba(68, 70, 79, 0.5);
		border-radius: 0.25em;
		border: 1px solid #3d404c;
	}

	.vertical-timeline-content:after {
		content: "";
		display: table;
		clear: both;
	}

	.vertical-timeline-content h2 {
		font-weight: 400;
		margin-top: 4px;
	}

	.vertical-timeline-content p {
		margin: 1em 0 0 0;
		line-height: 1.6;
	}

	.vertical-timeline-content .vertical-date {
		font-weight: 500;
		text-align: right;
	}

	.vertical-date small {
		color: #ffffff;
		font-weight: 400;
	}

	.vertical-timeline-content:after,
	.vertical-timeline-content:before {
		right: 100%;
		top: 20px;
		border: solid transparent;
		content: " ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
	}

	.vertical-timeline-content:after {
		border-color: transparent;
		border-right-color: #3d404c;
		border-width: 10px;
		margin-top: -10px;
	}

	.vertical-timeline-content:before {
		border-color: transparent;
		border-right-color: #3d404c;
		border-width: 11px;
		margin-top: -11px;
	}

	.vertical-timeline-content h2 {
		font-size: 16px;
	}


	.vertical-timeline-content {
		position: relative;
		margin-left: 60px;
		background-color: #ECF0F1;
		border-radius: 0.25em;
		border: 1px solid #ccc !important;
	}
</style>
@endsection
@section('breadcrumb')
@endsection
@section('titulo')
@endsection
@section('contenido')
<div class="row">
	<div class="col-md-10">
		<div class="panel">
			<div>
				<div id="cargando" class="spin text-center">
					<i style="font-size:150px" class="fa fa-spinner fa-pulse fa-fw"></i>
					<span class="sr-only">Loading...</span>
				</div>
				<div id="linea_tiempo" class="v-timeline vertical-container">

				</div>
				<input type="hidden" id="telefono" value="{{ $telefono }}" />
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	var site_url = "{{site_url()}}";
	$(document).ready(function() {
		getNotificacionesChat();
	});

	function getNotificacionesChat() {
		var site_url = "{{site_url()}}";
		var user_name = "{{ $nombre_usuario; }}"
		var url = site_url + "/ApiChat/getNotificaciones";
		data = {
			telefono: $("#telefono").val()
		}
		$.ajax({
			type: "GET",
			url: url,
			data: data,
			success: function(response) {
				$("#linea_tiempo").html('');
				$("#cargando").hide();
				response = JSON.parse(response);
				if (response && response.length >= 1) {
					$.each(response, function(key, value) {
						suma = (parseInt(key) + parseInt(1))
						user_title = value.usuario ? value.usuario : 'Externo';
						enviado = value.mensaje_respuesta ? '<p style="font-size:14px" class="text-info">' + '<b>' + user_name + '</b>: ' + value.mensaje_respuesta + '</p>' : '';
						tipousuario = value.tipoUsuario ? '[ ' + value.tipoUsuario + ' ]' : '';
						if (value.usuario) {
							input = '<div class="row">' +
								'<textarea type="text" placeholder="Responder mensaje" id="responder_texto" class="form-control col-md-11 texto_' + value.id + '"/></textarea>' +
								'<button id="envionotificacion" type="button" onclick="enviarNotificacion(this)" data-respuesta="' + value.mensaje + '" data-id="' + value.id + '" data-celular_destinatario="' + value.celular2 + '" class="btn btn-primary col-md-1 pt-1 pb-1"><i class="fa fa-play"></i></button>' +
								'</div>'
						} else {
							input = "";
						}
						if (value.status != 1) {
							stylestatus = '';
							status = '<span style="margin-top:-10px" class="float-right"><i title="Marcar como leído" onclick="marcarLeido(' + value.id + ')" data-celular="' + value.celular + '" data-celular2="' + value.celular2 + '" class="fa fa-check-circle text-dark fa-1x ml-2"></i></span>';
						} else {
							stylestatus = 'style="opacity:0.9;"';
							status = '<span style="margin-top:-10px" class="float-right"><i title="Leído"  class="fa fa-check-circle text-success fa-1x ml-2"></i></span>';
						}
						$("#linea_tiempo").append('<div ' + stylestatus + ' class="vertical-timeline-block ' + value.id + '">' +
							'<div class="vertical-timeline-icon">' +
							'<i class="fa fa-calendar c-accent"></i>' +
							'</div>' +
							'<div class="vertical-timeline-content">' +
							'<div style="padding:5px">' +
							'<span style="margin-top:-10px" class="float-right"><i title="Eliminar notificación" onclick="eliminarNotificacion(' + value.id + ')" class="fa fa-times-circle text-danger fa-1x ml-2"></i></span>' +
							status +
							'<span class="vertical-date float-right"> ' + obtenerFechaMostrar(value.fecha_creacion) + ' </span>' +
							'<h5>' + user_title + '<small style="font-size:16px"> ' + tipousuario + '</small></h5>' +
							enviado +
							'<p style="font-size:14px">' + '<b>' + user_title + '</b>: ' + value.mensaje + '</p>' +
							'</div>' +
							'<div class="col-md-12">' +
							input +
							'</div>');
					});
				} else {
					$("#linea_tiempo").append("<h2>Sin resultados</h2>");
					$("#linea_tiempo").removeClass("v-timeline");
				}
			}
		});
	}

	function marcarLeido(id) {
		let params = {
			id: id
		}
		$.ajax({
			type: "GET",
			url: site_url + "/ApiChat/apiMarcarLeidoById",
			data: params,
			success: function(result) {
				response = JSON.parse(result);
				if (response) {
					toastr.success("Notificación marcada como leída");
					getNotificacionesChat();
				}
			}
		});
	}

	function eliminarNotificacion(id) {
		let params = {
			id: id
		}
		$.ajax({
			type: "GET",
			url: site_url + "/ApiChat/apiEliminarNotificacion",
			data: params,
			success: function(result) {
				response = JSON.parse(result);
				if (response) {
					$("." + id).remove();
					toastr.success("Notificación eliminada correctamente");
				}
			}
		});
	}

	function enviarNotificacion(_this) {

		var url = site_url + "/ApiChat/apiNotificacion";
		let id = $(_this).data('id');
		$("#envionotificacion").attr("disabled", true);
		let data = {
			mensaje: $('.texto_' + id).val(),
			telefono: $(_this).data('celular_destinatario'),
			celular2: $("#telefono").val(),
			mensaje_respuesta: $(_this).data('respuesta')
		}
		$.ajax({
			type: "GET",
			url: url,
			data: data,
			success: function(result) {
				response = JSON.parse(result);
				if (response) {
					getNotificacionesChat();
					$('#responder_texto').val('');
					$("#envionotificacion").attr("disabled", false);
					toastr.success("Notificación enviada correctamente");
				} else {
					$("#envionotificacion").attr("disabled", false);
				}
			}
		});
	}

	function obtenerFechaMostrar(fecha) {
		const dia = 2,
			mes = 1,
			anio = 0;
		split = fecha.split(' ');
		fecha = split[0].split('-');
		return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio] + ' ' + split[1];
	}
</script>
@endsection