@layout('template/layout')
@section('contenido')

<div class="container-fluid panel-body">
	<h2 class="mt-1">Directorio de contactos</h2>
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="tab" href="#clientes">Operadores</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#lavadores">Lavadores</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div id="clientes" class="container tab-pane active"><br>
			<h3>Listado de Operadores</h3>
			<div class="row">
				<?php
				if (isset($operadores) && $operadores) {
					foreach ($operadores as $operador) { ?>
						<div class="col-md-3 mt-3">
							<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9; min-height:160px">
								<div style="padding:10px" class="">
									<div class="float-right">
										<button class="btn btn-default btn-lg" style="background-color:#f6f7f8;" title="Comenzar chat" class="btn btn-primary" onclick="gotoChat(this)" data-telefono="{{ $operador->telefono }}"><i <?php echo $operador->notificacion ? 'style="color:#17a2b8"' : 'style="color:#22ae22"'; ?> class="fa fa-comment fa-2x"></i></button>
									</div>
									<i class="fa fa-user-circle fa-3x"></i>
									{{ $operador->nombre}}<br />
									<p>
										<b>Email: </b> {{ strtolower($operador->email) }}<br />
										<b>Teléfono: </b> {{ $operador->telefono }}<br />
									</p>
									<?php if ($operador->notificacion == true) { ?>
										<span class="text-info text-right bold"><b>Chat pendiente</b></span>
									<?php } ?>
								</div>
							</div>
						</div>
				<?php
					}
				} ?>
			</div>
		</div>

		<div id="lavadores" class="container tab-pane fade"><br>
			<h3>Listado de lavadores</h3>
			<div class="row">
				<?php
				if (isset($lavadores) && $lavadores) {
					foreach ($lavadores as $lavador) {  ?>
						<div class="col-md-3 mt-3">
							<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9; min-height:160px">
								<div style="padding:10px" class="">
									<div class="float-right">
										<button class="btn btn-default btn-lg" style="background-color:#f6f7f8;" title="Comenzar chat" class="btn btn-primary" onclick="gotoChat(this)" data-telefono="{{ $lavador->lavadorTelefono }}"><i <?php echo $lavador->notificacion ? 'style="color:#17a2b8"' : 'style="color:#22ae22"'; ?> class="fa fa-comment fa-2x"></i></button>
									</div>
									<i class="fa fa-user-circle fa-3x"></i>
									{{ $lavador->lavadorNombre}}<br />
									<p>
										<b>Email: </b> {{ strtolower($lavador->lavadorEmail) }}<br />
										<b>Teléfono: </b> {{ $lavador->lavadorTelefono }}<br />
									</p>
									<?php if ($lavador->notificacion == true) { ?>
										<span class="text-info text-right bold"><b>Chat pendiente</b></span>
									<?php } ?>
								</div>
							</div>
						</div>
				<?php
					}
				} ?>
			</div>
		</div>
	</div>
</div>
@endsection

<script type="text/javascript">
	var site_url = "{{site_url()}}"

	function gotoChat(_this) {
		window.location.href = site_url + '/xehos/chat?telefono_destinatario=' + $(_this).data('telefono')
	}
</script>