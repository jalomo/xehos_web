@layout('template/layout')

@section('styles')
<link href="{{ site_url('assets/js/timepicker/bootstrap-datetimepicker.css') }}" rel="stylesheet">
@endsection
@section('contenido')
<form id="frm">
    <input type="hidden" name="fecha_apartir" id="fecha_apartir" value="{{ $fecha_apartir }}">
    <div class="row">
        <div class="col-sm-3">
            <label class="control-label mb-1">Estado</label>
            {{ $id_estado }}
            <span class="error error_id_estado"></span>
        </div>
        <div class="col-sm-3">
            <label class="control-label mb-1">Sucursal</label>
            {{ $id_sucursal }}
            <span class="error error_id_sucursal"></span>
        </div>
        <div class="col-sm-3 col-sm-offset-1">
            <label for="">A partir de</label>
            <input required="" type="text" disabled="" class="form-control" name="cita" id="cita" value="{{ date_eng2esp_1($fecha_apartir) }}">
        </div>
        <div class='col-sm-3'>
            <label for="">Selecciona la fecha fin</label>
            <input id="fecha_hasta" name="fecha_hasta" type='date' class="form-control" value="" required="" />
            <span class="error_fecha"></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <br>
            <button id="generar" class="btn btn-success">Generar</button>
        </div>
    </div>
</form>
@endsection
@section('scripts')
<script src="{{ site_url('assets/js/timepicker/general.js') }}"></script>
<script src="{{ site_url('assets/js/timepicker/moment.js') }}"></script>
<script src="{{ site_url('assets/js/timepicker/bootstrap-datetimepicker.js') }}"></script>
<script>
    const onchange_estado = () => {
        let id_estado = document.getElementById('id_estado').value;
        let url = site_url + "ajaxRequestServicio/ajax_getmunicipioByEstado";

        $("#id_municipio, #id_sucursal").empty();
        $("#id_municipio, #id_sucursal").append("<option value=''>-- Selecciona --</option>");
        // $("#id_municipio, #id_sucursal").attr("disabled", true);
        $("#id_municipio, #id_sucursal").removeAttr("disabled");
        !id_estado && false
        getSucursales();
    }

    const getSucursales = () => {
        let id_estado = document.getElementById('id_estado').value;
        let url = site_url + "ajaxRequestServicio/ajax_getsucursales/" + id_estado;
        !id_estado && false
        myfunction.ajaxpost(url, {
            id_estado
        }, function(data) {
            $("#id_sucursal").empty();
            $("#id_sucursal").append("<option value=''>-- Selecciona --</option>");
            $("#id_sucursal").removeAttr("disabled");
            if (data && data.length > 0) {
                data.map(item => $("#id_sucursal").append("<option value= '" + item.id + "'>" + item.sucursal + "</option>"))
            } else {
                $("#id_sucursal").empty();
                $("#id_sucursal").append("<option value=''> No se encontraron datos</option>");
            }
        })
    }


    $("body").on("click", '#generar', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        if ($("#fecha_hasta").val() == '' || $("#fecha_apartir").val() == '') {
            alert("Es necesario ingresar los dos campos");
            // ("")
        } else {
            // alert("¿Está seguro de generar horarios?", callbackGenerar, "", "Confirmar", "Cancelar");
            // callbackGenerar
            // callback(callbackGenerar())
            callbackGenerar()
        }
    });

    function callbackGenerar() {
        var url = site_url + "/xehos/generarhorariosAux";
        myfunction.ajaxpost(url, $("#frm").serialize(), function(data) {
            return console.log(data)
            if (result == -1) {
                alert('La fecha fin debe ser mayor');
            } else if (result == -2) {
                alert('No existen lavadores asignados a esa sucursal');
            } else {
                window.location.reload();

            }
        })
    }
    const getFecha = () => {
        // return console.log("aqui ")
        var url = site_url + "/xehos/getFechaPartir";
        myfunction.ajaxpost(url, {
            id_sucursal: $("#id_sucursal").val()
        }, function(result) {
            $("#fecha_apartir").val(result.fecha)
            $("#cita").val(result.fecha)
            $('#datetimepicker1').datetimepicker({
                minDate: result.fecha,
                format: 'DD/MM/YYYY',
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                locale: 'es'
            });
        })
    }
    $("#id_sucursal").on("change", getFecha);
</script>
@endsection