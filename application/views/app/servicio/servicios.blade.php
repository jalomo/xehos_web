@layout('template/layout')
@section('styles')
<link rel="stylesheet" href="{{ site_url('assets/css/mapa.css') }}">
@if (empty($id_servicio))
<script src="{{ site_url('assets/js/servicio/mapa_current_ubicacion.js') }}"></script>
@else
<script src="{{ site_url('assets/js/servicio/mapa_custom_ubicacion.js') }}"></script>
@endif
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM&callback=initMap&libraries=places" async defer></script> 
@endsection
@section('breadcrumb') 
	{{ !empty($id_servicio) ? 'Detalle de servicio': "Agendar Cita" }} 
@endsection
@section('titulo') 
	{{ !empty($id_servicio) ? 'Detalle de servicio': "Agendar Cita" }} 
@endsection
@section('contenido')
	<div class="row">
		<?php echo form_open(!empty($id_servicio) ? "xehos/servicios/". encrypt($id_servicio) : 'xehos/servicios',['class'=>'col-md-12']); ?>
			<div class="row">
				<div class="col-md-12">
					<?php if (!empty($this->session->flashdata('registro_error'))) : ?>
						<div class="alert alert-danger" role="alert">
							@if (!empty($this->session->flashdata('registro_error')))
							{{ $this->session->flashdata('registro_error'); }}
							@endif
						</div>
					<?php endif; ?>
				
					@if (!empty($this->session->flashdata('registro_exitoso')))
					<div class="alert alert-success" role="alert">
						{{ $this->session->flashdata('registro_exitoso'); }}
					</div>
					@endif
				</div>
				<div class=" col-md-8">
					<div class="form-group">
						<label class="small mb-1" for="nombre">Auto *</label>
						<select onchange="cargarvehiculoinfo()"  class="form-control "  name="id_auto" id="id_auto">
							<option value="">Seleccionar</option>
							@foreach ($misautos as $item)
								@if (isset($info_servicio->id_auto) && $item->id == $info_servicio->id_auto)
									<option selected value="{{ $item->id }}">{{  $item->marca .' - '. $item->modelo }}</option>
									@else
									<option <?php echo set_select('id_auto',  $item->id); ?> value="{{ $item->id }}">{{  $item->marca .' - '. $item->modelo }}</option>
								@endif
							@endforeach
						</select>
						<?php echo form_error('id_auto', '<div class="text-danger">', '</div>'); ?>
					</div>
				</div>
				<div class="col-md-4 mt-4">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#automodal">
						Nuevo vehículo
					</button>
				</div>	
				<div id="infovehiculo" class="col-md-12 d-none">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
							  	<label class="small mb-2" for="nombre">Color *</label>
								<input readonly class="form-control" type="text" value="" name="info_color" id="info_color">
							</div>
						  </div>
						  <div class="col-md-4">
							<div class="form-group">
							  <label for="">Placas *</label>
							  <input readonly class="form-control" type="text" value="" name="info_placas" id="info_placas">
							</div>
						  </div>
						  <div class="col-md-4">
							<div class="form-group">
							  <label class="small mb-2" for="nombre">Modelo *</label>
							  <input readonly class="form-control" type="text" value="" name="info_modelo" id="info_modelo">
							</div>
						  </div>
						  <div class="col-md-4">
							<div class="form-group">
							  <label class="small mb-2" for="nombre">Marca *</label>
							  <input readonly class="form-control" type="text" value="" name="info_marca" id="info_marca">
							</div>
						  </div>
						  <div class="col-md-4">
							<div class="form-group">
							  <label class="small mb-2" for="anio">Año *</label>
							  <input readonly class="form-control" type="text" value="" name="info_anio" id="info_anio">
							</div>
						  </div>
						  <div class="col-md-4">
							<div class="form-group">
							  <label class="small mb-2" for="anio">Numero de serie *</label>
							  <input readonly class="form-control" type="text" value="" name="info_num_serie" id="info_num_serie">
							</div>
						  </div>
					</div>
				</div>
				<div class="col-md-12 mt-4"></div>
				<div class="col-md-4">
					<fieldset>
						<div class="form-group">
							<h3 for="nombre">¿ Necesitas facturar? </h3>
						</div>
					</fieldset>
				</div>
				<div class="col-md-8">
					<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalfacturacion">
						Agregar datos de facturación
					</button>
				</div>
				<div id="infodatosfacturacion" class="col-md-12 d-none">
					@include('app/servicio/datos_facturacion')
				</div>
				<div class="col-md-12 mt-4"></div>
				<div class="col-md-12">
					<fieldset>
						<div class="form-group">
							<label for="nombre">Usuario</label>
							<input readonly type="text" id="nombre" class="form-control" value="{{ $this->session->userdata('nombre').' '.$this->session->userdata('apellido_paterno') }}">
							<input type="hidden" id="id_usuario" value="{{ $this->session->userdata('usuario_id') }}" name="id_usuario">
							<input type="hidden" id="id_servicio" value="{{ $id_servicio }}" name="id_servicio">
						</div>
					</fieldset>
				</div>
			</div>
			
			<div class="row mt-4 mb-4 d-none">
				<div class="col-md-12" id="loading_map">
					<h3>Cargando mapa  <i class="fas fa-spinner fa-pulse"></i></h3>
				</div>
			</div>
			
			<div id="map_container" class="row">
				<h2 class="col-md-12">
					@if (!empty($id_servicio))
						@if (!empty($data['info_servicio']->latitud_lavador) && !empty($data['info_servicio']->longitud_lavador))
							Ubicación del servicio
						@else	
							Esperando confirmacion lavador
						@endif
						
					@else
						Mi Ubicación
					@endif
				</h2>
				<div class="col-md-12 mt-4 mb-4">
					<div class="pac-card" id="pac-card">
						<div>
							<div id="title">Ubicación</div>
							<div id="strict-bounds-selector" class="pac-controls">
							</div>
						</div>
						<div id="pac-container">
							<input id="pac-input" type="text" placeholder="Ingresa direccion" />
						</div>
						</div>
					<div style="width: 100%;height: 400px" id="mapa"></div>
					<div id="infowindow-content">
						<img src="" width="16" height="16" id="place-icon" />
						<span id="place-name" class="title"></span><br />
						<span id="place-address"></span>
					</div>
				</div>
			</div>
			<div class="row">
				@include('app/servicio/datos_ubicacion')
			</div>
			<hr>
			<div class="row mt-4">
				<div class="col-md-12 mb-2">
					<h2>
						@if (!empty($id_servicio))
							Datos de la cita
						@else
							Registrar cita
						@endif
					</h2>
				</div>
				@include('app/servicio/datos_cita')
			</div>
			@if (empty($id_servicio))
				<div class="col-md-12 text-right">
					<button type="submit" id="btnservicio" class="btn btn-success col-md-3">
						<i class="fas fa-money-bill"></i> Proceder a pagar
					</button>
				</div>
				@else
				<div class="col-md-12 text-right">
					<button type="submit" id="btnservicio" class="btn btn-primary col-md-3">
						<i class="fa fa-edit"></i> Editar informacion
					</button>
				</div>
			@endif
		</form>
	</div>
@endsection

@include('app/modal_alta_auto')
@include('app/modales/modal_facturacion')
@section('scripts')


<script src="{{ site_url('assets/js/servicio/servicio.js') }}"></script>

@endsection