
	<div class="col-md-4">
		<div class="form-group">
			<label for="fecha">Fecha</label>
			<select disabled onchange="onchange_horario()" class="form-control" name="fecha" id="fecha" >
				<option disabled value="">Seleccionar ...</option>
			</select>
			<?php echo form_error('fecha', '<div class="text-danger">', '</div>'); ?>
			<input type="hidden" name="loaded_fecha" id="loaded_fecha" value="{{ isset($data['info_servicio']->fecha_programacion)? $data['info_servicio']->fecha_programacion : '' }}">
			<input type="hidden" name="loaded_hora" id="loaded_hora" value="{{ isset($data['info_servicio']->id_horario)? $data['info_servicio']->id_horario : '' }}">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label for="nombre">Horario</label>
			<select disabled class="form-control"  name="hora" id="hora" >
			</select>
			<?php echo form_error('hora', '<div class="text-danger">', '</div>'); ?>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label for="fecha">Servicio</label>
			<select class="form-control " onchange="onchangeTiposervicio()" name="servicio" id="servicio" >
				<option value="">Seleccionar ...</option>
				@foreach ($cat_servicios as $item)
					@if (isset($data['info_servicio']) && $data['info_servicio']->id_servicio == $item->servicioId)
						<option selected value="{{ $item->servicioId }}">{{  $item->servicioNombre }}</option>	
					@else
						<option <?php echo set_select('servicio',  $item->servicioId); ?> value="{{ $item->servicioId }}">{{  $item->servicioNombre }}</option>
					@endif
				@endforeach
			</select>
			<?php echo form_error('servicio', '<div class="text-danger">', '</div>'); ?>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
			<label for="nombre">Comentarios</label>
			<textarea class="form-control " name="comentarios" id="comentarios" cols="10" rows="3">{{ isset($data['info_servicio']) ? $data['info_servicio']->comentarios : '' }}</textarea>
		</div>
	</div>
	<div class="col-md-8 mt-4 mb-4">
		<div class="alert" id="msg_servicio" role="alert">
			<h4 id="titulo_alerta" class="alert-heading text-white"></h4>
			<p id="contenido_alerta" class="text-white"></p>
		  </div>
	</div>
