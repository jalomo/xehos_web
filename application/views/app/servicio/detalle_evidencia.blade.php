@layout('template/layout')
@section('styles')
<style>
	.width-evidencia{
		max-width: 17rem !important;
		padding: 0.25rem;
		background-color: #fff;
		border: 1px solid #dee2e6;
		border-radius: 0.25rem;
	}

</style>
@endsection
@section('breadcrumb')
evidencia
@endsection
@section('titulo')
Evidencia
@endsection
@section('contenido')
<div class="row">
	@if (!empty($evidencia))
	@foreach ($evidencia as $item)
			<div class="col-md-3 text-center">
				<img src="{{$item->url}}" alt="{{$item->titulo}}" class="width-evidencia ">
			</div>
	@endforeach
	@else
	<div class="col-md-12">
		<div class="jumbotron">
			<h1 class="display-4">Hola <small>{{ $this->session->userdata('nombre') }}</small>!</h1>
			<p class="lead">No hay evidencia aún</p>
			<hr class="my-4">
			<p>El lavador aún no ha subido evidencia del servicio.</p>
		</div>
	</div>
	@endif
</div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
@endsection