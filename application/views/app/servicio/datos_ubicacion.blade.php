@if (!empty($id_servicio) && empty($data['info_servicio']->latitud_lavador) && $data['info_servicio']->id_estatus_lavado == 1)
    <div class="col-md-12">
        <div class="alert alert-warning" role="alert">
            El lavador no ha confirmado la ruta
          </div>
    </div>
@endif
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="calle">Calle</label>
            <input type="text" id="calle" name="calle"  class="form-control" value="{{ isset($data['info_ubicacion']->calle)? $data['info_ubicacion']->calle : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="numero_ext"># Exterior</label>
            <input type="text" id="numero_ext" name="numero_ext" class="form-control" value="{{ isset($data['info_ubicacion']->numero_ext)? $data['info_ubicacion']->numero_ext : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="numero_int"># Interior</label>
            <input type="text" id="numero_int" name="numero_int" class="form-control" value="{{ isset($data['info_ubicacion']->numero_int)? $data['info_ubicacion']->numero_int : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="colonia">Colonia</label>
            <input type="text" id="colonia" name="colonia" class="form-control" value="{{ isset($data['info_ubicacion']->colonia)? $data['info_ubicacion']->colonia : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="estado">Estado</label>
            <select  onchange="onchange_estado()" class="form-control" name="id_estado" id="id_estado" >
                <option value="">Seleccionar ...</option>
                @foreach ($estados as $item)
                    @if (isset($data['info_ubicacion']->id_estado) && $data['info_ubicacion']->id_estado == $item->id)
                        <option selected value="{{ $item->id }}">{{ $item->estado  }}</option>
                    @else
                        <option value="{{ $item->id }}">{{ $item->estado  }}</option>
                    @endif
                @endforeach
            </select>
            <?php echo form_error('id_estado', '<div class="text-danger">', '</div>'); ?>
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="id_municipio">Municipio</label>
            <select  onchange="" disabled class="form-control" name="id_municipio" id="id_municipio" >
                <option value="">Seleccionar ...</option>
            </select>
            <?php echo form_error('id_municipio', '<div class="text-danger">', '</div>'); ?>
            <input type="hidden" name="loaded_id_municipio" id="loaded_id_municipio" value="{{ isset($data['info_ubicacion']->id_municipio)? $data['info_ubicacion']->id_municipio : '' }}">
            <input type="hidden" name="loaded_id_sucursal" id="loaded_id_sucursal" value="{{ isset($data['info_ubicacion']->id_sucursal)? $data['info_ubicacion']->id_sucursal : '' }}">
            <input type="hidden" name="id_estatus_lavado" id="id_estatus_lavado" value="{{ isset($data['info_servicio']->id_estatus_lavado)? $data['info_servicio']->id_estatus_lavado : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="id_sucursal">Sucursal</label>
            <select  {{ !empty($id_servicio) ? 'disabled' : '' }}  onchange="onchangesucursal()" disabled class="form-control" name="id_sucursal" id="id_sucursal" >
                <option value="">Seleccionar ...</option>
            </select>
            <?php echo form_error('id_sucursal', '<div class="text-danger">', '</div>'); ?>
        </div>
    </fieldset>
</div>
<div class="col-md-4">
<fieldset>
        <div class="form-group">
        <label for="latitud">Latitud: </label>
            <input type="text" readonly id="latitud" name="latitud" class="form-control" value="{{ isset($data['info_ubicacion']->latitud)? $data['info_ubicacion']->latitud : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset>
        <div class="form-group">
            <label for="longitud">Longitud: </label>
            <input type="text" readonly  id="longitud" name="longitud" class="form-control" value="{{ isset($data['info_ubicacion']->longitud)? $data['info_ubicacion']->longitud : '' }}">
        </div>
    </fieldset>
</div>
