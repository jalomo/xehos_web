@layout('template/layout')
@section('styles')
<link href="{{ site_url('assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" crossorigin="anonymous" />
<link rel="stylesheet" href="{{ site_url('assets/fonts/flaticon/font/flaticon.css') }}">
<link rel="stylesheet" href="{{ site_url('assets/css/animate.min.css') }}">
@endsection
@section('breadcrumb')
servicios
@endsection
@section('titulo')
Servicios activos
@endsection
@section('contenido')
<div class="row">
	
	@if (count($servicios) != 0)
	<div class="col-md-12">
		<table class="table" id="tabla_servicios">
			<thead>
				<tr>
					<th scope="col">Folio</th>
					<th scope="col">Vehículo</th>
					<th scope="col">Estatus</th>
					<th class="responsive-hide" scope="col">Forma de pago</th>
					<th class="responsive-hide" scope="col">Fecha y hora servicio</th>
					<th scope="col">Entrega estimada</th>
					<th scope="col">-</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($servicios as $key=> $item)
				
				<tr>
					<th>{{isset( $item->folio_mostrar) ?  $item->folio_mostrar : ' -- '}}</th>
					<td>{{ $item->marca.' - '.$item->modelo .' '.$item->anio }}</td>
					<td>{{ $item->estatus_lavado }}</td>
					<td class="responsive-hide">{{ $item->metodo_pago }}</td>
					<td class="responsive-hide">{{ obtenerFechaEnLetra($item->fecha_programacion) .' a las: '.$item->hora_servicio }}</td>

					<td>{{ !empty($item->hora_servicio) ? obtenerFechaEnLetra($item->fecha_programacion) .' '.obtenerHoraEntregaServicio($item->fecha_servicio.' '.$item->hora_servicio) : ' - ' }}</td>
					<td>
						<div class="buttonshowhide" id="{{ "handlebuttons_".$key}}">
							<button id="eyebutton_{{ $key}}"  onclick="showhidebuttons({{ $key }})" class="btn btn-light">
								<i class="fas fa-eye"></i>
							</button>
						</div>
						<div class="buttonscontainer animate__animated pt-2 pb-2" id="{{ "buttoncontainer_".$key}}">
							@if (validareditarbtn($item->fecha_programacion.' '.$item->hora_servicio, $item->id_metodo_pago)['show'] == 1)
								<a title="editar servicio" href="{{ site_url('xehos/servicios/'.encrypt($item->id_servicio)) }}" class="btn btn-light">
									<span class="icon icon-eod">
										<i class="flaticon-info"></i>
									</span>
								</a>
							@endif

							@if ($item->id_estatus_lavado == 2)	
								<a title="Monitorear Ubicación lavador" href="{{ site_url('xehos/ubicacionlavador/'.encrypt($item->id_servicio)) }}" class="btn btn-light">
									<i class="fas fa-motorcycle"></i>
								</a>
							@endif
							<a title="Detalle del servicio" href="{{ site_url('xehos/detalleServicio/'.encrypt($item->id_servicio)) }}" class="btn btn-light">
								<span class="icon icon-eod">
									<i class="flaticon-info"></i>
								</span>
							</a>

							<a title="Documentacion" class="btn btn-light" href="https://hexadms.com/xehos/inventario/Inv_Exterior/generar_PDF/{{ encrypt($item->id_servicio)  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="documentacion">
								<span class="icon icon-eod">
									<i class="flaticon-pdf-file"></i>
								</span>
							</a>

							<a title="Evidencias" href="{{ site_url('xehos/evidencia/'.encrypt($item->id_servicio)) }}" class="btn btn-light">
								<span class="icon icon-eod">
									<i class="flaticon-play-button"></i>
								</span>
							</a>
							@if ($item->cancelado == 0 && $item->confirmado == 0)
								<button title="Cancelar cita" onclick="handlecancelar({{$item->id_servicio }})" class="btn btn-light">
									<span class="icon icon-eod">
										<i class="flaticon-cancel"></i>
									</span>
									
								</button>
							@endif
							@if ($item->confirmado == 0)
								<button title="confirmar cita" onclick="handleconfirmar({{$item->id_servicio }})" class="btn btn-light">
									<span class="icon icon-eod">
										<i class="flaticon-confirm"></i>
									</span>
								</button>
							@endif

							<a title="Gafete" class="btn btn-light" href="https://xehos.com/xehos_gafete/index.php/gafete/index/{{ $item->id_gafete  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="identificacion">
								<span class="icon icon-eod">
									<i class="flaticon-profile-user"></i>
								</span>
							</a>

							<a title="documento" class="btn btn-light" href="https://hexadms.com/xehos/index.php/{{ $item->url_documento  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="identificacion">
								<span class="icon icon-eod">
									<i class="flaticon-document"></i>
								</span>
							</a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@else
	<div class="col-md-12">
		<div class="jumbotron">
			<h1 class="display-4">Hola <small>{{ $this->session->userdata('nombre') }}</small>!</h1>
			<p class="lead">No cuentas con servicios activos.</p>
			<hr class="my-4">
			<p>Puedes agendar un servicio en la siguiente liga.</p>
			<p class="lead">
				<a class="btn btn-light btn-lg" href="{{ site_url('xehos/servicios')}}" role="button">Agendar</a>
			</p>
		</div>
	</div>
	@endif
</div>
@endsection

@section('scripts')

<script src="{{ site_url('assets/js/datatables/jquery.dataTables.min.js') }}" crossorigin="anonymous" ></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="{{ site_url('assets/js/sweetalert/dist/sweetalert.min.js') }}" crossorigin="anonymous" ></script>
<script>
	const handlecancelar = (id_servicio)=>{
		swal({
			title: "Cancelar cita",
			text: "Estás seguro que deseas cancelar la cita",
			icon: "warning",
			dangerMode: true,
			buttons: ["Cerrar", "Confirmar"],
			})
			.then((cancelar) => {
			if (cancelar) {
				let url = site_url + "ajaxRequestServicio/ajax_updateservicio";
				let data ={
					cancelado: 1
				}

				myfunction.ajaxpost(url, { id_servicio, data }, function ({status, msg}) {
					if(status == 1){
						swal("El lavador continuara el servicio", { title: "Confirmacion exitosa", icon: "success"}).then(window.location.reload());
					}else{
						swal(msg, { icon: "warning"}).then(window.location.reload());
					}
				})
			}
		});
	}

	const handleconfirmar = (id_servicio)=>{
		swal({
			title: "Confirmar cita",
			text: "Estás confirmando la cita de lavado",
			icon: "warning",
			buttons: ["Cerrar", "Confirmar"],
			})
			.then((confirmar) => {
			if (confirmar) {
				let url = site_url + "ajaxRequestServicio/ajax_updateservicio";
				let data ={
					confirmado: 1
				}

				myfunction.ajaxpost(url, { id_servicio, data }, function ({status, msg}) {
					if(status == 1){
						swal(msg, { icon: "success"}).then(window.location.reload());
					}else{
						swal(msg, { icon: "warning"}).then(window.location.reload());
					}
				})
			}
		});
	}	

	const showhidebuttons = (id)=>{

		if(!$("#buttoncontainer_" + id).hasClass("bottom-buttons")){
			$(".buttonscontainer").removeClass("animate__bounceInLeft bottom-buttons");
			$("#buttoncontainer_" + id)
			.removeClass("animate__backOutLeft")
			.addClass("animate__bounceInLeft bottom-buttons" );	

		}else{
			$(".buttonscontainer").removeClass("animate__bounceInLeft bottom-buttons");
		}
	}
</script>
@endsection
