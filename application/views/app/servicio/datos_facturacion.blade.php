<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="">Razón Social *</label>
            <input readonly class="form-control" type="text" readonly value="" name="selected_razon_social" id="selected_razon_social">
            <input type="hidden" name="id_selected_facturacion" id="id_selected_facturacion">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="">Domicilio *</label>
            <input class="form-control" readonly type="text" required value="" id="selected_domicilio">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="">Email Facturación *</label>
            <input class="form-control" readonly type="email" value="" id="selected_email_facturacion">
            <div id="msgvalidationemail" class=""> </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="small mb-2" for="nombre">CFDI *</label>
            <select readonly class="form-control " name="selected_id_cfdi" id="selected_id_cfdi">
                <option value="">Seleccionar</option>
                @foreach ($cat_cfdi as $item)
                <option value="{{ $item->id }}">{{ $item->cfdi }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="small mb-2" for="nombre">Método de Pago </label>
            <select readonly class="form-control " name="selected_id_metodo_pago" id="selected_id_metodo_pago">
                <option value="">Seleccionar</option>
                <option selected value="1">Pago en una sola exhibición</option>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="">Rfc *</label>
            <input readonly class="form-control" type="text" required value="" id="selected_rfc">
            <div id="msgvalidationrfc" class=""> </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="">Código Postal *</label>
            <input readonly class="form-control" type="number" required value="" id="selected_cp">
        </div>
    </div>
</div>