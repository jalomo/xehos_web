<div class="buttonresponsive d-sm-block d-md-none">
    <button class="btn btn-primary" style="min-width: 3rem">
        <i class="fas fa-eye"></i>
    </button>
</div>
<div class="containerbotones d-none d-sm-block">
    @if (validareditarbtn($item->fecha_programacion.' '.$item->hora_servicio, $item->id_metodo_pago)['show'] == 1)
        <a title="editar servicio" href="{{ site_url('xehos/servicios/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
            <i class="fas fa-edit"></i>
        </a>
    @endif
    
    @if ($item->id_estatus_lavado == 2)	
        <a title="Monitorear Ubicación lavador" href="{{ site_url('xehos/ubicacionlavador/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
            <i class="fas fa-motorcycle"></i>
        </a>
    @endif
    <a title="Detalle del servicio" href="{{ site_url('xehos/detalleServicio/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
        <i class="fas fa-receipt"></i>
    </a>
    
    <a title="Documentacion" class="btn btn-light" href="https://hexadms.com/xehos/inventario/Inv_Exterior/generar_PDF/{{ encrypt($item->id_servicio)  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="documentacion" style="min-width: 3rem">
        <i class="far fa-file-pdf"></i>
    </a>
    
    <a title="Evidencias" href="{{ site_url('xehos/evidencia/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
        <i class="fas fa-photo-video"></i>
    </a>
    @if ($item->cancelado == 0 && $item->confirmado == 0)
        <button title="Cancelar cita" onclick="handlecancelar({{$item->id_servicio }})" class="btn btn-light" style="min-width: 3rem">
            <i class="fas fa-ban"></i>
        </button>
    @endif
    @if ($item->confirmado == 0)
        <button title="confirmar cita" onclick="handleconfirmar({{$item->id_servicio }})" class="btn btn-light" style="min-width: 3rem">
            <i class="fas fa-calendar-check"></i>
        </button>
    @endif
    
    <a title="Gafete" class="btn btn-light" href="https://xehos.com/xehos_gafete/index.php/gafete/index/{{ $item->id_gafete  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="identificacion" style="min-width: 3rem">
        <i class="far fa-id-card"></i>
    </a>
    
    <a title="documento" class="btn btn-light" href="https://hexadms.com/xehos/index.php/{{ $item->url_documento  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="identificacion" style="min-width: 3rem">
        <i class="fas fa-file-image"></i>
    </a>
</div>
