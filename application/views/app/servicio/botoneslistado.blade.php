<?php
    var_dump($item->id_servicio);
    die();
?>
@if (validareditarbtn($item->fecha_programacion.' '.$item->hora_servicio, $item->id_metodo_pago)['show'] == 1)
    <a title="editar servicio" href="{{ site_url('xehos/servicios/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
        <span class="icon icon-eod">
            <i class="flaticon-info"></i>
        </span>
    </a>
@endif

@if ($item->id_estatus_lavado == 2)	
    <a title="Monitorear Ubicación lavador" href="{{ site_url('xehos/ubicacionlavador/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
        <i class="fas fa-motorcycle"></i>
    </a>
@endif
<a title="Detalle del servicio" href="{{ site_url('xehos/detalleServicio/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
    <span class="icon icon-eod">
        <i class="flaticon-info"></i>
    </span>
</a>

<a title="Documentacion" class="btn btn-light" href="https://hexadms.com/xehos/inventario/Inv_Exterior/generar_PDF/{{ encrypt($item->id_servicio)  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="documentacion" style="min-width: 3rem">
    <span class="icon icon-eod">
        <i class="flaticon-pdf-file"></i>
    </span>
</a>

<a title="Evidencias" href="{{ site_url('xehos/evidencia/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
    <span class="icon icon-eod">
        <i class="flaticon-play-button"></i>
    </span>
</a>
@if ($item->cancelado == 0 && $item->confirmado == 0)
    <button title="Cancelar cita" onclick="handlecancelar({{$item->id_servicio }})" class="btn btn-light" style="min-width: 3rem">
        <span class="icon icon-eod">
            <i class="flaticon-cancel"></i>
        </span>
        
    </button>
@endif
@if ($item->confirmado == 0)
    <button title="confirmar cita" onclick="handleconfirmar({{$item->id_servicio }})" class="btn btn-light" style="min-width: 3rem">
        <span class="icon icon-eod">
            <i class="flaticon-confirm"></i>
        </span>
    </button>
@endif

<a title="Gafete" class="btn btn-light" href="https://xehos.com/xehos_gafete/index.php/gafete/index/{{ $item->id_gafete  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="identificacion" style="min-width: 3rem">
    <span class="icon icon-eod">
        <i class="flaticon-profile-user"></i>
    </span>
</a>

<a title="documento" class="btn btn-light" href="https://hexadms.com/xehos/index.php/{{ $item->url_documento  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="identificacion" style="min-width: 3rem">
    <span class="icon icon-eod">
        <i class="flaticon-document"></i>
    </span>
</a>
