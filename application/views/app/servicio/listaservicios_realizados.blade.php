@layout('template/layout')
@section('styles')
<link href="{{ site_url('assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" crossorigin="anonymous" />
<link rel="stylesheet" href="{{ site_url('assets/fonts/flaticon/font/flaticon.css') }}">
<link rel="stylesheet" href="{{ site_url('assets/css/animate.min.css') }}">
@endsection
@section('breadcrumb')

@endsection
@section('titulo')
Servicios realizados
@endsection
@section('contenido')
<div class="row">
	@if (count($servicios) != 0)
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Folio</th>
					<th scope="col">Vehículo</th>
					<th scope="col">Estatus</th>
					<th class="responsive-hide" scope="col">Forma de pago</th>
					<th class="responsive-hide" scope="col">Fecha servicio</th>
					<th scope="col">Fecha y hora entrega</th>
					<th scope="col">-</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($servicios as $key=> $item)
				
				<tr>
					<th>{{isset( $item->folio_mostrar) ?  $item->folio_mostrar : ' -- '}}</th>
					<td>{{ $item->marca.' - '.$item->modelo .' '.$item->anio }}</td>
					<td>{{ $item->estatus_lavado }}</td>
					<td class="responsive-hide">{{ $item->metodo_pago }}</td>
					<td class="responsive-hide">{{ obtenerFechaEnLetra($item->fecha_programacion) }}</td>
					
					<td>{{ !empty($item->hora_servicio) ? obtenerFechaEnLetra($item->fecha_programacion) .' '. obtenerHoraEntregaServicio($item->fecha_servicio.' '.$item->hora_servicio) : ' - ' }}</td>
					<td>
						<div class="buttonshowhide" id="{{ "handlebuttons_".$key}}">
							<button id="eyebutton_{{ $key}}"  onclick="showhidebuttons({{ $key }})" class="btn btn-light">
								<i class="fas fa-eye"></i>
							</button>
						</div>
						<div class="buttonscontainer animate__animated pt-2 pb-2" id="{{ "buttoncontainer_".$key}}">
							<a title="Detalle del servicio" href="{{ site_url('xehos/detalleServicio/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
								<i class="fas fa-receipt"></i>
							</a>
							@if (!empty($item->id_estatus_lavado) && $item->id_estatus_lavado == 6 && empty($item->id_servicio_calificacion))
								<a title="Califica nuestro servicio" href="{{ site_url('xehos/calificacion/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
									<i class="fas fa-star"></i>
								</a>
							@else
								<!-- <button type="button" title="Calificado" class="btn btn-secondary" disabled style="min-width: 3rem">
									<i class="fas fa-star"></i>
								</button> -->
							@endif
							
							<a title="Documentacion" class="btn btn-light" href="https://hexadms.com/xehos/inventario/Inv_Exterior/generar_PDF/{{ encrypt($item->id_servicio)  }}" target="_blank" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="documentacion" style="min-width: 3rem">
								<span class="icon icon-eod">
									<i class="flaticon-pdf-file"></i>
								</span>
							</a>
							<a title="Evidencias" href="{{ site_url('xehos/evidencia/'.encrypt($item->id_servicio)) }}" class="btn btn-light" style="min-width: 3rem">
								<span class="icon icon-eod">
									<i class="flaticon-play-button"></i>
								</span>
							</a>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@else
	<div class="col-md-12">
		<div class="jumbotron">
			<h1 class="display-4">Hola <small>{{ $this->session->userdata('nombre') }}</small>!</h1>
			<p class="lead">No has realizado ningún servicio</p>
			<hr class="my-4">
			<p>Puedes agendar un servicio en la siguiente liga.</p>
			<p class="lead">
				<a class="btn btn-primary btn-lg" href="{{ site_url('xehos/servicios')}}" role="button">
					<i class="far fa-clock"></i> Agendar
				</a>
			</p>
		</div>
	</div>
	@endif
</div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script>
		const showhidebuttons = (id)=>{
			if(!$("#buttoncontainer_" + id).hasClass("bottom-buttons")){
				$(".buttonscontainer").removeClass("animate__bounceInLeft bottom-buttons");
				$("#buttoncontainer_" + id)
				.removeClass("animate__backOutLeft")
				.addClass("animate__bounceInLeft bottom-buttons" );	

			}else{
				$(".buttonscontainer").removeClass("animate__bounceInLeft bottom-buttons");
			}
		}
</script>
@endsection