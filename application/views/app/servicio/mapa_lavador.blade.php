@layout('template/layout')
@section('styles')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM&callback=initMap&libraries=places" async defer></script> 
<link rel="stylesheet" href="{{ site_url('assets/css/mapa.css') }}">
<script src="{{ site_url('assets/js/servicio/mapa_custom_ubicacion.js') }}"></script>
@endsection
@section('breadcrumb')
lavador
@endsection
@section('titulo')
Ubicación del lavador
@endsection

@section('contenido')
<div class="row">
    <div class="col-md-12  mt-4 mb-4">
        <div style="width: 100%;height: 400px" id="mapa"></div>
        <input type="hidden" id="id_servicio" value="{{ $id_servicio }}" name="id_servicio">
        <input type="hidden" name="id_estatus_lavado" id="id_estatus_lavado" value="{{ isset($data['info_servicio']->id_estatus_lavado)? $data['info_servicio']->id_estatus_lavado : '' }}">
    </div>
    <div class="col-md-3">
        <fieldset>
                <div class="form-group">
                <label for="latitud">Latitud: </label>
                    <input type="text" readonly id="latitud" name="latitud" class="form-control" value="{{ isset($data['info_servicio']->latitud_lavador)? $data['info_servicio']->latitud_lavador : '' }}">
                </div>
            </fieldset>
        </div>
    <div class="col-md-3">
        <fieldset>
            <div class="form-group">
                <label for="longitud">Longitud: </label>
                <input type="text" readonly  id="longitud" name="longitud" class="form-control" value="{{ isset($data['info_servicio']->longitud_lavador)? $data['info_servicio']->longitud_lavador : '' }}">
            </div>
        </fieldset>
    </div>
</div>
@endsection
@section('scripts')

@endsection