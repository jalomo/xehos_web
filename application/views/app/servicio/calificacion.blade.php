@layout('template/layout')
@section('styles')
	
@endsection
@section('breadcrumb')
	Calificar servicio
@endsection
@section('titulo')
Calificar servicio
@endsection
@section('contenido')


<form class="row justify-content-center mb-4">
    <div class="col-md-6 text-center">
        <h1>Califica esta app</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-center">
                    <h1 class="p-2" onclick="handlestars(this)" id="star1">
                        <i class="far fa-star"></i>
                    </h1>
                    <h1 class="p-2" onclick="handlestars(this)" id="star2">
                        <i class="far fa-star"></i>
                    </h1>
                    <h1 class="p-2" onclick="handlestars(this)" id="star3">
                        <i class="far fa-star"></i>
                    </h1>
                    <h1 class="p-2" onclick="handlestars(this)" id="star4">
                        <i class="far fa-star"></i>
                    </h1>
                    <h1 class="p-2" onclick="handlestars(this)" id="star5">
                        <i class="far fa-star"></i>
                    </h1>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <h3>¿El operador brindó buen servicio?</h3>
                <div class="d-flex justify-content-center">
                    <button type="button" id="btn1si"  class="btn btn-outline-success mr-2 col-md-2"> SI </button>
                    <button id="btn1no" type="button" class="btn btn-outline-danger col-md-2"> NO </button>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <h3>¿Estás satisfecho con tu servicio?</h3>
                <div class="d-flex justify-content-center">
                    <button type="button" id="btn2si" class="btn btn-outline-success mr-2 col-md-2"> SI </button>
                    <button id="btn2no" type="button" class="btn btn-outline-danger col-md-2"> NO </button>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <h3>¿Nos recomendarías con tus amigos?</h3>
                <div class="d-flex justify-content-center">
                    <button type="button" id="btn3si" class="btn btn-outline-success mr-2 col-md-2"> SI </button>
                    <button id="btn3no" type="button" class="btn btn-outline-danger col-md-2"> NO </button>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <div class="form-group">
                    <textarea placeholder="Describe tu experiencia" class="form-control" id="descripcion_experiencia" name="descripcion_experiencia" rows="3"></textarea>
                    <input type="hidden" name="puntaje" value="0" id="puntaje">
                    <input type="hidden" name="operador_servicio" value="0" id="operador_servicio">
                    <input type="hidden" name="satisfecho" value="0" id="satisfecho">
                    <input type="hidden" name="recomendaria" value="0" id="recomendaria">
                    <input type="hidden" value="{{ $id_servicio}}" id="id_servicio" name="id_servicio">
                  </div>
            </div>
            <div class="col-md-12">
                <button type="button" onclick="guardarcalificacion()" class="btn btn-primary col-md-3"> 
                    <i class="far fa-paper-plane"></i> Enviar 
                </button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ site_url('assets/js/servicio/calificacion.js') }}"></script>
@endsection