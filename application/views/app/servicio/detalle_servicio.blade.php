@layout('template/layout')
@section('styles')
<link rel="stylesheet" href="{{ site_url('assets/css/mapa.css') }}">
<script src="{{ site_url('assets/js/servicio/mapa_custom_ubicacion.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM&callback=initMap&libraries=places" async defer></script> 
@endsection
@section('breadcrumb') 
	Detalle de servicio
@endsection
@section('titulo') 
	Detalle de servicio
@endsection
@section('contenido')
	<div class="row">
		<div class="col-md-12">
			<h3>Datos del vehículo</h3>
		</div>
		<div class="col-md-12">
			<div class="row">
				  <div class="col-md-4">
					<div class="form-group">
					  <label class="small mb-2" for="nombre">Marca</label>
					<input type="hidden" name="id_servicio" id="id_servicio" value="{{ $data['info_servicio']->id_servicio}}">
					<input type="hidden" name="id_estatus_lavado" id="id_estatus_lavado" value="{{ $data['info_servicio']->id_estatus_lavado}}">
					
					<input readonly class="form-control" type="text" value="{{$data['info_servicio']->marca }}" name="info_marca" id="info_marca">
					</div>
				  </div>
				  <div class="col-md-4">
					<div class="form-group">
					  <label class="small mb-2" for="nombre">Modelo</label>
					  <input readonly class="form-control" type="text" value="{{$data['info_servicio']->modelo }}" name="info_modelo" id="info_modelo">
					</div>
				  </div>
				  <div class="col-md-4">
					<div class="form-group">
					  <label class="small mb-2" for="anio">Año</label>
					  <input readonly class="form-control" type="text" value="{{$data['info_servicio']->anio }}" name="" id="">
					</div>
				  </div>
				  <div class="col-md-4">
					<div class="form-group">
						  <label class="small mb-2" for="nombre">Color *</label>
						<input readonly class="form-control" type="text" value="{{$data['info_servicio']->color }}" >
					</div>
				  </div>
				  <div class="col-md-4">
					<div class="form-group">
					  <label for="">Placas</label>
					  <input readonly class="form-control" type="text" value="{{$data['info_servicio']->placas }}">
					</div>
				  </div>
				  <div class="col-md-4">
					<div class="form-group">
					  <label class="small mb-2" for="anio">Numero de serie *</label>
					  <input readonly class="form-control" type="text" value="{{$data['info_servicio']->numero_serie }}" >
					</div>
				  </div>
			</div>
		</div>
		@if (empty($data['info_servicio']->id_servicio_calificacion) && ($data['info_servicio']->id_estatus_lavado == 6 || $data['info_servicio']->id_estatus_lavado == 5))
			<div class="col-md-12">
				<div class="alert alert-warning">
					Ayudanos a mejorar el servicio, calificando nuestra página
					<a title="Califica nuestro servicio" href="{{ site_url('xehos/calificacion/'.encrypt($data['info_servicio']->id_servicio)) }}" class="alert-link" style="min-width: 3rem">
						Link
					</a>
				</div>
			</div>
		@endif
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<h2> Ubicación de servicio </h2>
		</div>
		<div class="col-md-12  mt-4 mb-4">
			<div style="width: 100%; height: 400px" id="mapa"></div>
		</div>
		<div class="col-md-12">
			<div class="row">
				@if (!empty($data['info_servicio']->id_servicio) && empty($data['info_servicio']->latitud_lavador) && $data['info_servicio']->id_estatus_lavado == 1)
					<div class="col-md-12">
						<div class="alert alert-warning" role="alert">
							El lavador no ha confirmado la ruta
						</div>
					</div>
				@endif
				<div class="col-md-4">
					<fieldset >
						<div class="form-group">
							<label for="calle">Calle</label>
							<input type="text" id="calle" name="calle" readonly  class="form-control" value="{{ isset($data['info_servicio']->calle)? $data['info_servicio']->calle : '' }}">
						</div>
					</fieldset>
				</div>
				<div class="col-md-4">
					<fieldset >
						<div class="form-group">
							<label for="numero_ext"># Exterior</label>
							<input type="text" id="numero_ext" name="numero_ext" readonly class="form-control" value="{{ isset($data['info_servicio']->numero_ext)? $data['info_servicio']->numero_ext : '' }}">
						</div>
					</fieldset>
				</div>
				<div class="col-md-4">
					<fieldset >
						<div class="form-group">
							<label for="numero_int"># Interior</label>
							<input type="text" id="numero_int" name="numero_int" readonly class="form-control" value="{{ isset($data['info_servicio']->numero_int)? $data['info_servicio']->numero_int : '' }}">
						</div>
					</fieldset>
				</div>
				<div class="col-md-4">
					<fieldset >
						<div class="form-group">
							<label for="colonia">Colonia</label>
							<input type="text" id="colonia" name="colonia" readonly class="form-control" value="{{ isset($data['info_servicio']->colonia)? $data['info_servicio']->colonia : '' }}">
						</div>
					</fieldset>
				</div>
				<div class="col-md-4">
					<fieldset >
						<div class="form-group">
							<label for="colonia">Estado</label>
							<input type="text" id="colonia" name="colonia" readonly class="form-control" value="{{ isset($data['info_servicio']->estado)? $data['info_servicio']->estado : '' }}">
						</div>
					</fieldset>
				</div>
				<div class="col-md-4">
					<fieldset >
						<div class="form-group">
							<label for="municipio">Municipio</label>
							<input type="text" id="municipio" name="municipio" readonly class="form-control" value="{{ isset($data['info_servicio']->municipio)? $data['info_servicio']->municipio : '' }}">
						</div>
					</fieldset>
				</div>
				<div class="col-md-4">
					<fieldset >
						<div class="form-group">
							<label for="sucursal">Sucursal</label>
							<input type="text" id="sucursal" name="sucursal" readonly class="form-control" value="{{ isset($data['info_servicio']->sucursal)? $data['info_servicio']->sucursal : '' }}">
						</div>
					</fieldset>
				</div>
				<div class="col-md-4">
					<fieldset >
						<div class="form-group">
							<label for="latitud">Latitud</label>
							<input type="text" id="latitud" name="latitud" readonly class="form-control" value="{{ isset($data['info_servicio']->latitud)? $data['info_servicio']->latitud : '' }}">
						</div>
					</fieldset>
				</div>
				<div class="col-md-4">
					<fieldset >
						<div class="form-group">
							<label for="colonia">Longitud: </label>
							<input type="text" id="longitud" name="longitud" readonly class="form-control" value="{{ isset($data['info_servicio']->longitud)? $data['info_servicio']->longitud : '' }}">
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12 mb-2">
			<h2> Datos de la cita </h2>
		</div>
		<div class="col-md-12">
			<div class="card bg-light mb-3">
				<div class="card-header"><b>{{ $data['info_servicio']->servicioNombre}}</b></div>
				<div class="card-body">
					<h5 class="card-title">Info: </h5>
					<p class="card-text">
						Solicitaste un servicio <b>{{ $data['info_servicio']->servicioNombre}}</b>, el día <b>{{  obtenerFechaEnLetra($data['info_servicio']->fecha_servicio)  }}</b> a las <b> {{ $data['info_servicio']->hora_servicio }} hrs.</b>
					</p>
					@if (isset($data['info_servicio']->comentarios))
						<h5>Comentarios: </h5>
						<p>
							{{ $data['info_servicio']->comentarios }}
						</p>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection