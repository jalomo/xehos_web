@layout('template_login/layoutlogin')
@section('styles')
<link href="{{ site_url('assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" crossorigin="anonymous" />
@endsection

@section('contenido')
<div id="layoutAuthentication_content">
    <main>
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-lg-7">
                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                        <div class="card-header carheader-login">
                            <h3 class="text-center font-weight-light my-4"></h3>
                        </div>
                        <div class="card-body">
                            <div class="jumbotron text-center">
                                @if (isset($data['estatus_validado']) && $data['estatus_validado'] == 1)
                                    <h1 class="display-4">Error</h1>
                                    <p class="lead">No se puede validar la cuenta con la url formada</p>
                                @endif
                                
                                @if (isset($data['estatus_validado']) && $data['estatus_validado'] == 2)
                                    <h1 class="display-4">Validando cuenta</h1>
                                    <p class="lead">Estamos validando tu información.</p>
                                @endif
                            
                                @if (isset($data['estatus_validado']) && $data['estatus_validado'] == 3)
                                    <h1 class="display-4">Cuenta ya validada</h1>
                                    <p class="lead">La cuenta ya fue validada previamente.</p>
                                @endif
                            
                                <hr class="my-4">
                                <p><i style="font-size: 10rem;" class="fas fa-spinner fa-spin"></i></p>
                                <input type="hidden" id="estatus_validado" value="{{ $data['estatus_validado'] }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </main>
</div>
@endsection

@section('scripts')
<script src="{{ site_url('assets/js/sweetalert/dist/sweetalert.min.js') }}" crossorigin="anonymous" ></script>
<script src="{{ site_url('assets/js/registro/confirmarcuenta.js') }}" ></script>
@endsection