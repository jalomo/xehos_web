
  <!-- Modal -->
  <div class="modal fade" id="automodal" tabindex="-1" role="dialog" aria-labelledby="automodalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="automodalLabel">Agregar vehículo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="col-md-12">
              <div id="msgmodal"  role="alert">
              
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="small mb-2" for="nombre">Color *</label>
                <select class="form-control select2 py-4" name="id_color" id="id_color">
                  <option value="">Seleccionar</option>
                  @foreach ($cat_colores as $item)
                    <option <?php echo set_select('id_color',  $item->id ); ?> value="{{ $item->id }}">{{ $item->color }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="">Placas *</label>
                <input class="form-control" onkeyup="validarplaca()" type="text" value="<?php echo set_value('placas'); ?>" name="placas" id="placas">
                <div id="msgvalidationplacas" class=""> </div>
                <input type="hidden" id="placas_validas" value="placas_validas" name="placas_validas">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="small mb-2" for="nombre">Marca *</label>
                <select  onchange="onchangeMarca()"  class="form-control" name="id_marca" id="id_marca">
                  <option value="">Seleccionar</option>
                  @foreach ($cat_marcas as $item)
                    <option <?php echo set_select('id_marca',  $item->id ); ?> value="{{ $item->id }}">{{ $item->marca }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="small mb-2" for="nombre">Modelo *</label>
                <select disabled class="form-control select2 py-4" name="id_modelo" id="id_modelo">
                  <option value="">Seleccionar</option>
                 
                </select>
              </div>
            </div>
           
            <div class="col-md-12">
              <div class="form-group">
                <label class="small mb-2" for="anio">Año </label>
                <select class="form-control select2 py-4" name="id_anio" id="id_anio">
                  <option value="">Seleccionar</option>
                  @foreach ($cat_anio as $item)
                    <option <?php echo set_select('id_anio',  $item->id ); ?> value="{{ $item->id }}">{{ $item->anio }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="small mb-2" for="anio">Numero de serie </label>
                <input class="form-control" type="text" value="<?php echo set_value('numero_serie'); ?>" name="numero_serie" id="numero_serie">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="small mb-2" for="kilometraje">Kilometraje </label>
                <input class="form-control" type="number" name="kilometraje" value="<?php echo set_value('kilometraje'); ?>" id="kilometraje">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="button" onclick="guardarvehiculomodal()" class="btn btn-primary">Guardar vehículo</button>
        </div>
      </div>
    </div>
  </div>