@layout('template/layout')
@section('styles')
<link href="{{ site_url('assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" crossorigin="anonymous" />
@endsection
@section('breadcrumb')
Detalle
@endsection
@section('titulo')
Mi Perfil
@endsection
@section('contenido')
	<?php echo form_open('xehos/editarperfil',['class'=>'row']); ?>
	<div class="col-md-12">
		<?php if(validation_errors() !== '' || !empty($this->session->flashdata('registro_error'))):?>
			<div class="alert alert-danger" role="alert">
				<?php echo validation_errors(); ?>
				@if (!empty($this->session->flashdata('registro_error')))
					{{ $this->session->flashdata('registro_error'); }}
				@endif
			</div>
		<?php endif;?>
	</div>
	<div class="col-md-4">
		<fieldset >
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" readonly id="nombre" name="nombre" class="form-control" value="{{ isset($data['usuario']->nombre)? $data['usuario']->nombre : '' }}">
			</div>
		</fieldset>
	</div>
	<div class="col-md-4">
		<fieldset >
			<div class="form-group">
				<label for="nombre">Apellido materno</label>
				<input type="text" readonly id="apellido_paterno" name="apellido_paterno" class="form-control" value="{{ isset($data['usuario']->apellido_paterno)? $data['usuario']->apellido_paterno : '' }}">
			</div>
		</fieldset>
	</div>
	<div class="col-md-4">
		<fieldset >
			<div class="form-group">
				<label for="nombre">Apellido paterno</label>
				<input type="text" readonly id="apellido_materno" name="apellido_materno" class="form-control" value="{{ isset($data['usuario']->apellido_materno)? $data['usuario']->apellido_materno : '' }}">
			</div>
		</fieldset>
	</div>
	<div class="col-md-4">
		<fieldset >
			<div class="form-group">
				<label for="nombre">Email</label>
				<input type="text" readonly id="email" name="email" class="form-control" value="{{ isset($data['usuario']->email)? $data['usuario']->email : '' }}">
			</div>
		</fieldset>
	</div>
	<div class="col-md-4">
		<fieldset >
			<div class="form-group">
				<label for="nombre">Telefono</label>
				<input type="text" id="telefono" name="telefono" class="form-control" value="{{ isset($data['usuario']->telefono)? $data['usuario']->telefono : '' }}">
			</div>
		</fieldset>
	</div>
	<div class="col-md-4 mt-4">
		<button type="submit" class="btn btn-primary mt-2"> 
			<i class="fas fa-save"></i> Actualizar
		</button>
	</div>
</form>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
@endsection