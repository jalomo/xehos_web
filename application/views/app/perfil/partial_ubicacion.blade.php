<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="calle">Calle</label>
            <input type="text" {{ isset($editar) && $editar ? "" : 'readonly' }}  id="calle" name="calle" class="form-control" value="{{ isset($ubicacion_usuario->calle)? $ubicacion_usuario->calle : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="numero_ext"># Exterior</label>
            <input type="text" {{ isset($editar) && $editar ? "" : 'readonly' }} id="numero_ext" name="numero_ext" class="form-control" value="{{ isset($ubicacion_usuario->numero_ext)? $ubicacion_usuario->numero_ext : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="numero_int"># Interior</label>
            <input type="text" {{ isset($editar) && $editar ? "" : 'readonly' }} id="numero_int" name="numero_int" class="form-control" value="{{ isset($ubicacion_usuario->numero_int)? $ubicacion_usuario->numero_int : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="colonia">Colonia</label>
            <input type="text" {{ isset($editar) && $editar ? "" : 'readonly' }} id="colonia" name="colonia" class="form-control" value="{{ isset($ubicacion_usuario->colonia)? $ubicacion_usuario->colonia : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="estado">Estado</label>
            <select {{ isset($editar) && $editar ? "" : 'disabled' }} onchange="onchange_estado()" class="form-control" name="id_estado" id="id_estado" >
                <option value="">Seleccionar ...</option>
                @foreach ($estados as $item)
                    @if (isset($ubicacion_usuario->id_estado) && $item->id == $ubicacion_usuario->id_estado)
                        <option selected value="{{ $item->id }}">{{ $item->estado  }}</option>
                        @else
                        <option  value="{{ $item->id }}">{{ $item->estado  }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="id_municipio">Municipio</label>
            <select {{ isset($editar) && $editar ? "" : 'disabled' }} class="form-control" name="id_municipio" id="id_municipio" >
                <option  value="">Seleccionar ...</option>
                @foreach ($cat_municipios as $item)
                    @if (isset($ubicacion_usuario->id_municipio) && $item->id == $ubicacion_usuario->id_municipio)
                        <option selected value="{{ $item->id }}">{{ $item->municipio  }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="latitud">Latitud</label>
            <input type="text" readonly id="latitud" name="latitud" class="form-control" value="{{ isset($ubicacion_usuario->latitud)? $ubicacion_usuario->latitud : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset >
        <div class="form-group">
            <label for="longitud">Longitud</label>
            <input type="text" readonly  id="longitud" name="longitud" class="form-control" value="{{ isset($ubicacion_usuario->longitud)? $ubicacion_usuario->longitud : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4 mt-4">
    @if (isset($editar) && $editar)    
        <button type="submit" class="btn btn-primary col-12 mt-2"> 
            <i class="fas fa-save"></i> Actualizar
        </button>
        @else
        <a href="{{ site_url('xehos/editarubicacionusuario') }}"  class="btn btn-primary mt-2 col-md-12" > 
            <i class="fas fa-edit"></i> Editar Ubicación
        </a>
    @endif
</div>