<div class="col-md-4">
    <fieldset disabled>
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" id="nombre" class="form-control" value="{{ isset($data['usuario']->nombre)? $data['usuario']->nombre : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset disabled>
        <div class="form-group">
            <label for="nombre">Apellido materno</label>
            <input type="text" id="apellido_paterno" class="form-control" value="{{ isset($data['usuario']->apellido_paterno)? $data['usuario']->apellido_paterno : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset disabled>
        <div class="form-group">
            <label for="nombre">Apellido paterno</label>
            <input type="text" id="apellido_materno" class="form-control" value="{{ isset($data['usuario']->apellido_materno)? $data['usuario']->apellido_materno : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset disabled>
        <div class="form-group">
            <label for="nombre">Email</label>
            <input type="text" id="email" class="form-control" value="{{ isset($data['usuario']->email)? $data['usuario']->email : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-4">
    <fieldset disabled>
        <div class="form-group">
            <label for="nombre">Telefono</label>
            <input type="text" id="telefono" class="form-control" value="{{ isset($data['usuario']->telefono)? $data['usuario']->telefono : '' }}">
        </div>
    </fieldset>
</div>
<div class="col-md-12">
    
    <br>
    <div class="row">
        @if (false)
            <div class="col-md-3">
                <a href="{{ site_url('xehos/editarperfil') }}"  class="btn btn-primary mt-2 col-md-12" > 
                    <i class="fas fa-edit"></i> Ir a editar 
                </a>
            </div>
        @endif
        <div class="col-md-3">
            <button type="button" onclick="openmodal()" class="btn btn-danger mt-2 col-md-12" > 
                <i class="fas fa-edit"></i> Cambiar contraseña
            </button>
        </div>
    </div>
</div>