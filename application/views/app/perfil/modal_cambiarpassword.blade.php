
  <!-- Modal -->
  <div class="modal fade" id="passwordmodal" tabindex="-1" role="dialog" aria-labelledby="passwordmodalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="passwordmodalLabel">Cambiar password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="col-md-12">
              <div id="msgmodal"  role="alert"></div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="small mb-2" for="password">Contraseña Actual</label>
                <input class="form-control" autocomplete="off" type="password" value="{{ $data['usuario']->password }}" name="actual_password" id="actual_password">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="small mb-2" for="new_password">Nueva contraseña</label>
                <input class="form-control" autocomplete="off" type="password" name="new_password" id="new_password">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="small mb-2" for="repeat_password">Repetir contraseña</label>
                <input class="form-control" onkeyup="validarrepeatcontrasena()" autocomplete="off" type="password" name="repeat_password"  id="repeat_password">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="button" onclick="actualizarcontrasena()" class="btn btn-primary">Actualizar contraseña</button>
        </div>
      </div>
    </div>
  </div>