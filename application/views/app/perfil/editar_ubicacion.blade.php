@layout('template/layout')
@section('styles')
<link rel="stylesheet" href="{{ site_url('assets/css/mapa.css') }}">
@endsection
@section('breadcrumb')
editar_ubicacion
@endsection
@section('titulo')
Ubicación
@endsection
@section('contenido')
<?php echo form_open('xehos/editarubicacionusuario',['class'=>'row']); ?>
	<div class="col-md-12 mt-4">
		<h1>Ubicación</h1>
	</div>
	<div class="col-md-12">
		<?php if(validation_errors() !== '' || !empty($this->session->flashdata('error_update'))):?>
			<div class="alert alert-danger" role="alert">
				<?php echo validation_errors(); ?>
				@if (!empty($this->session->flashdata('error_update')))
					{{ $this->session->flashdata('error_update'); }}
				@endif
			</div>
		<?php endif;?>
	</div>
	
	<div id="map_container" style="width: 100%;" class="">
		<div class="col-md-12 mt-4 mb-4">
			<div class="pac-card" id="pac-card">
				<div>
				  <div id="title">Ubicación</div>
				  <div id="strict-bounds-selector" class="pac-controls">
				  </div>
				</div>
				<div id="pac-container">
				  <input id="pac-input" type="text" placeholder="Ingresa direccion" />
				</div>
			  </div>
			<div style="width: 100%;height: 400px" id="mapa"></div>
			<div id="infowindow-content">
				<img src="" width="16" height="16" id="place-icon" />
				<span id="place-name" class="title"></span><br />
				<span id="place-address"></span>
			  </div>
		</div>
	</div>
	<div class="row">
		@include('app/perfil/partial_ubicacion')
	</div>
</form>
@endsection

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM&callback=initMap&libraries=places" async defer></script> 

<script src="{{ site_url('assets/js/servicio/mapa.js') }}"></script>
<script src="{{ site_url('assets/js/servicio/ubicaciones/ubicaciones.js') }}"></script>
@endsection