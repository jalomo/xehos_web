@layout('template/layout')
@section('styles')

@endsection
@section('breadcrumb')
Detalle
@endsection
@section('titulo')
Mi Perfil
@endsection
@section('contenido')
<form class="row">
	@include('app/perfil/partial_usuario')
	<hr>
</form>
@endsection
@include('app/perfil/modal_cambiarpassword')
@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ site_url('assets/js/registro/changepassword.js') }}"></script>
@endsection