<div class="modal fade" id="modalfacturacion" tabindex="-1" role="dialog" aria-labelledby="modalfacturacionLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalfacturacionLabel">Facturación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if (!empty($tiene_datosfacturacion))    
                <div class="row" id="seleccionar_datos">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="fecha">Datos de facturación</label>
                            <select  class="form-control select2" name="datos_facturacion" id="datos_facturacion" >
                                <option value="">Seleccionar ...</option>
                                @foreach ($datos_facturacion as $item)
                                    <option value="{{ $item->id }}">{{ $item->razon_social  }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 mt-2 mb-2 text-right">
                        <button type="button" onclick="closemodal_loadinfo()" class="btn btn-success"> Seleccionar </button>
                    </div>
                </div>
                @else
                <div id="formfacturacion" class="row ">
                    <hr>
                    <div class="col-md-12">
                        <div id="msgmodalfacturacion" role="alert">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Razon social *</label>
                            <input class="form-control" type="text" required value="" name="razon_social" id="razon_social">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Domicilio *</label>
                            <input class="form-control" type="text" required value="" name="domicilio" id="domicilio">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Email facturación *</label>
                            <input class="form-control" onkeyup="onchangevalidaremail()" type="email" value="" name="email_facturacion" id="email_facturacion">
                            <div id="msgvalidationemail" class=""> </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="small mb-2" for="nombre">CFDI *</label>
                            <select class="form-control select2 " name="id_cfdi" id="id_cfdi">
                                <option value="">Seleccionar</option>
                                @foreach ($cat_cfdi as $item)
                                <option value="{{ $item->id }}">{{ $item->cfdi }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="small mb-2" for="nombre">Método de pago </label>
                            <select class="form-control select2 " name="id_metodo_pago" id="id_metodo_pago">
                                <option value="">Seleccionar</option>
                                <option selected value="1">Pago en una sola exhibición</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Rfc *</label>
                            <input class="form-control" onkeyup="validarrfc()" type="text" required value="" name="rfc" id="rfc">
                            <div id="msgvalidationrfc" class=""> </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Código postal *</label>
                            <input class="form-control" type="number" required value="" name="cp" id="cp">
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                @if (empty($tiene_datosfacturacion)) 
                <button type="button" onclick="guardardatosfacturacion()" id="btn-guardardatosfacturacion" class="btn btn-primary">Guardar</button>
                @endif
            </div>
        </div>
    </div>
</div>