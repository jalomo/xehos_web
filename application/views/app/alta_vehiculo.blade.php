@layout('template/layout')
@section('styles')
<link href="{{ site_url('assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" crossorigin="anonymous" />
@endsection
@section('breadcrumb')
	autolavado
@endsection
@section('titulo')
	Agregar vehículo
@endsection
@section('contenido')

<?php echo form_open('xehos/altavehiculo', ['class' => 'row']); ?>
<div class="col-md-12">
	<?php if (validation_errors() !== '' || !empty($this->session->flashdata('registro_error'))) : ?>
		<div class="alert alert-danger" role="alert">
			<?php echo validation_errors(); ?>
			@if (!empty($this->session->flashdata('registro_error')))
			{{ $this->session->flashdata('registro_error'); }}
			@endif
		</div>
	<?php endif; ?>

	@if (!empty($this->session->flashdata('registro_exitoso')))
	<div class="alert alert-success" role="alert">
		{{ $this->session->flashdata('registro_exitoso'); }}
	</div>
	@endif
</div>
<div class="col-md-4">
	<div class="form-group">
		<label class="small mb-2" for="nombre">Color *</label>
		<select class="form-control select2 py-4" name="id_color" id="id_color">
			<option value="">Seleccionar</option>
			@foreach ($cat_colores as $item)
				<option <?php echo set_select('id_color',  $item->id ); ?> value="{{ $item->id }}">{{ $item->color }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="col-md-4">
	<div class="form-group">
		<label for="">Placas *</label>
		<input class="form-control" type="text" value="<?php echo set_value('placas'); ?>" name="placas" id="placas">
	</div>
</div>
<div class="col-md-4">
	<div class="form-group">
		<label class="small mb-2" for="nombre">Marca *</label>
		<select onchange="onchangeMarca()" class="form-control select2 py-4" name="id_marca" id="id_marca">
			<option value="">Seleccionar</option>
			@foreach ($cat_marcas as $item)
				<option <?php echo set_select('id_marca',  $item->id ); ?> value="{{ $item->id }}">{{ $item->marca }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="col-md-4">
	<div class="form-group">
		<label class="small mb-2" for="nombre">Modelo *</label>
		<select disabled class="form-control select2 py-4" name="id_modelo" id="id_modelo">
			<option value="">Seleccionar</option>
		</select>
	</div>
</div>

<div class="col-md-4">
	<div class="form-group">
		<label class="small mb-2" for="anio">Año *</label>
		<select class="form-control select2 py-4" name="id_anio" id="id_anio">
			<option value="">Seleccionar</option>
			@foreach ($cat_anio as $item)
				<option <?php echo set_select('id_anio',  $item->id ); ?> value="{{ $item->id }}">{{ $item->anio }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="col-md-4">
	<div class="form-group">
		<label class="small mb-2" for="anio">Numero de serie</label>
		<input class="form-control" type="text" value="<?php echo set_value('numero_serie'); ?>" name="numero_serie" id="numero_serie">
	</div>
</div>
<div class="col-md-4">
	<div class="form-group">
		<label class="small mb-2" for="kilometraje">Kilometraje </label>
		<input class="form-control" type="number" name="kilometraje" value="<?php echo set_value('kilometraje'); ?>" id="kilometraje">
	</div>
</div>
<div class="col-md-12">
	<button class="btn btn-primary">
		Registrar
	</button>
</div>
</form>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script>

const onchangeMarca = () => {
    let id_marca = document.getElementById('id_marca').value;
    let url = site_url + "ajaxRequestServicio/ajax_getmodelobymarca";
    // return console.log(id_marca)
    $("#id_modelo").empty();
    $("#id_modelo").append("<option value=''>-- Selecciona --</option>");
    $("#id_modelo").removeAttr("disabled");
    !id_marca && false
    myfunction.ajaxpost(url, { id_marca }, function (data) {
        $("#id_modelo").empty();
        $("#id_modelo").append("<option value=''>-- Selecciona --</option>");
        $("#id_modelo").removeAttr("disabled");
        if (data && data.length > 0) {
            data.map(item => $("#id_modelo").append("<option value= '" + item.id + "'>" + item.modelo + "</option>"))
        } else {
            $("#id_modelo").empty();
            $("#id_modelo").append("<option value=''> No se encontraron datos</option>");
        }
    })
}
</script>
@endsection