@layout('app/email/layout_correo')
@section('contenido')
<tr style="border-collapse:collapse;">
  <td style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:left top;background-color:#FAFAFA;" bgcolor="#fafafa" align="left">
    <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
      <tr style="border-collapse:collapse;">
        <td width="540" valign="top" align="center" style="padding:0;Margin:0;">
          <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
            <tr style="border-collapse:collapse;">
              <td align="left" style="padding:0;Margin:0;padding-bottom:10px;">
                <h4 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:16px;font-style:normal;font-weight:bold;color:#212121;">{{$asunto}}</h4>
              </td>
            </tr>
            <tr style="border-collapse:collapse;">
              <td align="left" style="padding:0;Margin:0;padding-bottom:5px;">
                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                  Estimado <strong>{{$datos->nombre.' '.$datos->apellido_paterno.' '.$datos->apellido_materno}},</strong>
                </p>
                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                    La contraseña para su cuenta en Xehos autolavado ha cambiado
                </p>
                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                  <strong>Fecha y hora: </strong> {{ date('d-m-Y :H:i')  }} <br>
                </p>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
@endsection
