@layout('template/layout')
@section('styles')
<link href="{{ site_url('assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" crossorigin="anonymous" />
@endsection
@section('breadcrumb') 
	vehículos registrados
@endsection
@section('titulo') 
	Mis vehículos
@endsection
@section('contenido')
	<div class="row">
		<div class="col-md-8"></div>
		<div class="col-md-4 text-right">
			<a href="{{ site_url('xehos/altavehiculo') }}" class="btn btn-primary">
				<i class="fas fa-plus"></i> Agregar vehículo
			</a>
		</div>
		<div class="col-md-12">
			<div class="row mt-4">
				@if (isset($misautos))
					@foreach ($misautos as $auto)
						<div class="col-sm-4 mb-3">
							<div class="card">
								<div class="card-body">
								<h4 class="card-title">{{ $auto->marca .' - '. $auto->modelo }}</h4>
								<p class="card-text">
									<div>
										<span>{{ 'Color: '. $auto->color}}</span>
									</div>
									<div>
										<span class="card-title">{{ 'Año: '. $auto->anio}}</span>
									</div>
									<div>
										<span class="card-title">{{ 'Placas: '.  $auto->placas}}</span>
									</div>
								</p>
								</div>
							</div>
						</div>
					@endforeach
				@endif
			  </div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script>
    let tabla = $('#tabla-misvehiculos').DataTable({

    });
</script>
@endsection