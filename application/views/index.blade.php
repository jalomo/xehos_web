@layout('template/layout')
@section('styles')
<link href="{{ site_url('assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" crossorigin="anonymous" />
@endsection
@section('breadcrumb') 
	seleccionar estado
@endsection
@section('titulo') 
	Selecciona tu estado
@endsection
@section('contenido')
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label class="small mb-1" for="nombre">Estado *</label>
				<select  class="form-control py-4"  name="estado" id="estado">
					<option value="">Seleccionar</option>
				</select>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
@endsection