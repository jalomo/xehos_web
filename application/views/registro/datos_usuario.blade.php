<div class="form-row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="small mb-1" for="nombre">Nombre *</label>
            <input name="nombre" class="form-control py-4" id="nombre" type="text" placeholder="Nombre" value="<?php echo set_value('nombre'); ?>" />
            <div class="text-danger" id="error_nombre"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="small mb-1" for="nombre">Apellido paterno *</label>
            <input name="apellido_paterno" class="form-control py-4" id="apellido_paterno" type="text" placeholder="Apellido paterno" value="<?php echo set_value('apellido_paterno'); ?>" />
            <div class="text-danger" id="error_apellido_paterno"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="small mb-1" for="nombre">Apellido materno *</label>
            <input name="apellido_materno" class="form-control py-4" id="apellido_materno" type="text" placeholder="Apellido materno" value="<?php echo set_value('apellido_materno'); ?>" />
            <div class="text-danger" id="error_apellido_materno"></div>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="small mb-1" for="inputEmailAddress">Correo electronico</label>
            <input class="form-control py-4" name="email" id="email" type="email" aria-describedby="email" value="<?php echo set_value('email'); ?>" placeholder="correo@dominio.com" />
            <div class="text-danger" id="error_email"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="small mb-1" for="telefono">Telefono</label>
            <input class="form-control py-4" name="telefono" id="telefono" type="text" aria-describedby="telefono" value="<?php echo set_value('telefono'); ?>" placeholder="" />
            <div class="text-danger" id="error_telefono"></div>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="small mb-1" for="password">Contraseña</label>
            <input class="form-control py-4" name="password" id="password" type="password" placeholder="Ingresar contraseña" value="<?php echo set_value('password'); ?>" />
            <div class="text-danger" id="error_password"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="small mb-1" for="inputConfirmPassword">Confirmar Contraseña</label>
            <input class="form-control py-4" name="passconf" id="inputConfirmPassword" type="password" placeholder="Confirma contraseña" value="<?php echo set_value('passconf'); ?>" />
            <?php echo form_error('passconf', '<div class="text-danger">', '</div>'); ?>
            <div class="text-danger" id="error_passconf"></div>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="estado">Estado</label>
            <select onchange="onchange_estado()" class="form-control" name="id_estado" id="id_estado" >
                <option value="">Seleccionar ...</option>
                @foreach ($estados as $item)
                    <option value="{{ $item->id }}">{{ $item->estado  }}</option>
                @endforeach
            </select>
            <div class="text-danger" id="error_id_estado"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="id_sucursal">Sucursal</label>
            <select disabled class="form-control" name="id_sucursal" id="id_sucursal" >
                <option value="">Seleccionar ...</option>
            </select>
            <div class="text-danger" id="error_id_sucursal"></div>
        </div>
    </div>
</div>