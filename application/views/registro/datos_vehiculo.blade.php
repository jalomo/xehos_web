<div class="col-md-4">
    <div class="form-group">
        <label for="">Placas *</label>
        <input class="form-control" onkeyup="validarplaca()" type="text" value="<?php echo set_value('placas'); ?>" name="placas" id="placas">
        <div id="msgvalidationplacas" class=""> </div>
        <input type="hidden" id="placas_validas" value="placas_validas" name="placas_validas">
        <div class="text-danger" id="error_placas"></div>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="small mb-2" for="nombre">Marca *</label>
        <select onchange="onchangeMarca()" class="form-control" name="id_marca" id="id_marca">
            <option value="">Seleccionar</option>
            @foreach ($cat_marcas as $item)
            <option <?php echo set_select('id_marca',  $item->id); ?> value="{{ $item->id }}">{{ $item->marca }}</option>
            @endforeach
        </select>
        <div class="text-danger" id="error_id_marca"></div>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="small mb-2" for="nombre">Modelo *</label>
        <select disabled class="form-control " name="id_modelo" id="id_modelo">
            <option value="">Seleccionar</option>
        </select>
        <div class="text-danger" id="error_id_modelo"></div>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="small mb-2" for="anio">Año *</label>
        <select class="form-control" name="id_anio" id="id_anio">
            <option value="">Seleccionar</option>
            @foreach ($cat_anio as $item)
            <option <?php echo set_select('id_anio',  $item->id); ?> value="{{ $item->id }}">{{ $item->anio }}</option>
            @endforeach
        </select>
        <div class="text-danger" id="error_id_anio"></div>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="small mb-2" for="nombre">Color *</label>
        <select class="form-control" name="id_color" id="id_color">
            <option value="">Seleccionar</option>
            @foreach ($cat_colores as $item)
            <option <?php echo set_select('id_color',  $item->id); ?> value="{{ $item->id }}">{{ $item->color }}</option>
            @endforeach
        </select>
        <div class="text-danger" id="error_id_color"></div>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="small mb-2" for="kilometraje">Kilometraje </label>
        <input class="form-control" type="number" name="kilometraje" value="<?php echo set_value('kilometraje'); ?>" id="kilometraje">
        <div class="text-danger" id="error_kilometraje"></div>
    </div>
</div>