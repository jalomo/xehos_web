<nav class="sb-topnav navbar navbar-expand navbar-dark bg-navbar-custom">
    <a class="navbar-brand" href="{{site_url('/xehos/misvehiculos')}}">

        <img class="img-navbar img-responsive" src="{{base_url('assets/imgs/xehoslogo.png')}}" alt="autolavado">
    </a>

    <button class="btn btn-link btn-sm order-1 order-lg-0 text-white" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
    <!-- Navbar Search-->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

    </form>

    <!-- Navbar-->
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown">
            <a href="{{ site_url('xehos/buzonnotificaciones') }}" style="font-size:20px" class="text-white p-2 mt-2" id="notification">
                <i class="fa fa-bell noti-header"></i>
                <span id="total_notify" style="padding-left:4px; padding-right:4px; border-radius:9px; font-size:14px" class="count bg-danger">-</span>
            </a>
        </li>
        <li class="nav-item dropdown mr-4">
            <a href="{{ site_url('xehos/contactos') }}" style="font-size:20px" class="text-white p-2 mt-2" id="message">
                <i class="fa fa-users noti-header"></i>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white" style="font-size:20px" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cogs fa-fw"></i></a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ site_url('login/cerrarsession') }}">Cerrar sesión</a>
            </div>
        </li>
    </ul>
</nav>