<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" type="image/png" href="{{ base_url('assets/imgs/favicon.ico')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" media="screen">
        <title>Xehos-Autolavado</title>
        <script>
            var site_url = "{{ site_url() }}";
            var base_url = "{{ base_url() }}";
            var telefono = "{{ $this->session->userdata('telefono') }}";
        </script>
        @include('template/scriptsheader')
        @yield('styles')
    </head>
    <body class="sb-nav-fixed">
        @include('template/navbar')
        <div id="layoutSidenav">
            @include('template/sidebar')
            @include('template/content')
        </div>
        
        @include('template/scriptsfooter')
        @yield('scripts')
    </body>
</html>
