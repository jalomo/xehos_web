<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4" style="color: #7780a3;">@yield('titulo')</h1>
            <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ site_url('xehos/misvehiculos') }}">{{ $this->uri->segment(1) }}</a> </li>
                <li class="breadcrumb-item active"><a href="{{ site_url('xehos/'.$this->uri->segment(2)) }}">{{ $this->uri->segment(2) }}</a> </li>
            </ol>
            @yield('contenido')
        </div>
    </main>
    <footer class="py-4 bg-footer-custom mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-white">Copyright &copy; Xehos Autolavado 2020</div>
            </div>
        </div>
    </footer>
</div>