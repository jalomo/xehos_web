<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Servicios</div>
                <a class="nav-link" href="{{ site_url('xehos/misvehiculos') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-car-side"></i>
                    </div>
                    Mis vehículos
                </a>
                <a class="nav-link" href="{{ site_url('xehos/servicios') }}">
                    <div class="sb-nav-link-icon">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                    Agendar cita
                </a>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-hand-sparkles"></i>
                        </div>
                        Servicios 
                    <div class="sb-sidenav-collapse-arrow text-white">
                        <i class="fas fa-angle-down"></i>
                    </div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link"  href="{{ site_url('xehos/listaservicios') }}">Servicios activos</a>
                        <a class="nav-link"  href="{{ site_url('xehos/listaserviciosrealizados') }}">Servicios realizados</a>
                    </nav>
                </div>
                <a class="nav-link" href="{{ site_url('xehos/buzonnotificaciones') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-bell"></i>
                    </div>
                    Buzón de Notificaciones
                </a>
                <a class="nav-link" href="{{ site_url('xehos/autos_nuevos?webapp=true') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Autos nuevos
                </a>
                <a class="nav-link" href="{{ site_url('xehos/autos_seminuevos?webapp=true') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Autos semi nuevos
                </a>
                <a class="nav-link" href="{{ site_url('xehos/autopartes') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-car"></i>
                    </div>
                    Autopartes
                </a>
                <a class="nav-link" href="{{ site_url('xehos/contactos')}}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-comments"></i>
                    </div>
                    Chat
                </a>
                <a class="nav-link"  href="{{ site_url('xehos/tecompramostuauto') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-car"></i>
                    </div>
                    
                    ¡Te compramos tu auto!
                </a>
                <div class="sb-sidenav-menu-heading">Perfil</div>
                <a class="nav-link" href="{{ site_url('xehos/miperfil') }}">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-users"></i>
                    </div>
                    Mi perfil
                </a>
                

            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Bienvenido:</div>
            {{ $this->session->userdata('nombre') }}
        </div>
    </nav>
</div>