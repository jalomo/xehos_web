@layout('template_login/layoutlogin')
@section('styles')

@endsection

@section('contenido')
<div id="layoutAuthentication_content">
    <main>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="card shadow-lg border-0 rounded-lg mt-5">
                        <div class="card-header carheader-login">
                            <h3 class="text-center font-weight-light my-4">
                                <img class="img-login img-responsive" src="{{base_url('assets/imgs/xehoslogo.png')}}" alt="">
                            </h3>
                        </div>
                        <div class="card-body">
                                <?php echo form_open('login'); ?>

                                <div class="form-group">
                                    <label class="small mb-1" for="inputEmailAddress">Correo electrónico</label>
                                    <input class="form-control py-4" id="inputEmailAddress" value="<?php echo set_value('email'); ?>" name="email" type="email" placeholder="Correo electrónico" />
                                </div>
                                <div class="form-group">
                                    <label class="small mb-1" for="inputPassword">Contraseña</label>
                                    <input class="form-control py-4" id="password" name="password" value="<?php echo set_value('password'); ?>"  type="password" placeholder="Contraseña" />
                                </div>
                                <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                    <button class="btn btn-primary col-md-12"><i class="fas fa-unlock-alt"></i> Entrar </button>
                                </div>

                                <?php if(validation_errors() !== '' || !empty($this->session->flashdata('login_error'))):?>
                                    <div class="alert alert-danger mt-2" role="alert">
                                        <?php echo validation_errors(); ?>
                                        @if (!empty($this->session->flashdata('login_error')))
                                            {{ $this->session->flashdata('login_error'); }}
                                        @endif
                                    </div>
                                <?php endif;?>
                            </form>
                        </div>
                        <div class="card-footer text-center">
                        <div class="small"><a href="{{ base_url('login/registrar')}}">¿ No estás registrado? ir a Registrarse</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection

@section('scripts')
<script src="{{ site_url('assets/js/sweetalert/dist/sweetalert.min.js') }}" crossorigin="anonymous" ></script>
<script src="{{ site_url('assets/js/registro/reenviarcodigo.js') }}" ></script>
@endsection