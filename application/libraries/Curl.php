<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Curl
{
    protected $ci;

    public function __construct()
    {
        $this->ci = &get_instance();
    }

    public function curlGet($url = '',  $url_externa = false)
    {
        if ($url === '') {
            return [];
        }
        $final_url = !$url_externa ? site_url() . $url : $url;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $final_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: 2c978bb6-e3c3-49fa-9ed7-c7a91aab0495",
                "cache-control: no-cache"
            ),
        ));


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return $response;
        }
    }

    public function curlDel($url = '')
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => site_url() . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
        ));



        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    public function curldownloadPdf($url = '', $base_64_view)
    {

        header("Content-type:application/pdf");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => site_url() . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('view' => $base_64_view),
            CURLOPT_HTTPHEADER => array(),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        echo $response;
    }

    public function curlPost($url = '', $data, $url_externa = false, $send_js = false)
    {
        $curl = curl_init();
        $final_url = !$url_externa ? site_url() . $url : $url;
        $parametros = is_array($data) && !$url_externa  ? http_build_query($data) : json_encode($data);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $final_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $parametros,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            )
        ));

        $body = curl_exec($curl);
        // echo $body;
        return $body;
    }

    function getHeaders($respHeaders)
    {
        $headers = array();
        $headerText = substr($respHeaders, 0, strpos($respHeaders, "\r\n\r\n"));


        foreach (explode("\r\n", $headerText) as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list($key, $value) = explode(': ', $line);
                if ($key == 'X-Message') {
                    $headers[$key] = $value;
                }
            }
        }

        return $headers;
    }
}

/* End of file CurlLibrary.php */
