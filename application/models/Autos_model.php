<?php

class Autos_model  extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAutosByUsurio($usuario_id)
    {
        $this->db->select(
            'autos.id, autos.placas,autos.numero_serie,autos.kilometraje, cat_colores.color, cat_marcas.marca,cat_anios.anio,cat_modelos.modelo'
        );
        $this->db->from('autos');
        $this->db->join('cat_marcas', 'cat_marcas.id = autos.id_marca');
        $this->db->join('cat_modelos', 'cat_modelos.id = autos.id_modelo');
        $this->db->join('cat_anios', 'cat_anios.id = autos.id_anio');
        $this->db->join('cat_colores', 'cat_colores.id = autos.id_color');
        $this->db->where('autos.id_usuario', $usuario_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function getAutoByParametros($parametro)
    {
        $this->db->select(
            'autos.id, autos.placas,autos.numero_serie,autos.kilometraje, cat_colores.color, cat_marcas.marca,cat_anios.anio,cat_modelos.modelo'
        );
        $this->db->from('autos');
        $this->db->join('cat_marcas', 'cat_marcas.id = autos.id_marca');
        $this->db->join('cat_modelos', 'cat_modelos.id = autos.id_modelo');
        $this->db->join('cat_anios', 'cat_anios.id = autos.id_anio');
        $this->db->join('cat_colores', 'cat_colores.id = autos.id_color');
        
        if (isset($parametro['auto_id'])) {
            $this->db->where('autos.id', $parametro['auto_id']);
        }

        $query = $this->db->get();
        return $query->result();
    }
}
