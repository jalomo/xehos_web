<?php

class Servicio_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
    }

    public function getfechaslavadores($fecha = '', $id_sucursal = '')
    {
        if ($id_sucursal == '') {
            return [];
            die();
        }

        $this->db->where('fecha >=', $fecha  != '' ? $fecha : date('Y-m-d'));
        return $this->db
            ->select('DISTINCT(fecha) as fecha')
            ->where('id_sucursal', $id_sucursal)
            ->order_by('fecha', 'asc')
            ->get('horarios_lavadores')
            ->result();
    }

    public function getHorariosDisponibles($fecha, $id_sucursal)
    {
        if ($fecha == date('Y-m-d')) {
            $this->db->where('hora >=', date('H:i'));
        }

        $this->db->select('DISTINCT(hora) as hora');
        return $this->db->where('fecha', $fecha)
            ->where('id_sucursal', $id_sucursal)
            ->where('activo', 1)
            ->where('ocupado', 0)
            ->order_by('hora', 'asc')
            ->get('horarios_lavadores')
            ->result();
    }

    public function getIdLavador()
    {
        if (empty($this->input->post('fecha')) && empty($this->input->post('hora'))) {
            return [];
        }

        $q = $this->db
            ->where('fecha', $_POST['fecha'])
            ->where('hora', $_POST['hora'])
            ->where('id_sucursal', $_POST['id_sucursal'])
            ->where('activo', 1)
            ->where('ocupado', 0)
            ->get('horarios_lavadores');
        if ($q->num_rows() == 1) {
            return $q->row();
        } else {
            $id_lavador_siguiente = $this->getLavadorSiguiente();
            $array_lavadores = [];
            
            foreach ($q->result() as $l => $lavador) {
                $array_lavadores[] = $lavador->id_lavador;
            }
            //Puede ser que existen 3 lavadores con el horario por ej. 18:00 (4,5,6) pero el lavador siguiente es el 1 entonces debo validar
            if (!in_array($id_lavador_siguiente, $array_lavadores)) {
                $id_lavador_siguiente = $array_lavadores[0];
            }
            $q = $this->db->where('fecha', $_POST['fecha'])
                ->where('hora', $_POST['hora'])
                ->where('activo', 1)
                ->where('ocupado', 0)
                ->where('id_lavador', $id_lavador_siguiente)
                ->get('horarios_lavadores');
            return $q->row();
        }
    }

    public function getLavadorSiguiente()
    {
        $lavadores = $this->getLavadores();
        $q = $this->db->limit(1)->select('s.id_lavador')
            ->order_by('s.id', 'desc')
            ->where('id_lavador !=', '')
            ->where('s.id_sucursal', $this->input->post('id_sucursal'))
            ->join('ubicacion_servicio u', 's.id_ubicacion = u.id')
            ->get('servicio_lavado s');
        if ($q->num_rows() == 1) {
            $id_lavador = $q->row()->id_lavador;
            //Validar si es el último lavador, regresar al primero
            if ($id_lavador == null) {
                return $lavadores[0]->lavadorId;
            }
            if ($id_lavador >= count($lavadores)) {
                return $lavadores[0]->lavadorId;
            } else {
                $q = $this->db->limit(1)->select('lavadorId')->order_by('lavadorId', 'asc')->where('activo', 1)->where('lavadorId >', $id_lavador)->get('lavadores');
                if ($q->num_rows() == 1) {
                    return $q->row()->lavadorId;
                } else {
                    return 1;
                }
            }
        } else {
            return 1;
        }
    }

    public function getLavadores()
    {
        return $this->db
            ->where('activo', 1)
            ->get('lavadores')
            ->result();
    }

    public function listadoservicios($parametro)
    {
        $this->db->select(
            'servicio_lavado.id as id_servicio, horarios_lavadores.hora as hora_servicio, horarios_lavadores.fecha as fecha_servicio,  servicio_lavado.folio_mostrar,servicio_lavado.cancelado, servicio_lavado.confirmado, servicio_lavado.id_usuario,servicio_lavado.comentarios,servicio_lavado.id_estatus_lavado, servicio_lavado.id_servicio_calificacion,servicio_lavado.fecha_programacion, servicio_lavado.fecha_entrega_unidad, autos.placas,autos.numero_serie,autos.kilometraje, cat_colores.color, cat_marcas.marca,cat_anios.anio,cat_modelos.modelo, cat_estatus_lavado.estatus as estatus_lavado,cat_metodo_pago.id as id_metodo_pago, cat_metodo_pago.metodo_pago, lavadores.url_documento,lavadores.id_id as id_gafete'
        );

        $this->db->from('servicio_lavado');
        $this->db->join('cat_estatus_lavado', 'servicio_lavado.id_estatus_lavado = cat_estatus_lavado.id', 'LEFT');
        $this->db->join('horarios_lavadores', 'horarios_lavadores.id = servicio_lavado.id_horario');
        $this->db->join('autos', 'servicio_lavado.id_auto = autos.id');
        $this->db->join('cat_marcas', 'cat_marcas.id = autos.id_marca');
        $this->db->join('cat_modelos', 'cat_modelos.id = autos.id_modelo');
        $this->db->join('cat_anios', 'cat_anios.id = autos.id_anio');
        $this->db->join('cat_colores', 'cat_colores.id = autos.id_color');
        $this->db->join('cat_metodo_pago', 'cat_metodo_pago.id = servicio_lavado.id_tipo_pago', 'LEFT');
        $this->db->join('lavadores', 'lavadores.lavadorId = servicio_lavado.id_lavador');
        
        // $this->db->join('evidencia_proceso_servicio', 'evidencia_proceso_servicio.id_servicio = servicio_lavado.id', 'LEFT');
        // $this->db->join('evidencia_inventario', 'evidencia_inventario.id_orden = servicio_lavado.id', 'LEFT');
        // $this->db->join('diagnostico', 'diagnostico.id_orden = servicio_lavado.id', 'LEFT');

        if (isset($parametro['id_usuario'])) {
            $this->db->where('servicio_lavado.id_usuario', $parametro['id_usuario']);
        }

        if (isset($parametro['servicio_id'])) {
            $this->db->where('servicio_lavado.id', $parametro['servicio_id']);
        }

        if (isset($parametro['realizados'])  && $parametro['realizados'] == 1) {
            // $this->db->where('servicio_lavado.pago IS NOT null', null, false);
            $this->db->where('servicio_lavado.id_estatus_lavado', 6);
            // $this->db->where('servicio_lavado.completo', 1);
        }

        if (isset($parametro['activos'])  && $parametro['activos'] == 1) {
            $this->db->where('servicio_lavado.cancelado', 0);
            // $this->db->where('servicio_lavado.completo', 0);
            $this->db->where_in('servicio_lavado.id_estatus_lavado', [1, 2, 3, 4, 5]);
        }

        // $this->db->where('servicio_lavado.id_estatus_lavado', 1);
        
        // $this->db->group_by('id_servicio');
        $this->db->order_by('servicio_lavado.id','desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getprecioservicio($id_servicio = '')
    {
        $this->db->select(
            'servicio_lavado.id as id_servicio_lavado,autos.placas,cat_modelos.modelo,precios_servicios.precio'
        );

        $this->db->from('servicio_lavado');
        $this->db->join('autos', 'servicio_lavado.id_auto = autos.id');
        $this->db->join('cat_modelos', 'cat_modelos.id = autos.id_modelo');
        $this->db->join('precios_servicios', 'precios_servicios.id_tipo_auto = cat_modelos.id_tipo_auto and servicio_lavado.id_servicio = precios_servicios.id_servicio');

        if ($id_servicio != '') {
            $this->db->where('servicio_lavado.id', $id_servicio);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function fechaEntregaUnidad($placa = '')
    {

        $this->db->select(
            'servicio_lavado.fecha_entrega_unidad,autos.placas'
        );

        $this->db->from('servicio_lavado');
        $this->db->join('autos', 'servicio_lavado.id_auto = autos.id');

        if ($placa != '') {
            $this->db->where('autos.placas', $placa);
        }

        $query = $this->db->order_by("servicio_lavado.fecha_entrega_unidad", "desc")
            ->limit(1)
            ->get();
        return $query->result();
    }

    public function getSiglasSucursal($id_sucursal = '')
    {
        $q = $this->db->where('id', $id_sucursal)->select('nomenclatura')->get('sucursales');
        if ($q->num_rows() == 1) {
            return $q->row()->nomenclatura;
        } else {
            return '';
        }
    }

    public function getFolio()
    {
        $q = $this->db->order_by('id', 'desc')->where('folio_control is not null')->limit(1)->select('folio_control')->get('servicio_lavado');
        if ($q->num_rows() == 0) {
            return 0;
        } else {
            return $q->row()->folio_control;
        }
    }


    public function getdetalleservicio($parametro)
    {
        $this->db->select(
            'servicio_lavado.id as id_servicio, 
            servicio_lavado.folio_mostrar,
            servicio_lavado.cancelado, 
            servicio_lavado.confirmado, 
            servicio_lavado.id_usuario,
            servicio_lavado.latitud_lavador,
            servicio_lavado.comentarios,
            servicio_lavado.id_estatus_lavado,
            servicio_lavado.id_servicio_calificacion,
            servicio_lavado.fecha_programacion, 
            servicio_lavado.fecha_entrega_unidad, 
            servicio_lavado.id_tipo_pago,
            servicio_lavado.id_auto,
            servicios.servicioNombre,
            horarios_lavadores.hora as hora_servicio,
            horarios_lavadores.fecha as fecha_servicio,  
            autos.placas,
            autos.numero_serie,
            autos.kilometraje,
            cat_colores.color, 
            cat_marcas.marca,
            cat_anios.anio,
            cat_modelos.modelo,
            ubicacion_servicio.numero_int,
            ubicacion_servicio.numero_ext,
            ubicacion_servicio.calle,
            ubicacion_servicio.colonia,
            ubicacion_servicio.id_sucursal,
            ubicacion_servicio.id_estado,
            ubicacion_servicio.latitud,
            ubicacion_servicio.longitud,
            ubicacion_servicio.id_municipio, 
            sucursales.sucursal,
            cat_municipios.municipio,
            cat_estatus_lavado.estatus as estatus_lavado, 
            cat_metodo_pago.metodo_pago,
            estados.estado,
            sucursales.sucursal,
            cat_municipios.municipio,
            cat_estatus_lavado.estatus as estatus_lavado, 
            cat_metodo_pago.id as id_metodo_pago,
            estados.estado
           '
        );

        $this->db->from('servicio_lavado');
        $this->db->join('cat_estatus_lavado', 'servicio_lavado.id_estatus_lavado = cat_estatus_lavado.id', 'LEFT');
        $this->db->join('horarios_lavadores', 'horarios_lavadores.id = servicio_lavado.id_horario');
        $this->db->join('autos', 'servicio_lavado.id_auto = autos.id');
        $this->db->join('cat_marcas', 'cat_marcas.id = autos.id_marca');
        $this->db->join('cat_modelos', 'cat_modelos.id = autos.id_modelo');
        $this->db->join('cat_anios', 'cat_anios.id = autos.id_anio');
        $this->db->join('cat_colores', 'cat_colores.id = autos.id_color');
        $this->db->join('cat_metodo_pago', 'cat_metodo_pago.id = servicio_lavado.id_tipo_pago', 'LEFT');
        $this->db->join('servicios', 'servicios.servicioId = servicio_lavado.id_servicio');
        $this->db->join('ubicacion_servicio', 'ubicacion_servicio.id = servicio_lavado.id_ubicacion');

        $this->db->join('estados', 'estados.id = ubicacion_servicio.id_estado');
        $this->db->join('sucursales', 'sucursales.id = ubicacion_servicio.id_sucursal');
        $this->db->join('cat_municipios', 'cat_municipios.id = ubicacion_servicio.id_municipio');


        if (isset($parametro['servicio_id'])) {
            $this->db->where('servicio_lavado.id', $parametro['servicio_id']);
        }
        $query = $this->db->get();
        return $query->result();
    }
}
