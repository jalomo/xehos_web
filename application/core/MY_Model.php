<?php

class MY_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function buscarTodos($table_name, $return_array = false)
    {
        $query = $this->db->get($table_name, 100);

        if ($return_array) {
            return $query->result_array();
        } else {

            return $query->result();
        }
    }


    public function getById($table_name, $id)
    {
        return  $this->db->select('*')
            ->where('id', $id)
            ->get($table_name)->result();
    }

    public function getwhere($table_name, $column_name, $value)
    {
        return  $this->db->select('*')
            ->where($column_name, $value)
            ->get($table_name)->result();
    }

    public function getwherearray($table_name, $array)
    {
        return  $this->db->select('*')
            ->where($array)
            ->get($table_name)->result();
    }

    public function store($table_name, $parametros)
    {
        return $this->db->insert($table_name, $parametros);
    }

    public function update($table_name, $where_column, $id, $parametros)
    {
        $this->db->where($where_column, $id);
        return $this->db->update($table_name, $parametros);
    }
}
