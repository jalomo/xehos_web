<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function enviar_correo($correo, $titulo, $cuerpo, $archivo = array())
{

    $configGmail = array(
        'protocol' => 'smtp',
        'smtp_host' => 'smtp.gmail.com',
        'smtp_port' => 587,
        'smtp_user' => CONST_CORREO_SALIDA,
        'smtp_pass' => CONST_PASS_CORREO,
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'smtp_crypto' => 'tls',
        'newline' => "\r\n"
    );
    if ($correo != 'notienecorreo@notienecorreo.com.mx') {
        $CI = &get_instance();
        $CI->load->library('email');
        $CI->email->initialize($configGmail);

        $CI->email->from(CONST_CORREO_SALIDA, CONST_AGENCIA);
        $CI->email->to($correo);


        foreach ($archivo as $key => $value) {
            $CI->email->attach($value);
        }
        //si quieres que te envíen una copia a otro correo descomenta abajo y ponlo
        $CI->email->subject($titulo);
        $CI->email->message($cuerpo);
        $CI->email->send();
    }
    //var_dump($CI->email->print_debugger());exit();
}
/************Fin correo_helper.php*********************/
