<?php

function dd($data)
{
    echo "<pre>";
    print_r($data);
    echo "<pre>";
    die();
}

function parsetoselect($data, $id = '', $name = '')
{
    $new_array = [];
    if (isset($data) && is_array($data)) {
        $new_array[0] = "seleccionar";
        foreach ($data as $key => $item) {
            $item_id = $item[$id];
            $value = $item[$name];
            $new_array[$item_id] = $value;
        }
    }

    return $new_array;
}

function getFirstFromArray($query_response)
{
    if (isset($query_response) && count($query_response) > 0) {
        return $query_response[0];
    }
}

function encrypt($data)
{
    $id = (float)$data * CONST_ENCRYPT;
    $url_id = base64_encode($id);
    $url = str_replace("=", "", $url_id);
    return $url;
}

function decrypt($data)
{
    $url_id = base64_decode($data);
    $id = (float)$url_id / CONST_ENCRYPT;
    return $id;
}

function obtenerFechaEnLetra($fecha)
{
    $num = date("j", strtotime($fecha));
    $anno = date("Y", strtotime($fecha));
    $mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
    $mes = $mes[(date('m', strtotime($fecha)) * 1) - 1];

    return  $num . ' de ' . $mes . ' del ' . $anno;
}

function obtenerHoraEntregaServicio($fecha)
{
    $date_servicio =  date("Y-m-d H:i", strtotime($fecha . "+ 45 minutes"));
    $date = strtotime($date_servicio);
    return date('H:i', $date);
}

//TODO:helper
function validareditarbtn($fecha, $tipo_pago)
{
    $fecha_servicio = date("Y-m-d H:i", strtotime($fecha));
    $fecha_valida_terminal = date("Y-m-d H:i", strtotime($fecha . "+ 12 hours"));
    $fecha_valida_otra = date("Y-m-d H:i", strtotime($fecha . "+ 24 hours"));
    if ($tipo_pago == 2) {
        $data['show'] = 0;
        if ($fecha_servicio >= $fecha_valida_terminal) {
            $data['show'] = 1;
        }

        return $data;
    } else {
        $data['show'] = 0;
        if ($fecha_servicio >= $fecha_valida_otra) {
            $data['show'] = 1;
        }

        return $data;
    }
}

function random($num)
{
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $string = '';
    for ($i = 0; $i < $num; $i++) {
        $string .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $string;
}


function getFolioMostrar($id_sucursal = '', $paquete = false)
{
    $ins = &get_instance();
    $ins->load->model('Servicio_model', 'ms');
    $siglas = $ins->ms->getSiglasSucursal($id_sucursal);
    $folio = (int)$ins->ms->getFolio() + 1;
    return ($paquete ? 'P-' : 'S-') . $siglas . '-' . $folio;
}

function getFolioControl()
{
    $ins = &get_instance();
    $ins->load->model('Servicio_model', 'ms');
    $folio = $ins->ms->getFolio();
    return (int)$folio + 1;
}

if (!function_exists('date_eng2esp')) {
    function date_eng2esp($date)
    {
        //Convertir fecha ingles a español
        // Y-M-D a d/m/Y
        // 0-1-2
        $dateARR = explode('-', str_replace('/', '-', $date));
        if (count($dateARR) == 3) {
            return $dateARR[2] . '/' . $dateARR[1] . '/' . $dateARR[0];
        } else {
            return '';
        }
    }
}



function sms_general($celular_sms = '', $mensaje_sms = '')
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, CONST_URL_NOTIFICACION_APP);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt(
        $ch,
        CURLOPT_POSTFIELDS,
        "celular=" . $celular_sms . "&mensaje=" . $mensaje_sms . "&sucursal=XEHOS"
    );
    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close($ch);
}
