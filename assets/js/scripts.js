/*!
 * Start Bootstrap - SB Admin v6.0.2 (https://startbootstrap.com/template/sb-admin)
 * Copyright 2013-2020 Start Bootstrap
 * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-sb-admin/blob/master/LICENSE)
 */

$(document).ready(function() {
    var url = site_url + "/ApiChat/getTotalNotificaciones";
    var url_ultima = site_url + "/ApiChat/getUltimaNotificacion";
    const obtenerNotificaciones = () => {
        $.ajax({
            type: "GET",
            url: url,
            data: { telefono: telefono },
            datatype: "JSON",
            success: function(response) {
                if (response && response) {
                    let tot_anterior = sessionStorage.total_notificaciones;
                    tot = response;
                    if (tot > tot_anterior) {
                        doBounce($("#total_notify"), 10, '10px', 600);
                        $.ajax({
                            type: "GET",
                            url: url_ultima,
                            data: { telefono: telefono },
                            datatype: "JSON",
                            success: function(response) {
                                result = JSON.parse(response);
                                toastr.info(result.usuario + '<br/>' + 'Tipo: ' + result.tipo + '<br/>' + result.mensaje);
                            }
                        });
                    }
                    $("#total_notify").html(tot);
                } else {
                    tot = 0;
                    $("#total_notify").html(tot);
                }
                sessionStorage.setItem('total_notificaciones', tot)
            }
        });
    }
    
    
    typeof telefono !== 'undefined' && setInterval(obtenerNotificaciones(), 20000);

});



function doBounce(element, times, distance, speed) {
    for (var i = 0; i < times; i++) {
        element.animate({ marginTop: '-=' + distance }, speed)
            .animate({ marginTop: '+=' + distance }, speed);
    }
}
(function($) {
    "use strict";

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
    $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function() {
        if (this.href === path) {
            $(this).addClass("active");
        }
    });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
    });

    $('.select2').select2();


})(jQuery);

var myfunction = {};
var myvalidations = {};
myfunction.ajaxget = ajaxGet = async(url = '') => {
    if (url == '') {
        return false;
    }
    const data = await fetch(`${url}`);
    return data
}

myfunction.ajaxpost = async(url, parametros = {}, callbackfn) => {
    if (!url) {
        return {}
    }

    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: parametros,
        success: callbackfn
    });
}


myvalidations.validarEmail = (valor) => {
    let regex = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
    if (regex.test(valor)) {
        return true;
    } else {
        return false
    }
}

myvalidations.validarRfc = (valor) => {
    let regex = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\d]{3}))?$/
    if (regex.test(valor)) {
        return true;
    } else {
        return false
    }
}