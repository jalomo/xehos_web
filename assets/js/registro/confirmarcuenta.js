const estatus_validado = document.getElementById('estatus_validado').value;
if (estatus_validado && estatus_validado == 2) {
    let url = site_url + "ajaxRequestServicio/confirmarcuenta";
    myfunction.ajaxget(url)
        .then(response => response.json())
        .then(({ status, msg }) => {

            status == 1 && swal({
                text: msg,
                title: "Cuenta verificada",
                icon: "success",
                button: "Continuar",
                closeOnClickOutside: false
            }).then((value) => {
                return window.location.href = site_url + "login";
            });

            status == 0 && swal({
                msg,
                title: "Alerta",
                icon: "error",
                button: "Continuar",
                closeOnClickOutside: false
            }).then((value) => {
                return window.location.href = site_url + "login/cerrarsession";
            });
        })
}

if (estatus_validado == 3) {
    swal("La cuenta ya fue verificada previamente", { title: "Alerta", icon: "success" })
        .then((value) => window.location.href = site_url + "xehos/servicios");
}

if (estatus_validado == 1 || estatus_validado == 0) {
    swal("No se puede validar la cuenta con la url formada", { title: "Alerta", icon: "error" })
        .then((value) => window.location.href = site_url + "login");
}
