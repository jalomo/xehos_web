
const onchange_estado = () => {
    let estado = document.getElementById('id_estado').value;
    !estado && false
    let url = site_url + "ajaxRequestServicio/ajax_getsucursales/" + estado;
    $("#id_sucursal").empty();
    $("#id_sucursal").append("<option value=''>-- Selecciona --</option>");
    $("#id_sucursal").attr("disabled", true);

    myfunction.ajaxget(url)
        .then(response => response.json())
        .then(data_response => {
            $("#id_sucursal").empty();
            $("#id_sucursal").append("<option value=''>-- Selecciona --</option>");
            $("#id_sucursal").removeAttr("disabled");
            if (data_response && data_response.length > 0) {
                data_response.map(item => $("#id_sucursal").append("<option value= '" + item.id + "'>" + item.sucursal + "</option>"))
            } else {
                $("#id_sucursal").empty();
                $("#id_sucursal").append("<option value=''> No se encontraron datos</option>");
            }
        })
}

const onchangeMarca = () => {
    let id_marca = document.getElementById('id_marca').value;
    let url = site_url + "ajaxRequestServicio/ajax_getmodelobymarca";
    // return console.log(id_marca)
    $("#id_modelo").empty();
    $("#id_modelo").append("<option value=''>-- Selecciona --</option>");
    $("#id_modelo").removeAttr("disabled");
    !id_marca && false
    myfunction.ajaxpost(url, { id_marca }, function (data) {
        $("#id_modelo").empty();
        $("#id_modelo").append("<option value=''>-- Selecciona --</option>");
        $("#id_modelo").removeAttr("disabled");
        if (data && data.length > 0) {
            data.map(item => $("#id_modelo").append("<option value= '" + item.id + "'>" + item.modelo + "</option>"))
        } else {
            $("#id_modelo").empty();
            $("#id_modelo").append("<option value=''> No se encontraron datos</option>");
        }
    })
}


const validarplaca = () => {
    let placas = document.getElementById('placas').value;
    if (placas.trim().length == 0 || placas.trim().length > 10) {
        document.getElementById("msgvalidationplacas").classList.add("valid-feedback", "d-block")
        document.getElementById("msgvalidationplacas").classList.remove("invalid-feedback")
        $("#msgvalidationplacas").html("<span>Placas no validas</span>");
        document.getElementById("placas_validas").value = 0
        return false;
    }

    let url = site_url + "ajaxRequestServicio/ajax_existePlaca";
    myfunction.ajaxpost(url, { placas }, function (data) {
        if (data.length >= 1) {
            document.getElementById("msgvalidationplacas").classList.add("invalid-feedback", "d-block")
            document.getElementById("msgvalidationplacas").classList.remove("valid-feedback")
            $("#msgvalidationplacas").html("<span>Las Placas ya se registraron.</span>");
            document.getElementById("placas_validas").value = 1

        } else {
            document.getElementById("msgvalidationplacas").classList.add("valid-feedback", "d-block")
            document.getElementById("msgvalidationplacas").classList.remove("invalid-feedback")
            $("#msgvalidationplacas").html("<span>Placas validas</span>");
            document.getElementById("placas_validas").value = 0
        }
    })
}


const handleregistro = () => {
    let url = site_url + "ajaxRequestServicio/ajax_validar_registrar";
    const form = $("#frmregistro").serializeArray();
    myfunction.ajaxpost(url, form, (response) => {
        // console.log(error, validaciones)
        $(".text-danger").text("");
        response.error && handleValidations(response.validaciones)
        !response.error && handleResponseSuccess(response);
    })
}

const verificar_cuenta = (token) => {
    let url = site_url + "ajaxRequestServicio/ajax_correo_confirmacion";
    myfunction.ajaxpost(url, {
        telefono: document.getElementById('telefono').value,
        correo: document.getElementById('email').value,
        token
    }, (response) => window.location.href = site_url + "login")
}

const handleValidations = (validaciones) => {
    Object.keys(validaciones).map(item => {
        $("#error_" + item).text(validaciones[item]);
    })
}

const handleResponseSuccess = ({ msg, token }) => {
    // verificar_cuenta(token);
    // swal(msg, { title: "Enviamos un mensaje para confirmar tus datos", icon: "success" })
    swal({
        text: msg,
        title: "Enviaremos un correo para confirmar tus datos",
        icon: "success",
        closeOnClickOutside: false
    })
        .then((value) => {
            var loading_item = document.createElement("i");
            loading_item.classList.add("fas");
            loading_item.classList.add("fa-spinner");
            loading_item.classList.add("fa-spin");
            loading_item.classList.add("spinner-lg");
            swal({
                content: loading_item,
                buttons: false,
                closeOnClickOutside: false
            });
            verificar_cuenta(token);
        });
};