const openmodal = () => {
    $("#passwordmodal").modal('show')
}

const validarrepeatcontrasena = () => {
    // $("#passwordmodal").modal('show')
    // msgmodal
    // new_password
    // repeat_password
    const new_pass = document.getElementById('new_password').value
    const repeat_password = document.getElementById('repeat_password').value
    if (repeat_password.length <= 6 && new_pass.length <= 6) {
        $("#msgmodal").text('6 caracteres minimo para contraseña segura').addClass('alert alert-warning')
        return false
    }

    if (new_pass == '' && repeat_password !== '') {
        $("#msgmodal").text('Ingresa la nueva contraseña').addClass('alert alert-warning')
        return false
    }

    if (new_pass !== repeat_password) {
        $("#msgmodal").text('Las contraseñas no coinciden').addClass('alert alert-warning')
        return false
    } else {
        $("#msgmodal").text('Contraseña validas').addClass('alert alert-success')
        return false;
    }


}

const actualizarcontrasena = () => {
    const new_pass = document.getElementById('new_password').value
    const repeat_password = document.getElementById('repeat_password').value
    if (new_pass == repeat_password) {
        let url = site_url + "ajaxRequestServicio/ajax_updatepassword";
        myfunction.ajaxpost(url, {
            password: new_pass
        }, function (data) {
            if (data.status == 1) {
                swal({
                    title: "Éxito",
                    text: data.msg,
                    closeOnClickOutside: false,
                    icon: "success",
                    button: "Continuar",
                }).then(value => {
                    return window.location.href = site_url + "xehos/miperfil";
                })
                

            } else {
                swal({
                    title: "Error!",
                    text: "ha ocurrido un error cambiando la contraseña",
                    closeOnClickOutside: false,
                    icon: "warning",
                    dangerMode: true,
                    button: "Continuar",
                }).then(value => {
                    return window.location.href = site_url + "xehos/miperfil";
                })
            }
            $("#passwordmodal").modal('hide')
        })
    } else {
        $("#msgmodal").text('Las contraseñas no coinciden').addClass('alert alert-warning')
    }
}
