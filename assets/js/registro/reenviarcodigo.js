const reenviar_correo = () => {
    // return console.log("aqui")
    swal({
        text: 'Reenviar liga de confirmación a correo.',
        content: "input",
        button: {
            text: "enviar",
            closeModal: false,
        },
    })
        .then(data_correo => {
            if (!data_correo || !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(data_correo)) {
               return swal({
                    title: "Ingresa un correo valido",
                    icon: "error",
                });
            }

            var loading_item = document.createElement("i");
            loading_item.classList.add("fas");
            loading_item.classList.add("fa-spinner");
            loading_item.classList.add("fa-spin");
            loading_item.classList.add("spinner-lg");
            swal({
                content: loading_item,
                buttons: false,
                closeOnClickOutside: false
            });

            let url = site_url + "ajaxRequestServicio/ajax_renviar_codigo_registro";
            myfunction.ajaxpost(url, { correo: data_correo }, (response) => {
                !response.error && handleResponseSuccess();
            })
        })
}

const handleResponseSuccess = () => {
    swal({
        title: "Correo de confirmación enviado",
        icon: "success",
        closeOnClickOutside: false
    });
};