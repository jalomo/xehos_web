
const onchange_estado = () => {
    let id_estado = document.getElementById('id_estado').value;
    let url = site_url + "ajaxRequestServicio/ajax_getmunicipioByEstado";

    $("#id_municipio, #id_sucursal").empty();
    $("#id_municipio, #id_sucursal").append("<option value=''>-- Selecciona --</option>");
    // $("#id_municipio, #id_sucursal").attr("disabled", true);
    $("#id_municipio, #id_sucursal").removeAttr("disabled");
    !id_estado && false
    myfunction.ajaxpost(url, { id_estado }, function (data) {
        $("#id_municipio").empty();
        $("#id_municipio").append("<option value=''>-- Selecciona --</option>");
        $("#id_municipio").removeAttr("disabled");
        if (data && data.length > 0) {
            data.map(item => $("#id_municipio").append("<option value= '" + item.id + "'>" + item.municipio + "</option>"))
        } else {
            $("#id_municipio").empty();
            $("#id_municipio").append("<option value=''> No se encontraron datos</option>");
        }
    })
}