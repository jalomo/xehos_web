
const procesarpago = () => {
    let id_cupon = document.getElementById('id_cupon').value
    let id_servicio = document.getElementById('id_servicio').value
    let total = document.getElementById('total').value
    let subtotal = document.getElementById('subtotal').value
    let descuento = document.getElementById('descuento').value
    let id_tipo_pago = document.querySelector('input[name="tipopago_radio"]:checked') && document.querySelector('input[name="tipopago_radio"]:checked').value

    var loading_item = document.createElement("i");
    loading_item.classList.add("fas");
    loading_item.classList.add("fa-spinner");
    loading_item.classList.add("fa-spin");
    loading_item.classList.add("spinner-lg");
    swal({
        content: loading_item,
        buttons: false,
        closeOnClickOutside: false
    });

    let url = site_url + "ajaxRequestServicio/ajax_procesarventa";
    myfunction.ajaxpost(url, {
        id_cupon,
        id_servicio,
        id_tipo_pago,
        total,
        subtotal,
        descuento
    }, function ({ status, msg }) {
        if (status == 1) {
            swal({
                title: "Éxito!",
                text: msg,
                closeOnClickOutside: false,
                icon: "success",
                button: "Continuar",
            }).then(value => {
                return window.location.href = site_url + "xehos/listaservicios";
            })
        } else {
            swal({
                title: "Error!",
                text: "ha ocurrido un error procesando el pago",
                closeOnClickOutside: false,
                icon: "warning",
                dangerMode: true,
                button: "Continuar",
            }).then(value => {
                return window.location.href = site_url + "xehos/listaservicios";
            })
        }
    })

}

paypal.Buttons({
    createOrder: function (data, actions) {
        // This function sets up the details of the transaction, including the amount and line item details.
        return actions.order.create({
            purchase_units: [{
                amount: {
                    value: document.getElementById('subtotal').value
                }
            }]
        });
    },
    onApprove: function (data, actions) {
        // This function captures the funds from the transaction.
        return actions.order.capture().then(function (details) {
            // This function shows a transaction success message to your buyer.
            swal({
                title: "Éxito!",
                text: 'Completando transaccion ' + details.payer.name.given_name,
                closeOnClickOutside: false,
                icon: "info",
                button: "Continuar",
            }).then(value => {
                procesarpago();
            })
        });
    }
},
    {
        locale: 'en_MX',
        style: { size: 'responsive' }
    }).render('#paypal-button-container');



const showformtipopago = () => {
    let metodo = document.querySelector('input[name="tipopago_radio"]:checked') && document.querySelector('input[name="tipopago_radio"]:checked').value
    // console.log(metodo)
    const element_paypal = document.getElementById("paypalcontainer");
    const element_terminal = document.getElementById("terminal");
    const element_lluvia = document.getElementById("seguro_lluvia");

    if (metodo == 1) {
        element_paypal.classList.remove("d-none");
        element_terminal.classList.add("d-none");
        element_lluvia.classList.add("d-none");
    } else if (metodo == 2) {
        element_paypal.classList.add("d-none");
        element_terminal.classList.remove("d-none");
        element_lluvia.classList.add("d-none");
    } else if (metodo == 3) {
        element_paypal.classList.add("d-none");
        element_terminal.classList.add("d-none");
        element_lluvia.classList.remove("d-none");
    }
}

const validarcupon = () => {
    let cupon = document.getElementById('cupon').value
    let precio_servicio = document.getElementById('total').value
    let user_id = document.getElementById('user_id').value

    if (cupon.trim().length == 0) {
        swal({
            title: "Error!",
            text: "Ingresar un cupon valido",
            closeOnClickOutside: false,
            icon: "warning",
            dangerMode: true,
            button: "Continuar",
        })
    }

    $("#div_response_cupon").html('<div class="progress"> <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div> </div>');
    let url = site_url + "ajaxRequestServicio/ajax_cupondescuento";
    myfunction.ajaxpost(url, {
        cupon,
        user_id
    }, function (data) {
        let {
            descuento,
            id_cupon
        } = data
        if (id_cupon && descuento !== 0) {
            let new_total = (parseInt(precio_servicio) * parseInt(descuento)) / 100;//parseInt(precio_servicio) - parseInt(descuento)
            $("#div_response_cupon").html('<div class="alert alert-success " role="alert">Cupon valido.</div>');
            document.getElementById('subtotal').value = new_total
            document.getElementById('descuento').value = descuento
            document.getElementById('id_cupon').value = id_cupon
            $("#txt_descuento").text(descuento + ' %')
            if (parseInt(descuento) == 100) {
                const element_paypal = document.getElementById("paypalcontainer");
                const element_terminal = document.getElementById("terminal");
                const element_lluvia = document.getElementById("seguro_lluvia");
                const element_descuento_cupon = document.getElementById("cupon100");

                element_descuento_cupon.classList.remove("d-none");
                element_paypal.classList.add("d-none");
                element_terminal.classList.add("d-none");
                element_lluvia.classList.add("d-none");
            }
            return $("#calcular_total").text('$ ' + new_total)

        } else {
            $("#div_response_cupon").html('<div class="alert alert-danger" role="alert">Cupon no valido.</div>');
        }
    })
}

