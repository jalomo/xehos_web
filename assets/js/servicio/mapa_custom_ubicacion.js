var map;
var marker;
// function initMap() {

//     // var myLatlng =  { lat: 19.3849022, lng: -103.5722297 }
//     var myLatlng = new google.maps.LatLng(19.3849022, -103.5722297);

//     map = new google.maps.Map(document.getElementById("mapa"), {
//         center: myLatlng,
//         zoom: 17,
//     });
//     const card = document.getElementById("pac-card");
//     const input = document.getElementById("pac-input");
//     map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
//     const autocomplete = new google.maps.places.Autocomplete(input);
//     // Bind the map's bounds (viewport) property to the autocomplete object,
//     // so that the autocomplete requests use the current map bounds for the
//     // bounds option in the request.
//     autocomplete.bindTo("bounds", map);
//     // Set the data fields to return when the user selects a place.
//     autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
//     const infowindow = new google.maps.InfoWindow();
//     const infowindowContent = document.getElementById("infowindow-content");
//     infowindow.setContent(infowindowContent);

//     if (navigator.geolocation) {
//         navigator.geolocation.getCurrentPosition(function (position) {
//             let { coords } = position;
//             let pos = {
//                 lat: parseFloat(coords.latitude),
//                 lng: parseFloat(coords.longitude)
//             }
//             latLng = new google.maps.LatLng(pos.lat, pos.lng);
//             marker = new google.maps.Marker({
//                 map,
//                 draggable: false,
//                 position: latLng,
//                 anchorPoint: new google.maps.Point(0, -29),
//             });
//             let latitud_servicio = document.getElementById('latitud').value
//             let longitud_servicio = document.getElementById('longitud').value
//             SetMarker(latitud_servicio, longitud_servicio, "ubicacion de servicio")

//         }, () => {
//             console.log("No se puede obtener la ubicacion actual")
//         });
//     } else {
//         console.log("geolocation no soportada")
//     }
// }


const actualizacionautomatica = () => {
    getlavadorposition();
}
setInterval(actualizacionautomatica, 3000);


function initMap() {
    let latitud_servicio = document.getElementById('latitud').value
    let longitud_servicio = document.getElementById('longitud').value
    const customLatLng = { lat: parseFloat(latitud_servicio), lng: parseFloat(longitud_servicio) };//{ lat: latitud_servicio, lng: longitud_servicio };
    map = new google.maps.Map(document.getElementById("mapa"), {
        zoom: 17,
        center: customLatLng,
    });
    //autocompletar
    const card = document.getElementById("pac-card");
    const input = document.getElementById("pac-input");
    if(!!card && !!input){
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo("bounds", map);
        autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
    }
    const infowindow = new google.maps.InfoWindow();
    const infowindowContent = document.getElementById("infowindow-content");
    infowindow.setContent(infowindowContent);
    //fin autocompletar
    marker = new google.maps.Marker({
        position: customLatLng,
        map,
        title: "ubicacion",
    });

    //set autocomplete function
    
    if(!!card && !!input){
        autocomplete.addListener("place_changed", () => {
            infowindow.close();
            marker.setVisible(false);
            const place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No hay referencia para esta ubicacion: '" + place.name + "'");
                return;
            }
    
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
    
            const { lat, lng } = place.geometry.location
            
            SetMarker(lat(), lng(), msg = 'ubicacion seleccionada')
            getAdress(lat(), lng()).then(({ numero_ext, calle, colonia }) => {
                $("#calle").val(calle);
                $("#numero_ext").val(numero_ext);
                $("#colonia").val(colonia);
            })
            let address = "";
            if (place.address_components) {
                address = [
                    (place.address_components[0] &&
                        place.address_components[0].short_name) ||
                    "",
                    (place.address_components[1] &&
                        place.address_components[1].short_name) ||
                    "",
                    (place.address_components[2] &&
                        place.address_components[2].short_name) ||
                    "",
                ].join(" ");
            }
            infowindowContent.children["place-icon"].src = place.icon;
            infowindowContent.children["place-name"].textContent = place.name;
            infowindowContent.children["place-address"].textContent = address;
            infowindow.open(map, marker);
        });
    }
    //fin set autocompelte function

}

const getlavadorposition = () => {
    let id_servicio = document.getElementById('id_servicio').value
    let id_estatus_lavado = document.getElementById('id_estatus_lavado').value
    if (id_servicio !== '' && (id_estatus_lavado == 2 || id_estatus_lavado == 3)) {
        let url = site_url + "ajaxRequestServicio/ajax_getpositionlavador"
        myfunction.ajaxpost(url, { id_servicio }, function (data) {
            if (data && data.length > 0) {
                let { latitud_lavador, longitud_lavador } = data[0]
                if (latitud_lavador !== '' && longitud_lavador !== '') {
                    $("#latitud").val(latitud_lavador);
                    $("#longitud").val(longitud_lavador);
                    SetMarker(latitud_lavador, longitud_lavador, "posicion del lavador")
                }
            }
        })
    }
}


function SetMarker(lat = '', lng = '', msg = 'ubicacion') {
    //Remove previous Marker.
    if (marker != null) {
        marker.setMap(null);
    }

    //Set Marker on Map.
    var myLatlng = new google.maps.LatLng(lat, lng);
    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: msg
    });

    //Create and open InfoWindow.
    let infoWindow = new google.maps.InfoWindow();
    infoWindow.setContent(msg);
    infoWindow.open(map, marker);
};


const getAdress = async (lat = '', lng = '') => {
    console.log("entro")
    if (lat == '' && lng == '') {
        return false
    }
    let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM`;
    let response = await myfunction.ajaxget(url);
    let data = await response.json();
    let resultado = data.results[0].address_components
    return {
        numero_ext: resultado[0].long_name,
        calle: resultado[1].long_name,
        colonia: resultado[2].long_name
    }

}
