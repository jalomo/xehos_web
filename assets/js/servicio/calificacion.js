
$("#btn1si").on('click',()=> {
    $("#btn1si").removeClass('btn-outline-success').addClass('btn-success')
    $("#btn1no").addClass('btn-outline-danger').removeClass('btn-danger')
    $("#operador_servicio").val(1)
});
$("#btn1no").on('click',()=> {
    $("#btn1si").add('btn-outline-success').removeClass('btn-success')
    $("#btn1no").removeClass('btn-outline-danger').addClass('btn-danger')
    $("#operador_servicio").val(0)
});

$("#btn2si").on('click',()=> {
    $("#operador_servicio").val(1)
    $("#btn2si").removeClass('btn-outline-success').addClass('btn-success')
    $("#btn2no").addClass('btn-outline-danger').removeClass('btn-danger')
});
$("#btn2no").on('click',()=> {
    $("#operador_servicio").val(0)
    $("#btn2si").add('btn-outline-success').removeClass('btn-success')
    $("#btn2no").removeClass('btn-outline-danger').addClass('btn-danger')
});

$("#btn3si").on('click',()=> {
    $("#operador_servicio").val(1)
    $("#btn3si").removeClass('btn-outline-success').addClass('btn-success')
    $("#btn3no").addClass('btn-outline-danger').removeClass('btn-danger')
});
$("#btn3no").on('click',()=> {
    $("#operador_servicio").val(0)
    $("#btn3si").add('btn-outline-success').removeClass('btn-success')
    $("#btn3no").removeClass('btn-outline-danger').addClass('btn-danger')
});

const guardarcalificacion = () => {
    let id_servicio = $("#id_servicio").val();
    let puntaje = $("#puntaje").val()
    let operador_servicio = $("#operador_servicio").val()
    let satisfecho = $("#satisfecho").val()
    let recomendaria = $("#recomendaria").val()
    let descripcion_experiencia = $("#descripcion_experiencia").val()

    let url = site_url + "ajaxRequestServicio/guardarcalificacion";
    myfunction.ajaxpost(url, {
        id_servicio,
        puntaje,
        operador_servicio,
        satisfecho,
        recomendaria,
        comentarios:descripcion_experiencia
        }, function ({ status, msg }) {
            if (status == 1) {
                swal({
                    title: "Éxito!",
                    text: msg,
                    closeOnClickOutside:false,
                    icon: "success",
                    button: "Continuar",
                }).then(value=>{
                    window.location.href = site_url + "xehos/listaserviciosrealizados";
                })
            } else {
                swal({
                    title: "Error!",
                    text: "ha ocurrido un error",
                    closeOnClickOutside:false,
                    icon: "warning",
                    dangerMode: true,
                    button: "Continuar",
                }).then(value=>{
                    window.location.href = site_url + "xehos/listaserviciosrealizados";
                })
            }
    })

}



const handlestars = (item) => {
    let id = item.getAttribute('id');
    let full_star = "<i class='fas fa-star'></i>"
    let emtpy_star = "<i class='far fa-star'></i>"
    if (id == 'star1') {
        $("#star1").html(full_star)
        $("#star2").html(emtpy_star)
        $("#star3").html(emtpy_star)
        $("#star4").html(emtpy_star)
        $("#star5").html(emtpy_star)
        $("#puntaje").val(1)
    }

    if (id == 'star2') {
        $("#star1").html(full_star)
        $("#star2").html(full_star)
        $("#star3").html(emtpy_star)
        $("#star4").html(emtpy_star)
        $("#star5").html(emtpy_star)
        $("#puntaje").val(2)
    }

    if (id == 'star3') {
        $("#star1").html(full_star)
        $("#star2").html(full_star)
        $("#star3").html(full_star)
        $("#star4").html(emtpy_star)
        $("#star5").html(emtpy_star)
        $("#puntaje").val(3)
    }

    if (id == 'star4') {
        $("#star1").html(full_star)
        $("#star2").html(full_star)
        $("#star3").html(full_star)
        $("#star4").html(full_star)
        $("#star5").html(emtpy_star)
        $("#puntaje").val(4)
    }

    if (id == 'star5') {
        $("#star1").html(full_star)
        $("#star2").html(full_star)
        $("#star3").html(full_star)
        $("#star4").html(full_star)
        $("#star5").html(full_star)
        $("#puntaje").val(5)
    }

}