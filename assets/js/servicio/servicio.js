
const onchange_horario = () => {
    let fecha = document.getElementById('fecha').value;
    let id_sucursal = document.getElementById('id_sucursal').value;
    let url = site_url + "ajaxRequestServicio/ajax_gethorariosdisponibles";
    $("#hora").empty();
    $("#hora").append("<option value=''>-- Selecciona --</option>");
    $("#hora").attr("disabled", true);

    !fecha && false
    myfunction.ajaxpost(url, { id_sucursal, fecha }, function (data) {
        $("#hora").empty();
        $("#hora").append("<option value=''>-- Selecciona --</option>");
        $("#hora").removeAttr("disabled");
        if (data && data.length > 0) {
            data.map(item => {
                return $("#hora").append("<option value= '" + item.hora + "'>" + item.hora + "</option>")
            })
            // .replace(':', '')
            // if (document.getElementById('loaded_hora').value !== '') {
            //     let hora = document.getElementById('loaded_hora').value
            //     document.getElementById('hora').value = hora;
            //     $("#hora").attr("disabled", true);
            // }
        } else {
            $("#hora").empty();
            $("#hora").append("<option value=''> No se encontraron datos</option>");
        }
    })
}


const onchangesucursal = () => {
    $("#fecha").empty();
    $("#fecha").append("<option value=''>-- Selecciona --</option>");
    $("#fecha").removeAttr("disabled");
    let id_sucursal = document.getElementById('id_sucursal').value
    let url = site_url + "ajaxRequestServicio/ajax_getfechasBySucursal";
    // let fecha = document.getElementById('loaded_fecha').value !== '' ? document.getElementById('loaded_fecha').value : ''
    myfunction.ajaxpost(url, { fecha: '', id_sucursal, }, function (data) {
        $("#fecha").empty();
        $("#fecha").append("<option value=''>-- Selecciona --</option>");
        $("#fecha").removeAttr("disabled");
        if (data && data.length > 0) {
            data.map(item => $("#fecha").append("<option value= '" + item.fecha + "'>" + item.fecha + "</option>"))
            // if (document.getElementById('loaded_fecha').value !== '') {
            //     let fecha = document.getElementById('loaded_fecha').value
            //     document.getElementById('fecha').value = fecha;
            //     onchange_horario()
            //     $("#fecha").attr("disabled", true);
            // }
        } else {
            $("#hora").attr("disabled", true);
            $("#fecha").empty();
            $("#fecha").append("<option value=''> No se encontraron datos</option>");
        }
    })
}

const onchange_estado = () => {
    let id_estado = document.getElementById('id_estado').value;
    let url = site_url + "ajaxRequestServicio/ajax_getmunicipioByEstado";

    $("#id_municipio, #id_sucursal").empty();
    $("#id_municipio, #id_sucursal").append("<option value=''>-- Selecciona --</option>");
    // $("#id_municipio, #id_sucursal").attr("disabled", true);
    $("#id_municipio, #id_sucursal").removeAttr("disabled");
    !id_estado && false
    getSucursales();

    myfunction.ajaxpost(url, { id_estado }, function (data) {
        $("#id_municipio").empty();
        $("#id_municipio").append("<option value=''>-- Selecciona --</option>");
        $("#id_municipio").removeAttr("disabled");
        if (data && data.length > 0) {
            data.map(item => $("#id_municipio").append("<option value= '" + item.id + "'>" + item.municipio + "</option>"))
        } else {
            $("#id_municipio").empty();
            $("#id_municipio").append("<option value=''> No se encontraron datos</option>");
        }
    })
}

const getSucursales = () => {
    let id_estado = document.getElementById('id_estado').value;
    let url = site_url + "ajaxRequestServicio/ajax_getsucursales/" + id_estado;
    !id_estado && false
    myfunction.ajaxpost(url, { id_estado }, function (data) {
        $("#id_sucursal").empty();
        $("#id_sucursal").append("<option value=''>-- Selecciona --</option>");
        $("#id_sucursal").removeAttr("disabled");
        if (data && data.length > 0) {
            data.map(item => $("#id_sucursal").append("<option value= '" + item.id + "'>" + item.sucursal + "</option>"))
            // if (document.getElementById('loaded_id_sucursal').value !== '') {
            //     let sucursal = document.getElementById('loaded_id_sucursal').value
            //     document.getElementById('id_sucursal').value = sucursal;
            //     onchangesucursal()
            //     $("#id_sucursal").attr("disabled", true);
            // }
        } else {
            $("#id_sucursal").empty();
            $("#id_sucursal").append("<option value=''> No se encontraron datos</option>");
        }
    })
}


const cargarvehiculoinfo = () => {
    let id_auto = document.getElementById('id_auto').value
    let url = site_url + "ajaxRequestServicio/ajax_getvehiculo/" + id_auto;
    myfunction.ajaxget(url)
        .then(success => success.json())
        .then(data => {
            let validate_data = data.length > 0 ? data[0] : [];
            const element = document.getElementById("infovehiculo");

            if (data.length == 0) {
                element.classList.add("d-none");
                return false;
            }
            element.classList.remove("d-none");
            let { anio, color, placas, numero_serie, modelo, marca } = validate_data;

            document.getElementById('info_color').value = color && color;
            document.getElementById('info_placas').value = placas && placas;
            document.getElementById('info_modelo').value = modelo && modelo;
            document.getElementById('info_marca').value = marca && marca;
            document.getElementById('info_anio').value = anio && anio;
            document.getElementById('info_num_serie').value = numero_serie && numero_serie;
        })
}

const guardarvehiculomodal = () => {
    // guardarvehiculomodal

    let data = {
        id_color: document.getElementById('id_color').value,
        placas: document.getElementById('placas').value,
        id_modelo: document.getElementById('id_modelo').value,
        id_marca: document.getElementById('id_marca').value,
        id_anio: document.getElementById('id_anio').value,
        numero_serie: document.getElementById('numero_serie').value,
        kilometraje: document.getElementById('kilometraje').value,
        id_usuario: document.getElementById('id_usuario').value
    }
    // console.log(data)
    if (data.id_usuario == '' || data.placas == '' || data.id_color == '' || data.id_modelo == '' || data.id_marca == '') {
        document.getElementById("msgmodal").classList.add("alert", "alert-danger")
        $("#msgmodal").html("<span>Llenar los campos requeridos.</span>");
        return false
    }

    if (document.getElementById("placas_validas").value == 1) {
        $("#msgmodal").html("<span>La placa no es valida.</span>");
        return false
    }

    let url = site_url + "ajaxRequestServicio/ajax_guardarvehiculo/";
    myfunction.ajaxpost(url, data, function (response) {

        if (response && response.status == 1) {
            document.getElementById("msgmodal").classList.add("alert", "alert-success")
            $("#msgmodal").html("<span>Guardado correctamente.</span>");
            window.location.reload();
        } else {
            document.getElementById("msgmodal").classList.add("alert", "alert-danger")
            $("#msgmodal").html("<span>Ha ocurrido un problema.</span>");
        }
    })
}

const onchangevalidaremail = () => {
    let email = document.getElementById('email_facturacion').value;
    let valido = myvalidations.validarEmail(email);
    if (!valido) {
        document.getElementById("msgvalidationemail").classList.add("invalid-feedback", "d-block")
        document.getElementById("msgvalidationemail").classList.remove("valid-feedback")
        $("#msgvalidationemail").html("<span>El email no es valido.</span>");
    } else {
        document.getElementById("msgvalidationemail").classList.add("valid-feedback", "d-block")
        document.getElementById("msgvalidationemail").classList.remove("invalid-feedback")
        $("#msgvalidationemail").html("<span>Email valido</span>");
    }
}
const validarrfc = () => {
    let rfc = document.getElementById('rfc').value;
    let valido = myvalidations.validarRfc(rfc);
    if (!valido) {
        document.getElementById("msgvalidationrfc").classList.add("invalid-feedback", "d-block")
        document.getElementById("msgvalidationrfc").classList.remove("valid-feedback")
        $("#msgvalidationrfc").html("<span>El RFC no es valido.</span>");
    } else {
        document.getElementById("msgvalidationrfc").classList.add("valid-feedback", "d-block")
        document.getElementById("msgvalidationrfc").classList.remove("invalid-feedback")
        $("#msgvalidationrfc").html("<span>RFC valido</span>");
    }
}

const onclickshowformfacturacion = () => {
    document.getElementById("formfacturacion").classList.add("d-block")
    document.getElementById("btn-guardardatosfacturacion").classList.add("d-block")
}

const closemodal_loadinfo = () => {
    if (document.getElementById('datos_facturacion').value == '') {
        return alert("selecciona el dato de facturacion");
    }

    let id_selected = document.getElementById('datos_facturacion').value;
    document.getElementById("infodatosfacturacion").classList.remove("d-none")
    let url = site_url + "ajaxRequestServicio/ajax_getdatosfacturacion/" + id_selected;
    myfunction.ajaxget(url).then(response => response.json())
        .then(({ id, razon_social, domicilio, email_facturacion, id_cfdi, id_metodo_pago, rfc, cp }) => {
            // .then((data) => {
            $("#modalfacturacion").modal('hide');
            // console.log(data)
            console.log(id, razon_social, domicilio, email_facturacion, id_cfdi, id_metodo_pago, rfc, cp)
            document.getElementById('selected_razon_social').value = razon_social
            document.getElementById('selected_domicilio').value = domicilio
            document.getElementById('selected_email_facturacion').value = email_facturacion
            document.getElementById('selected_id_cfdi').value = id_cfdi
            document.getElementById('selected_id_metodo_pago').value = id_metodo_pago
            document.getElementById('selected_rfc').value = rfc
            document.getElementById('selected_cp').value = cp
            document.getElementById('id_selected_facturacion').value = id

        })
}

const guardardatosfacturacion = () => {
    let data = {
        id_usuario: document.getElementById('id_usuario').value,
        razon_social: document.getElementById('razon_social').value,
        domicilio: document.getElementById('domicilio').value,
        email_facturacion: document.getElementById('email_facturacion').value,
        id_cfdi: document.getElementById('id_cfdi').value,
        id_metodo_pago: document.getElementById('id_metodo_pago').value,
        cp: document.getElementById('cp').value,
        rfc: document.getElementById('rfc').value
    }

    let email = document.getElementById('email_facturacion').value;
    let valido = myvalidations.validarEmail(email);
    if (!valido) {
        document.getElementById("msgmodalfacturacion").classList.add("alert", "alert-danger")
        $("#msgmodalfacturacion").html("<span>El email no es valido.</span>");
        return false
    }

    if (data.rfc == ''
        || data.razon_social == ''
        || data.id_metodo_pago == ''
        || data.id_cfdi == ''
        || data.domicilio == ''
        || data.email_facturacion == ''
        || data.cp == '') {
        document.getElementById("msgmodalfacturacion").classList.add("alert", "alert-danger")
        $("#msgmodalfacturacion").html("<span>Llenar los campos requeridos.</span>");
        return false
    }

    let url = site_url + "ajaxRequestServicio/ajax_datosfacturacion";
    myfunction.ajaxpost(url, data, function (response) {
        console.log(response)
        if (response && response.status == 1) {
            document.getElementById("msgmodal").classList.add("alert", "alert-success")
            $("#msgmodal").html("<span>Guardado correctamente.</span>");
            window.location.reload();
        } else {
            document.getElementById("msgmodal").classList.add("alert", "alert-danger")
            $("#msgmodal").html("<span>Ha ocurrido un problema.</span>");
        }
    })
}

const validarplaca = () => {
    let placas = document.getElementById('placas').value;
    let url = site_url + "ajaxRequestServicio/ajax_existePlaca";
    myfunction.ajaxpost(url, { placas }, function (data) {
        if (data.length >= 1) {
            document.getElementById("msgvalidationplacas").classList.add("invalid-feedback", "d-block")
            document.getElementById("msgvalidationplacas").classList.remove("valid-feedback")
            $("#msgvalidationplacas").html("<span>Las Placas ya se registraron.</span>");
            document.getElementById("placas_validas").value = 1

        } else {
            document.getElementById("msgvalidationplacas").classList.add("valid-feedback", "d-block")
            document.getElementById("msgvalidationplacas").classList.remove("invalid-feedback")
            $("#msgvalidationplacas").html("<span>Placas validas</span>");
            document.getElementById("placas_validas").value = 0
        }
    })
}

const onchangeMarca = () => {
    let id_marca = document.getElementById('id_marca').value;
    let url = site_url + "ajaxRequestServicio/ajax_getmodelobymarca";
    // return console.log(id_marca)
    $("#id_modelo").empty();
    $("#id_modelo").append("<option value=''>-- Selecciona --</option>");
    $("#id_modelo").removeAttr("disabled");
    !id_marca && false
    myfunction.ajaxpost(url, { id_marca }, function (data) {
        $("#id_modelo").empty();
        $("#id_modelo").append("<option value=''>-- Selecciona --</option>");
        $("#id_modelo").removeAttr("disabled");
        if (data && data.length > 0) {
            data.map(item => $("#id_modelo").append("<option value= '" + item.id + "'>" + item.modelo + "</option>"))
        } else {
            $("#id_modelo").empty();
            $("#id_modelo").append("<option value=''> No se encontraron datos</option>");
        }
    })
}

if (document.getElementById('id_servicio').value !== '') {
    cargarvehiculoinfo()
    onchange_estado()
}

const onchangeTiposervicio = () => {
    const id_servicio = document.getElementById('servicio').value
    $("#msg_servicio").addClass('custom-alert')
    switch (id_servicio) {
        case '1':
            $("#titulo_alerta").text('LAVADO EXPRESS')
            $("#contenido_alerta").html(`<ul>
                <li>Lavado Exterior</li>
                <li>Aplicación de Quita Gotas (vidrios)</li>
                <li>Aplicación de Cera Líquida</li>
                <li>Tratamiento para llantas</li>
            </ul>
            <h3>Total: $180</h3>
            `)
            break;
        case '2':
            $("#titulo_alerta").text('LAVADO RECOMENDADO')
            $("#contenido_alerta").html(`<ul>
                <li>LAVADO EXPRESS</li>
                <li>+ Limpieza Interior</li>
                <li>+ Aspirado</li>
            </ul>
            <h3>Total: $210 </h3>
            `)
            break;
        case '3':
            $("#titulo_alerta").text('SEMÍ-DETALLADO')
            $("#contenido_alerta").html(`<ul>
                <li>LAVADO Y ASPIRADO</li>
                <li>+ Aplicación de Cera en Crema</li>
            </ul>
            <h3>Total: $1500</h3> 
            `)
            break;
        case '4':
            $("#titulo_alerta").text('DETALLADO COMPLETO')
            $("#contenido_alerta").html(`<ul>
                <li>LAVADO, ASPIRADO Y SEMÍ-DETALLADO</li>
                <li>+ Descontaminación de pintura.</li>
                <li>+ Tratamiento de Pulido y Abrillantado</li>
            </ul>
            <h3>Total: $2350</h3>
            `)
            break;
        default:

            break;
    }
}