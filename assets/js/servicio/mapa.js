// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
// var mapa = null;

var map;
var marker;
function initMap() {

    // var myLatlng =  { lat: 19.3849022, lng: -103.5722297 }
    var myLatlng = new google.maps.LatLng(19.3849022, -103.5722297);

    map = new google.maps.Map(document.getElementById("mapa"), {
        center: myLatlng,
        zoom: 17,
    });
    const card = document.getElementById("pac-card");
    const input = document.getElementById("pac-input");
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
    const autocomplete = new google.maps.places.Autocomplete(input);
    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo("bounds", map);
    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
    const infowindow = new google.maps.InfoWindow();
    const infowindowContent = document.getElementById("infowindow-content");
    infowindow.setContent(infowindowContent);

    // console.log("default: ", { lat: 19.3849022, lng: -103.5722297 }, navigator.geolocation)

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            let { coords } = position;
            let pos = {
                lat: parseFloat(coords.latitude),
                lng: parseFloat(coords.longitude)
            }
            // console.log("current", pos)
            latLng = new google.maps.LatLng(pos.lat, pos.lng);

            marker = initmarker(infowindow, map, latLng);
            autocomplete.addListener("place_changed", () => {
                infowindow.close();
                marker.setVisible(false);
                const place = autocomplete.getPlace();

                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17); // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                // console.log(place.geometry.location)
                initmarker(infowindow, map, place.geometry.location)
                let address = "";

                if (place.address_components) {
                    address = [
                        (place.address_components[0] &&
                            place.address_components[0].short_name) ||
                        "",
                        (place.address_components[1] &&
                            place.address_components[1].short_name) ||
                        "",
                        (place.address_components[2] &&
                            place.address_components[2].short_name) ||
                        "",
                    ].join(" ");
                }
                infowindowContent.children["place-icon"].src = place.icon;
                infowindowContent.children["place-name"].textContent = place.name;
                infowindowContent.children["place-address"].textContent = address;
                infowindow.open(map, marker);
            });
            // 
        }, () => {
            console.log("error")
        });
    } else {
        console.log("geolocation no supported")
    }
}



const initmarker = (infoWindow, map, latlng = {}) => {

    marker = new google.maps.Marker({
        map,
        draggable: true,
        position: latlng,
        anchorPoint: new google.maps.Point(0, -29),
    });


    let markerLatLng = marker.getPosition();
    const { lat, lng } = markerLatLng
    let position = {
        lat: lat(),
        lng: lng(),
    }
    
    if (document.getElementById('id_servicio').value === '') {
        infoWindow.setPosition(position);
        infoWindow.setContent("Ubicacion actual.");
        infoWindow.open(map);
        map.setCenter(position);
        $("#latitud").val(lat());
        $("#longitud").val(lng());
        getAdress(lat(), lng()).then(({ numero_ext, calle, colonia }) => {
            $("#calle").val(calle);
            $("#numero_ext").val(numero_ext);
            $("#colonia").val(colonia);
        })
    }else{
        let latitud_servicio = document.getElementById('latitud').value
        let longitud_servicio = document.getElementById('longitud').value
        SetMarker(latitud_servicio, longitud_servicio, "ubicacion de servicio")
    }

    return marker;
}

const actualizacionautomatica = () => {
    getlavadorposition();
}
setInterval(actualizacionautomatica, 3000);

const getlavadorposition = () => {
    let id_servicio = document.getElementById('id_servicio').value
    let id_estatus_lavado = document.getElementById('id_estatus_lavado').value
    if (id_servicio !== '' && (id_estatus_lavado == 2 || id_estatus_lavado == 3)) {
        let url = site_url + "ajaxRequestServicio/ajax_getpositionlavador"
        myfunction.ajaxpost(url, { id_servicio }, function (data) {
            if (data && data.length > 0) {
                let { latitud_lavador, longitud_lavador } = data[0]
                if (latitud_lavador !== '' && longitud_lavador !== '') {
                    $("#latitud").val(latitud_lavador);
                    $("#longitud").val(longitud_lavador);
                    SetMarker(latitud_lavador, longitud_lavador)
                }
            }
        })
    }
}

const getAdress = async (lat = '', lng = '') => {
    if (lat == '' && lng == '') {
        return false
    }
    let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM`;
    let response = await myfunction.ajaxget(url);
    let data = await response.json();
    let resultado = data.results[0].address_components

    return {
        numero_ext: resultado[0].long_name,
        calle: resultado[1].long_name,
        colonia: resultado[2].long_name
    }

}

function SetMarker(lat = '', lng = '', msg = 'ubicacion') {
    //Remove previous Marker.
    if (marker != null) {
        marker.setMap(null);
    }

    //Set Marker on Map.
    var myLatlng = new google.maps.LatLng(lat, lng);
    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: msg
    });

    //Create and open InfoWindow.
    var infoWindow = new google.maps.InfoWindow();
    infoWindow.setContent(msg);
    infoWindow.open(map, marker);
};